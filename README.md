# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Advance Warfare is a multiplayer game and chat-system made for a programming assignment as part of a university qualification in programming. Currently there is no game-play side, only chat.

The plan is to use sprites ripped from the Nintendo DS game "Advance war: Duel Strike" to create a multiplayer-only turn-based strategy game that has a chat-frontend.

The project uses Maven - a build automation tool - and so is setup in Maven format, with configurations and code setup to use the resources folder which Maven builds into its the .jar file for the project.

Data is sent Server-Client by JSON - Data is put into a JSONObject instance before being sent in its String format with UTF-8 character encoding for both Server and Client socket read/write streams to prevent data-loss. Once the String is read its put into a new JSONObject instance on the readers end, allowing the data to be retrieved at ease. The JSONObject class and other related JSON classes come from the json.org library setup as a dependancy with Maven as so it wil be automatically downloaded, updated, and built into the JARs by Maven.

The program can run in a headless mode to allow it to run on command line systems, such as Linux web-servers. The Server also has its own GUI for running in graphical OS environments.

While a fair amount of the code is documented a lot of it still isn't - The primary focus for documentation / JavaDoc commenting has been on the "key" primary methods, such as Success/Failed methods and their handlers for passing and parsing data on both Server and Client end, and other methods that allow the main flow of the program to be followed.

The program supports user accounts that are stored using JSON objects. A user account is made up of a true-name and a display-name. The display-name can be changed at any time, and is the name shown in chat, while the true-name is fixed and normally hidden.

On online the player joins the global chat room (Which can't be left) - from there they can create new chat-rooms, join chat-rooms by name, change the settings of a chat-room if they are the owner, change their display-name or start a new game.

#### Login Screen ####

![user-account-login.png](https://bitbucket.org/repo/Ra9b5L/images/1344421328-user-account-login.png)

This is the first screen seen when stating up the Client. Here you can either create a new account or enter in the details of an existing account. 

When you create a new account you supply a username and password. This username is your true-name, and must be unique. Your password is hashed using SHA-256 on the Client side so that it can't be sniffed out using a package-sniffer. When the Server creates the user account file it creates a new JSON file with the true-name as the file name, and populates it with default data. The contents of the user-account file look like this:


```
#!JSON

{"password":"\u0004ø™m§c·©i±\u0002Žã\u0000uiêó¦5HmÚ²\u0011Õ\u0012È[�øû","display-name":"user2","friends":[],"status":"offline"}
```

Friends is a JSON array that holds the true-name of all of the users friends - this is sent to the Client and causes lists to render friend differently to make them stand out.
Status is used internally to denote if a player is offline, online, away, looking-for-game or in-game.
display-name is of course the user's display-name: this is the name that is shown in chat-logs and around the rest of the program.
password is a UTF-8 SHA-256 hashed version of the plain-text password provided by the user when the account was created.

#### Main frontend Screen ####

![Main-Frontend.png](https://bitbucket.org/repo/Ra9b5L/images/2587976380-Main-Frontend.png)

This is the next screen the user will meet after logging in - This is made up of a number of components:

Player list: This is a filtered list of the players currently online. The button above the list can be pressed and goes between three modes - "online", "in-room" and "friends" which changes the players listed. For example in "friends" mode the players friends are listed:

![players-friends-list.png](https://bitbucket.org/repo/Ra9b5L/images/235296089-players-friends-list.png)

* Chat-tab-panel: This panel is made up of tabs of the current chat-rooms the player is in. The player will always be in "global-chat" and cannot close that chat room. Other chat rooms however can be entered and exited at will provided the player has the correct permissions:

![chat-room-tab-panel.png](https://bitbucket.org/repo/Ra9b5L/images/1813698588-chat-room-tab-panel.png)

The chat-panels are a favorite as they have some interesting code:
Firstly the tabbed-panel heads are a custom component - this lets them be closed, and change their title based on unread messages in a chat-room. This is used when you are in a chat-room but that room isn't visible and it gets a new message, as shown in the image below:

![chat-room-tab-panel-new-messages.png](https://bitbucket.org/repo/Ra9b5L/images/1169918258-chat-room-tab-panel-new-messages.png)

Another interesting feature about the chat-rooms is their message handling. A message is actually a JSON object like so:


```
#!JSON

{"sender":"user","message":"message"}
```

The sender field is the true-name of the user that sent the message. All messages for a room are stored in a arraylist of type JSONObject with the actual list-model's data being rebuilt whenever a new message comes through or user data changes. When this happens for each element in the arraylist is converted into a single string by getting the current display-name for the true-name as sender and appending that to the message. The result is that if a user changes their displayname the chat logs will show that new displayname, meaning you can always match a message to a user on the players list without having to follow a chain of "x changed their display-name to y" messages.

![chat-room-tab-panel-display-name-before.png](https://bitbucket.org/repo/Ra9b5L/images/1217043482-chat-room-tab-panel-display-name-before.png)
![chat-room-tab-panel-display-name-after.png](https://bitbucket.org/repo/Ra9b5L/images/2690012502-chat-room-tab-panel-display-name-after.png)

Menu Bar: This menu-bar is rather ugly (something I'm hoping to fix later) but functional. It currently allows players to:

Change their display-name:

![change-display-name.png](https://bitbucket.org/repo/Ra9b5L/images/2433684857-change-display-name.png)

This dialog will be filled with their current display-name, and upon return the submitted display-name is checked with the Server only being told of a display-name change if the submitted display-name is different to the current display-name and not blank or server reserved (These names are "god", "server" and "announcer").

Create a new game:

This is the part currently being worked on. This will create a new game lobby where the player can (theoretically) invite other players to the lobby, open and change slots (changing the number of players that are in the game), change the map, and start the game (which only works when all players are ready).

The lobby GUI is functional but ugly - I had issues with the GUI layouts here trying to get the player-panels to separate their elements nicely (the ready button is meant to be at the bottom while the other two are meant to be at the top, and at the same place regardless of if a player is in the panel). Clicking the start button will start the game but currently the world-generating code is being done, so this may or may not result in anything more than a new blank JFrame appearing.

![game-lobby.png](https://bitbucket.org/repo/Ra9b5L/images/3208658817-game-lobby.png)

Chat-Room sub-menu:
This submenu allows you to do things with chat-rooms. Mainly:

Create new room:

![create-new-chat-room-public.png](https://bitbucket.org/repo/Ra9b5L/images/3673533335-create-new-chat-room-public.png)

A chat-room requires a name, and can be private or public (technically "not-private"). If a room is public then anyone can join provided they know the name of the room, while private rooms require a user to be on the allowed list to join. When the room is created the creator is made the room owner. The owner doesn't have to be on the allowed-list of a private room they own to join it - This is a hard coded check that is separate from the allowed-players-list in the chat-room object to prevent owners from being locked out from their own rooms. 

Should a user try and join a chat-room that they are not allowed in, they are told "chat-room doesn't exist" to hide the fact that they are not allowed. In the image below the output console is shown with the actual reason for why the user can't join:

![chat-room-failed-with-log.png](https://bitbucket.org/repo/Ra9b5L/images/3184872143-chat-room-failed-with-log.png)


The change-settings menu is grayed out if the user isn't the owner of the visible chat-room, and is the same component used to create chat-rooms, except with the chat-room name field disabled as the name can't be changed. Users can be added and removed from the chat-room's allowed list, and the room can be toggled between private and public:

![chat-room-change-settings.jpg](https://bitbucket.org/repo/Ra9b5L/images/2582641020-chat-room-change-settings.jpg)

Note that the allowed-players list in the chat-settings component is using the 'PlayerListRenderer' list-render, meaning that the list is rendered with the players status (offline/online/etc) and color based on that status (friends are rendered in blue):

![chat-settings-allowed-users-list.png](https://bitbucket.org/repo/Ra9b5L/images/2215724927-chat-settings-allowed-users-list.png)

ChatRooms are stored server side in a JSON file with the following format:

```
#!JSON
{"owner":"server","private":false,"allowed-players":["all"],"players-in-chat":[]}
```

The name of the ChatRoom is tied to the file-name the ChatRoom's data is stored in.

allowed-players is a JSON-Array that holds the true-names of all players allowed in the room if its set to private. If set to not-private then this has "all" as its only element, meaning anyone can join.

players-in-chat is a JSON-Array used only by the ChatRoom instance - data is never written into this array on disk, and is there only for reference (so that it doesn't have to be created each time the data is read from disk into the JSONObject instance).

### How do I get set up? ###

#### Client ####

To create a new Client instance simply run the .jar file - This will create a new Client instance with a host of "localhost". To set a custom host use the "host:" parameter like so: `java -jar Advance-Warfare.jar host:www.zhang.nz`. This will create a new Client instance that will try and connect with the host "www.zhang.nz".

#### Server ####

To create a new Server instance run the .jar file with the server parameter: `java -jar Advance-Warfare.jar server`
To run the server headless - which is required for running in a server/command line based environment - add the "headless" parameter: `java -jar Advance-Warfare.jar headless server`

### Image documentation ###

These are all images and design documents draw/written out by hand. These images are all 2k so they can be downloaded and zoomed in on for greater detail.
The two most important images are first, and it might be better to open the images in a new tab/window for easier viewing.

![processes.jpg](https://bitbucket.org/repo/Ra9b5L/images/2129975089-processes.jpg)
![rsz_diagram.jpg](https://bitbucket.org/repo/Ra9b5L/images/2734183433-rsz_diagram.jpg)

![20150608_203755.jpg](https://bitbucket.org/repo/Ra9b5L/images/3909118569-20150608_203755.jpg)
![20150608_203825.jpg](https://bitbucket.org/repo/Ra9b5L/images/2793986161-20150608_203825.jpg)
![20150608_203616.jpg](https://bitbucket.org/repo/Ra9b5L/images/3582692825-20150608_203616.jpg)
![20150608_203638.jpg](https://bitbucket.org/repo/Ra9b5L/images/3368850297-20150608_203638.jpg)
![20150608_203747.jpg](https://bitbucket.org/repo/Ra9b5L/images/3089914388-20150608_203747.jpg)
![20150608_203701.jpg](https://bitbucket.org/repo/Ra9b5L/images/1081925768-20150608_203701.jpg)
![20150608_203652.jpg](https://bitbucket.org/repo/Ra9b5L/images/1517556942-20150608_203652.jpg)
![20150608_203815.jpg](https://bitbucket.org/repo/Ra9b5L/images/2467239776-20150608_203815.jpg)