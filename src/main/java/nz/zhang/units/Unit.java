package nz.zhang.units;

import nz.zhang.weapons.PrimaryWeapon;
import nz.zhang.weapons.SecondaryWeapon;
import nz.zhang.world.Tile;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.net.URI;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Unit abstract class
 * <p>
 * Created by G-Rath on 30/04/2015.
 */
public abstract class Unit
{
	private final int MAX_FUEL;// = 99;
	private Color teamColor = Color.BLUE;

	private Path imagePath;

	private ArrayList<BufferedImage> animationFrames = new ArrayList<>();

	private BufferedImage selectedImage;

	private int currentFrame = 0;

	private String tag = "unit";

	private PrimaryWeapon primaryWeapon = null;
	private SecondaryWeapon secondaryWeapon = null;

	private Tile currentTile;
	/** This units current location on the grid */
	private Point location;

	private int currentFuel;

	private int fuelCostPerMove = 1;

	private MovementType movementType = MovementType.INFANTRY;

	private int movementPoints = 1;

	private int visionRange = 1;

	private boolean selected = false;

	private BufferedImage currentImage;

	public Unit( Color teamColor )
	{
		this( teamColor, 99 );
	}

	public Unit( Color teamColor, int maxFuel )
	{
		this.MAX_FUEL = maxFuel;
		this.teamColor = teamColor;

		if( teamColor == Color.BLUE )
			imagePath = Paths.get( URI.create( "game-sprites/blue-team.png" ) );
		else if( teamColor == Color.RED )
			imagePath = Paths.get( URI.create( "game-sprites/red-team.png" ) );
		else if( teamColor == Color.YELLOW )
			imagePath = Paths.get( URI.create( "game-sprites/yellow-team.png" ) );
		else if( teamColor == Color.GREEN )
			imagePath = Paths.get( URI.create( "game-sprites/green-team.png" ) );
	}

	public String tag()
	{
		return tag;
	}

	public boolean hasPrimaryWeapon()
	{
		return primaryWeapon != null;
	}

	public boolean hasSecondaryWeapon()
	{
		return secondaryWeapon != null;
	}

	public void destory()
	{

	}

	public void select()
	{
		selected = true;
		currentImage = selectedImage;
	}

	public void deselect()
	{
		selected = false;

		currentImage = animationFrames.get( currentFrame );
	}

	public void tick()
	{
		if( currentFrame++ == animationFrames.size())
			currentFrame = 0;

		if( selected )
		{
			currentImage = selectedImage;
		}
		else
			currentImage = animationFrames.get( currentFrame );
	}

	public void loadSelectionImage()
	{

	}

	public void loadAnimationImages()
	{

	}

	public boolean canBeOnTiel( Tile tileToCheck )
	{
		return false;
	}

	public void moveToTile( Tile tileToMoveTo )
	{

	}

	public void finishedMove()
	{

	}
}
