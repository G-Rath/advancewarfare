package nz.zhang.units.vehicles;

import nz.zhang.units.Vehicle;

import java.awt.*;

/**
 * Recon - Unit, Vehicle
 * <p>
 * Created by G-Rath on 21/05/2015.
 */
public class Recon extends Vehicle
{

	public Recon( Color teamColor )
	{
		super( teamColor );
	}
}
