package nz.zhang.units;

/**
 * MovementType - type of movement for units
 * <p>
 * Created by G-Rath on 20/05/2015.
 */
public enum MovementType
{
	INFANTRY, MECH, TIRE, TREADS, AIR, SHIP, TRANSPORT,
}
