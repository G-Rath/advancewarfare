package nz.zhang.units;

import java.awt.*;

/**
 * Vehicle abstract class
 * <p>
 * Created by G-Rath on 21/05/2015.
 */
public abstract class Vehicle extends Unit
{

	public Vehicle( Color teamColor )
	{
		super( teamColor );
	}
}
