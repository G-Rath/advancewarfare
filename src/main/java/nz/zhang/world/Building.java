package nz.zhang.world;

import nz.zhang.abilities.CanCapture;
import nz.zhang.units.Unit;

import java.awt.*;

/**
 * Building abstract class
 * <p/>
 * Created by G-Rath on 21/05/2015.
 */
public abstract class Building extends Tile
{
	/** Can this building be captured? If not then its expected that this building has a use-once ability */
	private boolean canBeCaptured = true;

	/** What team (if any) currently owns (has captured) this building? */
	private String team = "none";

	public Building( Point location, String team )
	{
		super( location );
		this.team = team;
	}

	public void capture( Unit unit )
	{
		if( unit instanceof CanCapture )
		{
			//the unit trying to capture can capture. (this is mostly a safety check since this is pretty impossible anyway).
			foreground.setHealth( foreground.getHealth() - ( (CanCapture) unit ).captureAmount() );

			new Background.Grass();
		}
	}

	public String getTeam()
	{
		return team;
	}
}
