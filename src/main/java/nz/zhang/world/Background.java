package nz.zhang.world;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Background class
 * <p>
 * Created by G-Rath on 21/05/2015.
 */
public class Background
{
	protected BufferedImage backgroundImage;

	protected ArrayList<BufferedImage> backgroundImages = new ArrayList<>();

	public void loadImage() throws IOException
	{

	}

	public void placed()
	{

	}

	public void draw( Graphics2D g2d, Point gridLoc, Component drawer )
	{
		if( backgroundImages.size() > 0 )
		{
			BufferedImage image = backgroundImages.get( 0 );
			g2d.drawImage( image, 0, image.getHeight(), drawer );
		}
	}

	static class Grass extends Background
	{
		@Override
		public void loadImage() throws IOException
		{
			//0 is the default image
			backgroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/background/grass.png" ) ) );

			//1 is the left shadow image
			backgroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/background/grass-shadow.png.png" ) ) );
		}
	}
}