package nz.zhang.world;

import org.json.JSONArray;
import org.json.JSONException;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * TileContainer - Holds all tiles in the world, and handles drawing them.
 * <p>
 * Created by G-Rath on 22/05/2015.
 */
public class TileContainer extends JPanel
{
	HashMap<Point, Tile> tileHashMap = new HashMap<>();

	ArrayList<Point> hqLocations = new ArrayList<>();

	public void parseTileData( JSONArray tileDataArray )
	{
		try
		{
			System.out.println( "\t" + "parsing tile-data:" );

			for( int x = 0; x < tileDataArray.length(); x++ )
			{
				JSONArray xArray = tileDataArray.getJSONArray( x );

				for( int y = 0; y < xArray.length(); y++ )
				{
					System.out.println( "\t\t" + x + "x, " + y + "y : " + tileDataArray.getString( y ) );

					Point location = new Point( x, y );

					Tile tile = new Tile(location);

					if( tileDataArray.getString( y ).equals( "hq" ) )
					{
						//Do nothing
						hqLocations.add( location );
					}
				}
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void doDrawing( Graphics g )
	{
		Graphics2D g2d = (Graphics2D) g;

		for( Map.Entry<Point, Tile> tileEntry : tileHashMap.entrySet() )
		{
			tileEntry.getValue().drawTile( g2d, this );
		}

		//g2d.drawImage( drawImage, drawLocation.x, drawLocation.y, this );

		Toolkit.getDefaultToolkit().sync(); //Sync the swing so that any animations we do will be smooth on Linux
		g2d.dispose();                      //Dispose the copy of the Graphic we made
	}

	@Override
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		doDrawing( g );
	}
}
