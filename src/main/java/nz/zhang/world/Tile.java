package nz.zhang.world;

import nz.zhang.units.Unit;

import java.awt.*;

/**
 * Tile class - represents one grid space
 * <p>
 * Created by G-Rath on 30/04/2015.
 */
public class Tile
{
	protected Foreground foreground;
	protected Background background;

	/** The Unit which is currently occupying this tile, if any */
	protected Unit occupant = null;
	/** This tiles grid location */
	protected Point gridLoc;

	public Tile( Point location )
	{
		gridLoc = location;
	}

	public Unit getOccupant()
	{
		return occupant;
	}

	public void setOccupant( Unit occupant )
	{
		this.occupant = occupant;
	}

	public float getDefRating()
	{
		return 0;
	}

	public void placeTile( Point gridLoc )
	{
		this.gridLoc = gridLoc;
	}

	public void drawTile( Graphics2D g2d, Component drawer )
	{
		foreground.draw( g2d, gridLoc, drawer );
		background.draw( g2d, gridLoc, drawer );
	}

	static class Plains extends Tile
	{
		public Plains( Point location )
		{
			super( location );
			background = new Background.Grass();
			foreground = new Foreground.None();
		}
	}
}
