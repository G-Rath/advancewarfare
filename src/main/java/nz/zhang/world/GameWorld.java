package nz.zhang.world;

import nz.zhang.client.Client;
import nz.zhang.server.Server;
import nz.zhang.units.Unit;
import org.json.JSONException;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

/**
 * GameWorld
 * <p>
 * Created by G-Rath on 29/04/2015.
 */
public class GameWorld extends JFrame
{
	public static Font monoFont = new Font( "monospaced", Font.BOLD, 15 );

	private final int screenX = 640;

	private final int screenY = 640;

	private String playerRed_TrueName;
	private String playerBlue_TrueName;
	private String playerYellow_TrueName;
	private String playerGreen_TrueName;

	private JLayeredPane lpane = new JLayeredPane();

	private Timer tickTimer;

	private TileContainer mainContainer;

	private HashMap<Point, Unit> unitsHashMap = new HashMap<>();

	private Point worldSize = new Point( 0, 0 );

	private String mapName = "none";

	private String currentGameID;

	public GameWorld( String currentGameID)
	{
		this.currentGameID = currentGameID;
		setupGUI();
	}

	public void processGameCommand( JSONObject gameCommand )
	{
		try
		{
			switch( gameCommand.getString( "game-command" ) )
			{
				case "pre-start-game":
					setupForGame( gameCommand );
					break;

				case "start-game":

					break;

				default:
					break;
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	private void setupGUI()
	{
		mainContainer = new TileContainer();

		this.setMinimumSize( new Dimension( 500, 300 ) );

		this.setTitle( "Advance Warfare - Client " );
		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.add( mainContainer );
//		this.add( mainPanel );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );
	}

	private void setupForGame( JSONObject commandData )
	{
//		System.out.println( "setting up for game " );

		try
		{
			playerRed_TrueName = commandData.getString( "player-red" );
			playerBlue_TrueName = commandData.getString( "player-blue" );
			playerYellow_TrueName = commandData.getString( "player-yellow" );
			playerGreen_TrueName = commandData.getString( "player-green" );

			mapName = commandData.getString( "map-name" );

			loadMap( mapName );

		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void loadMap( String mapName )
	{
		JSONObject mapData = Client.getInstance().getMapDataByName( mapName);

		if( mapData != null )
		{
			loadWorldFromData( mapData );
		}
	}

	public void loadWorldFromData( JSONObject worldData )
	{
		try
		{
			mapName = worldData.getString( "map-name" );
			worldSize.x = worldData.getInt( "width" );
			worldSize.y = worldData.getInt( "height" );

			mainContainer.parseTileData( worldData.getJSONArray( "tile-map" ) );
			finishedInitialSetup();
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	private void finishedInitialSetup()
	{
//		Client.getInstance().worldReadyForGameStart();
		System.out.println( "finished setting up" );
//		worldReadyForGameStart();
	}

	/** Sends a message to the {@code Server} to tell all other {@code Player}s in the game that this {@code Client} is ready for the game to start. */
	public void worldReadyForGameStart()
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "game-sync" );
			command.put( "game-id", currentGameID );
			command.put( "sync-point", "world-ready" );

			System.out.println( "worldReadyForGameStart - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Sends a command with parameters in JSON as a {@code String} to the {@link Server} where it can be created back into a {@link JSONObject} and parsed.
	 * <p>
	 * This is a pass-through method for the {@code Client} instances {@link Client#sendCommandToServer(JSONObject)} method so that we can make changes
	 * to how the data is sent without having to change multiple lines of code.
	 *
	 * @param command information for the command that the {@code Server} is to carry out.
	 */
	private void sendCommandToServer( JSONObject command )
	{
		Client.getInstance().sendCommandToServer( command );
	}

	public void setJFrameWindowIcon()
	{
		try
		{
			//@todo update this method - taken from pizza machine
			String filePath = "/sprites/pizza-icon.png";
			InputStream in = getClass().getResourceAsStream( filePath );
			BufferedImage image = ImageIO.read( in );

			setIconImage( image );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
	}

	public void setupWorldVisuals()
	{

	}
}
