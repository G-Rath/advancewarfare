package nz.zhang.world.buildings;

import nz.zhang.world.Building;
import nz.zhang.world.Foreground;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;

/**
 * HQ - Type of building. Game-win/lose condition building
 * Created by G-Rath on 5/06/2015.
 */
public class HQ extends Building
{
	public HQ( Point location, String team )
	{
		super( location, team );

		foreground = new Foreground()
		{
			@Override
			public void setupForeground() throws IOException
			{
				defenseBonus = 1;

				//0 is the default image

				switch( HQ.this.getTeam() )
				{
					case "red":
						foregroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/foreground/buildings/red/hq.png" ) ) );
						break;
					case "blue":
						foregroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/foreground/buildings/blue/hq.png" ) ) );
						break;
					case "yellow":
						foregroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/foreground/buildings/yellow/hq.png" ) ) );
						break;
					case "green":
						foregroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/foreground/buildings/green/hq.png" ) ) );
						break;
				}

				//1 is the left shadow image
				//backgroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/background/grass-shadow.png.png" ) ) );
			}
		};
	}
}
