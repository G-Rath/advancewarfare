package nz.zhang.world;

import nz.zhang.units.MovementType;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Foreground class
 * <p>
 * Created by G-Rath on 21/05/2015.
 */
public class Foreground
{
	/** The health of this tile. When health <= 0 {@link Foreground#healthBelowZero()} is called. For building this is capture ammount */
	protected int health = 10;

	protected ArrayList<BufferedImage> foregroundImages = new ArrayList<>();

	protected int defenseBonus = 0;

	protected HashMap<MovementType, Integer> supportedMovementTypes = new HashMap<>();

	protected boolean hasShadow = false;

	public void setupForeground() throws IOException
	{

	}

	/**
	 * Event method that is called when this {@code Foreground}'s health is or below zero. This is used to do actions such as {@link Building} changing
	 * owners (via capture) or other {@code Foreground} objects being destroyed.
	 */
	public void healthBelowZero()
	{

	}

	/**
	 * Sets the heath of this {@code Foreground} object - If it results in the health being {@code <=} 0 then {@link Foreground#healthBelowZero()}
	 * is called.
	 *
	 * @param health the new health of this {@code Foreground} object
	 */
	public void setHealth( int health )
	{
		this.health = health;

		if( health <= 0 )
		{
			healthBelowZero();
		}
	}

	public int getHealth()
	{
		return health;
	}

	public void draw( Graphics2D g2d, Point gridLoc, Component drawer )
	{
		if( foregroundImages.size() > 0 )
		{
			BufferedImage image = foregroundImages.get( 0 );
			g2d.drawImage( image, 0, image.getHeight(), drawer);
		}
	}

	public static class None extends Foreground
	{
		@Override
		public void setupForeground() throws IOException
		{
			defenseBonus = 0;
			//0 is the default image
			//backgroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/background/grass.png" ) ) );

			//1 is the left shadow image
			//backgroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/background/grass-shadow.png.png" ) ) );
		}
	}

	public static class Forest extends Foreground
	{
		@Override
		public void setupForeground() throws IOException
		{
			defenseBonus = 1;
			//0 is the default image
			foregroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/foreground/forest.png" ) ) );

			//1 is the left shadow image
			//backgroundImages.add( ImageIO.read( getClass().getResourceAsStream( "/game-sprites/world/background/grass-shadow.png.png" ) ) );
		}
	}
}
