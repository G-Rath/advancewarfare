package nz.zhang.gui;

import nz.zhang.gui.listmodels.GameListModel;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * GameList
 * <p>
 * Created by G-Rath on 3/05/2015.
 */
public class GameList extends JPanel
{
	private JList<String> listOfGames;

	private GameListModel<String> gameDisplayList = new GameListModel<>();

	public GameList( LayoutManager layout, boolean isDoubleBuffered )
	{
		super( layout, isDoubleBuffered );
		setupGameList();
	}

	public GameList( LayoutManager layout )
	{
		super( layout );
		setupGameList();
	}

	public GameList( boolean isDoubleBuffered )
	{
		super( isDoubleBuffered );
		setupGameList();
	}

	public GameList()
	{
		setupGameList();
	}

	/** Clears the selection of the listOfGames JList */
	public void clearGameListSelection()
	{
		listOfGames.clearSelection();
	}

	public void setupGameList()
	{
		gameDisplayList.add( "No Games" );
		listOfGames = new JList<>( gameDisplayList );
		listOfGames.setFocusable( false );
		JScrollPane gameScroll = new JScrollPane( listOfGames );

		//this.setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );

		this.add( new JLabel( "Games:", SwingConstants.LEFT ), BorderLayout.PAGE_START );
		this.add( gameScroll );

		//addGameDisplayString( "game 1" );
	}

	public void addListToDisplayModel( List<String> list )
	{
		gameDisplayList.addAll( list );
	}

	public void clearDisplayModel()
	{
		gameDisplayList.clear();
	}

	public void addGameDisplayString( String gameString )
	{
		gameDisplayList.add( gameString );
	}

}
