package nz.zhang.gui;

import nz.zhang.client.Client;
import nz.zhang.server.Server;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * LoginDialog
 * <p>
 * Created by G-Rath on 3/05/2015.
 */
public class LoginDialog extends JFrame
{
	private JTextField loginField_Username;
	private JPasswordField loginField_Password;

	private JButton loginField_Button_Login;
	private JButton loginField_Button_Signup;

	private JLabel loginField_Feedback_text;

	public LoginDialog() throws HeadlessException
	{
		createDialog();
	}

	public void tryConnectToServer()
	{
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout( new BoxLayout( mainPanel, BoxLayout.PAGE_AXIS ) );
		mainPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.add( mainPanel );
		this.setResizable( false );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );
	}

	public void createDialog()
	{
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout( new BoxLayout( mainPanel, BoxLayout.PAGE_AXIS ) );
		mainPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

		/* Username Panel */
		JPanel jPanel_Username = new JPanel();
		jPanel_Username.setLayout( new BoxLayout( jPanel_Username, BoxLayout.LINE_AXIS ) );

		loginField_Username = new JTextField();
		loginField_Username.setPreferredSize( new Dimension( 250, 20 ) );

		jPanel_Username.add( new JLabel( "Username:" ) );
		jPanel_Username.add( Box.createRigidArea( new Dimension( 10, 0 ) ) );
		jPanel_Username.add( loginField_Username );

		/* Password Panel */
		JPanel jPanel_Password = new JPanel();
		jPanel_Password.setLayout( new BoxLayout( jPanel_Password, BoxLayout.LINE_AXIS ) );

		loginField_Password = new JPasswordField();
		loginField_Password.setPreferredSize( new Dimension( 250, 20 ) );

		//loginField_Password.setBorder( BorderFactory.createCompoundBorder( BorderFactory.createLineBorder( Color.red ), loginField_Password.getBorder() ) );

		jPanel_Password.add( new JLabel( "Password:" ) );
		jPanel_Password.add( Box.createRigidArea( new Dimension( 10, 0 ) ) );
		jPanel_Password.add( loginField_Password );

		//Login Button Panel
		JPanel jPanel_FeedbackAndButtons = new JPanel();
		jPanel_FeedbackAndButtons.setLayout( new BoxLayout( jPanel_FeedbackAndButtons, BoxLayout.LINE_AXIS ) );
		//jPanel_FeedbackAndButtons.setBorder( BorderFactory.createEmptyBorder( 0, 10, 10, 0 ) );

		loginField_Feedback_text = new JLabel( "" );

		loginField_Button_Signup = new JButton( "Create account" );

		//loginField_Button_Signup.setFont( new Font( loginField_Button_Signup.getFont().getFontName(), Font.PLAIN, 5 ) );
		loginField_Button_Login = new JButton( "Login" );

		jPanel_FeedbackAndButtons.add( loginField_Feedback_text );
		jPanel_FeedbackAndButtons.add( Box.createHorizontalGlue() );
		jPanel_FeedbackAndButtons.add( loginField_Button_Signup );
		jPanel_FeedbackAndButtons.add( Box.createRigidArea( new Dimension( 10, 0 ) ) );
		jPanel_FeedbackAndButtons.add( loginField_Button_Login );

		loginField_Feedback_text.setForeground( Color.RED );
		loginField_Feedback_text.setFont( Server.FONT_MONO_SMALL_BOLD );
		loginField_Feedback_text.setText( "Account already exists " );

		//Add all to main panel
		mainPanel.add( jPanel_Username );
		mainPanel.add( Box.createVerticalStrut( 10 ) );
		mainPanel.add( jPanel_Password );
		mainPanel.add( Box.createVerticalStrut( 10 ) );
		mainPanel.add( jPanel_FeedbackAndButtons );

		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.add( mainPanel );
		this.setResizable( false );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );

		loginField_Password.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				eventButton_Pressed_Login();
			}
		} );
		loginField_Username.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				eventButton_Pressed_Login();
			}
		} );

		loginField_Button_Login.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				eventButton_Pressed_Login();
			}
		} );
		loginField_Button_Signup.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				eventButton_Pressed_CreateAccount();
			}
		} );
	}

	/**
	 * Gets the text of the loginField_Username JTextField that holds the player's username.
	 *
	 * @return Players username that's in the loginField_Username JTextField
	 */
	public String getUsername()
	{
		return loginField_Username.getText();
	}

	/**
	 * Gets a hashed String of the loginField_Password JPasswordField's text
	 *
	 * @return Hashed version of the JPasswordField's text
	 */
	public String getPassword()
	{
		return Client.getInstance().getHashedPassword( loginField_Password.getPassword() );
	}

	/**
	 * Set's the text and foreground color of the loginField_Feedback_text JLabel
	 *
	 * @param newText Text to set JLabel's text to
	 * @param color   Color to set the JLabel's foreground color to
	 */
	public void setFeedbackText( String newText, Color color )
	{
		loginField_Feedback_text.setForeground( color );
		setFeedbackText( newText );
	}

	/**
	 * Sets the text of the loginField_Feedback_text JLabel
	 *
	 * @param newText Text to set JLabel's text to
	 */
	public void setFeedbackText( String newText )
	{
		loginField_Feedback_text.setText( newText );
	}

	public void eventButton_Pressed_Login()
	{
		if( !loginField_Button_Login.isEnabled() )
			return; //If the button is not actually enabled, don't login.

		disableButtons(); //Disable the buttons so the user can't make any more threads until they are re-enabled

		new Thread( new Runnable()
		{
			@Override
			public void run()
			{
				Client.getInstance().tryLogin( loginField_Username.getText(), Client.getInstance().getHashedPassword( loginField_Password.getPassword() ) );
			}
		} ).start();
	}

	public void eventButton_Pressed_CreateAccount()
	{
		if( !loginField_Button_Signup.isEnabled() )
			return; //If the button is not actually enabled, don't create-account.

		disableButtons(); //Disable the buttons so the user can't make any more threads until they are re-enabled

		new Thread( new Runnable()
		{
			@Override
			public void run()
			{
				Client.getInstance().tryCreateAccount( loginField_Username.getText(), Client.getInstance().getHashedPassword( loginField_Password.getPassword() ) );
			}
		} ).start();
	}

	public void disableButtons()
	{
		loginField_Button_Login.setEnabled( false );
		loginField_Button_Signup.setEnabled( false );
	}

	public void enableButtons()
	{
		loginField_Button_Login.setEnabled( true );
		loginField_Button_Signup.setEnabled( true );
	}
}
