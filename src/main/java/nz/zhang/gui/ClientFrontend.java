package nz.zhang.gui;

import nz.zhang.client.Client;
import nz.zhang.server.Server;
import org.json.JSONObject;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.util.HashSet;

/**
 * ClientFrontend
 * <p>
 * Created by G-Rath on 9/05/2015.
 */
public class ClientFrontend extends JFrame
{
	private JMenuBar menuBar;

	private JPopupMenu contextMenu;

	private ChatPlayersList chatPlayersList;

	private JTextField inputField;

	private JTabbedPane chatTabsPanel;

	private JMenuItem menuItem_ChangeChatRoomSettings;

	private ChatRoomSettingsDialog chatRoomSettingsDialog = null;

	private String currentVisibleChatRoom = "";

	private JButton menu_CreateNewGame;

	public ClientFrontend() throws HeadlessException
	{
		setupClientFrontEnd();
	}

	public void rebuildPlayerFriendList( HashSet<String> friendList )
	{
		chatPlayersList.rebuildFriendsList( friendList );
	}

	public void updatePlayersList( JSONObject playersList, String topic )
	{
		chatPlayersList.updatePlayersList( playersList, topic );
		reLoadChatRoomMessagesLogs(); //Reload the chat-room message logs in case any display-names have been changed.
	}

	public void disableCreateNewGameButton()
	{
		menu_CreateNewGame.setEnabled( false );
	}

	public void enableCreateNewGameButton()
	{
		menu_CreateNewGame.setEnabled( true );
	}

	public boolean isPlayerOnline( String playerName )
	{
		return chatPlayersList.isPlayerOnline( playerName );
	}

	public void enterRoom( String roomName, boolean canClose )
	{
		addTab( roomName, canClose );
		changeVisibleRoom( roomName ); //Change the room, but don't request a new player-in-room list.
	}

	public void addTab( String tabName )
	{
		addTab( tabName, true );
	}

	public void addTab( String tabName, boolean canClose )
	{
		ChatRoomPanel chatPanel = new ChatRoomPanel();
		chatTabsPanel.addTab( tabName, chatPanel );
		chatTabsPanel.setTabComponentAt( chatTabsPanel.indexOfComponent( chatPanel ), new ChatTabHead( chatTabsPanel, canClose ) );
	}

	public void sendMessageToChatRoom( String message )
	{
		Client.getInstance().trySendMessageToChatRoom( message );
	}

	public void reLoadChatRoomMessagesLogs()
	{
		for( int i = 0; i < chatTabsPanel.getTabCount(); i++ )
		{
			( (ChatRoomPanel) chatTabsPanel.getComponentAt( i ) ).reGenerateChatLogModel();
		}
	}

	/**
	 * Checks if a {@code ChatRoomPanel} is dirty - To be dirty then the {@code ChatRoom} must have at least one new message that's considered unread.
	 * If a {@code ChatRoomPanel} is dirty then its {@code ChatTabHead} component's title is changed to show the number of unread messages.
	 * Once all the messages are cleared (by the {@code Client} viewing the {@code ChatRoomPanel} the {@code ChatRoom} is considered "clean".
	 *
	 * @param panel the {@code ChatRoomPanel} that we're wanting to check.
	 */
	public void checkChatRoomPanelForDirt( ChatRoomPanel panel )
	{
		ChatTabHead tabHead = (ChatTabHead) chatTabsPanel.getTabComponentAt( chatTabsPanel.indexOfComponent( panel ) );

//		if( tabHead.getTabbedPaneText().equals( currentVisibleChatRoom ) )
//			panel.clean();

		if( chatTabsPanel.getSelectedIndex() != chatTabsPanel.indexOfComponent( panel ) )
			panel.setDirty( true );
		else
			panel.clean();

		if( tabHead != null )
		tabHead.updateTitleForUnreadMessages( panel.getNumberOfNewMessagesSinceDirty() );
	}

	public void receiveNewMessageForRoom( String roomName, JSONObject messageObject )
	{
		int tabIndex = findChatRoomTabIndexByTitle( roomName );

		if( tabIndex > -1 ) //If the index is greater than -1 then a tab with that roomName as its title does exist.
		{
			ChatRoomPanel chatPanel = (ChatRoomPanel) chatTabsPanel.getComponentAt( tabIndex );
			chatPanel.addNewMessage( messageObject );

			checkChatRoomPanelForDirt( chatPanel );
		}
	}

	/**
	 * Returns the index location of the tab that has the given title in the {@code chatTabsPanel}.
	 *
	 * @param tabTitle the title of the tab we want to know the index of.
	 *
	 * @return the index of the tab that has the provided title, {@code -1} if no tab has said title.
	 */
	public int findChatRoomTabIndexByTitle( String tabTitle )
	{
		for( int i = 0; i < chatTabsPanel.getTabCount(); i++ )
		{
			if( chatTabsPanel.getTitleAt( i ).equals( tabTitle ) )
				return i;
		}

		return -1;
	}

	public void menuBar_Pressed_ChangeDisplayName()
	{
		String thisPlayersDisplayName = Client.getInstance().getThisPlayersDisplayName();

		String newDN;
		newDN = (String) JOptionPane.showInputDialog( this,
		                                              "new display-name:",
		                                              "change display-name",
		                                              JOptionPane.PLAIN_MESSAGE,
		                                              null,
		                                              null,
		                                              thisPlayersDisplayName );

		boolean stayInLoop = true;

		while( stayInLoop )
		{
			//If the display-name is null, blank, a reserved-name or the current name, don't accept it.
			if( newDN == null || newDN.equals( "" ) || Server.RESERVED_DISPLAY_NAMES.contains( newDN ) || newDN.equals( thisPlayersDisplayName ) )
			{
				if( newDN == null || newDN.equals( thisPlayersDisplayName ) ) //Cancelling isn't an error, but we should still note it.
				{
					System.out.println( "Client: user cancelled change-display-name" );
					stayInLoop = false;
				}
				else
				{
					System.out.println( "invalid display-name given" );

					JOptionPane.showMessageDialog( this, "Invalid display-name", "Display name error", JOptionPane.ERROR_MESSAGE );

					newDN = (String) JOptionPane.showInputDialog( this,
					                                              "new display-name:",
					                                              "change display-name",
					                                              JOptionPane.PLAIN_MESSAGE,
					                                              null,
					                                              null,
					                                              thisPlayersDisplayName );
				}
			}
			else
				stayInLoop = false;
		}

		Client.getInstance().handleTryChangeDisplayName( newDN );
	}

	public void menuBar_Pressed_CreateNewGame()
	{
		Client.getInstance().tryCreateNewGame();
	}

	public void createChatRoomReturn( boolean result )
	{
		if( chatRoomSettingsDialog != null )
			chatRoomSettingsDialog.checkRoomFreeReply( result );
	}

	public void changeVisibleRoom()
	{

	}

	public void setupMenuBar_ChatMenu()
	{
		JMenu chatRoomSubMenu = new JMenu( "chat-room" );
		JMenuItem menuItem_CreateNewChatRoom = new JMenuItem( "create new chat room" );
		JMenuItem menuItem_JoinChatRoom = new JMenuItem( "enter chat-room" );

		menuItem_ChangeChatRoomSettings = new JMenuItem( "room settings..." );

		chatRoomSubMenu.add( menuItem_CreateNewChatRoom );
		chatRoomSubMenu.add( menuItem_ChangeChatRoomSettings );
		chatRoomSubMenu.add( menuItem_JoinChatRoom );

		menuBar.add( chatRoomSubMenu );

		menuItem_CreateNewChatRoom.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				if( chatRoomSettingsDialog == null )
				{
					chatRoomSettingsDialog = new ChatRoomSettingsDialog( true, "", null );
					setChatRoomSettingsDialogWindowClosingAdapter();
				}
			}
		} );

		menuItem_ChangeChatRoomSettings.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				if( Client.getInstance().isRoomOwner( currentVisibleChatRoom ) && chatRoomSettingsDialog == null )
				{
					chatRoomSettingsDialog = new ChatRoomSettingsDialog( false, currentVisibleChatRoom, Client.getInstance().getChatRoomDataFromName( currentVisibleChatRoom ) );
					setChatRoomSettingsDialogWindowClosingAdapter();
				}
			}
		} );

		menuItem_JoinChatRoom.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				tryJoinChatRoomFromGUI();
//				( new JoinChatRoomDialog( ClientFrontend.this ) ).setupGUI();
//				(new JoinChatRoomDialog(ClientFrontend.this)).setupGUI();
//				new JoinChatRoomDialog( );
//				new JDialog( new JoinChatRoomDialog(), "", Dialog.ModalityType.DOCUMENT_MODAL );
			}
		} );
	}

	private void tryJoinChatRoomFromGUI()
	{
//		JoinChatRoomDialog chatRoomDialog = new JoinChatRoomDialog( ClientFrontend.this );
//
//		chatRoomDialog.setVisible( true ); //This doesn't return until setVisible(false) is called, so we're safe on the rest of this method

//		String thisPlayersDisplayName = Client.getInstance().getThisPlayersDisplayName();

		String chatRoomName;
		chatRoomName = (String) JOptionPane.showInputDialog( this, "enter chat-room:", "room name:", JOptionPane.PLAIN_MESSAGE );

		Client.getInstance().tryJoinChatRoom( chatRoomName );
	}

	private void setChatRoomSettingsDialogWindowClosingAdapter()
	{
		if( chatRoomSettingsDialog != null )
			chatRoomSettingsDialog.addWindowListener( new WindowAdapter()
			{
				@Override
				public void windowClosing( WindowEvent e )
				{
					System.out.println( "closing ChatRoomSettingsDialog" );
					chatRoomSettingsDialog = null;
					super.windowClosing( e );
				}
			} );
	}

	public void setupClientFrontEnd()
	{
		menuBar = new JMenuBar();

		JButton menu_ChangeDisplayName = new JButton( "change display name" );
		menu_CreateNewGame = new JButton( "create new game" );

		menu_ChangeDisplayName.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				ClientFrontend.this.menuBar_Pressed_ChangeDisplayName();
			}
		} );
		menu_CreateNewGame.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				ClientFrontend.this.menuBar_Pressed_CreateNewGame();
			}
		} );

		menuBar.add( menu_ChangeDisplayName );
		menuBar.add( menu_CreateNewGame );

		contextMenu = new JPopupMenu();

		chatTabsPanel = new JTabbedPane();

		chatTabsPanel.setTabLayoutPolicy( JTabbedPane.SCROLL_TAB_LAYOUT );

		chatPlayersList = new ChatPlayersList();

		inputField = new JTextField( 40 );
		JButton buttonSubmit = new JButton( "submit" );
		JButton buttonPlay = new JButton( "find game" );

		final JPanel inputPanel = new JPanel();
		inputPanel.setLayout( new BoxLayout( inputPanel, BoxLayout.LINE_AXIS ) );
		inputPanel.add( inputField );
		inputPanel.add( buttonSubmit );
		inputPanel.add( buttonPlay );

		JPanel jPanel_Label_ConsoleCommand = new JPanel();
		jPanel_Label_ConsoleCommand.setLayout( new BoxLayout( jPanel_Label_ConsoleCommand, BoxLayout.LINE_AXIS ) );

		JLabel jLabel = new JLabel( "You: " );
		jPanel_Label_ConsoleCommand.add( jLabel );
		jPanel_Label_ConsoleCommand.add( Box.createHorizontalGlue() );

		JPanel jPanel_InputPanel_And_Label = new JPanel();
		jPanel_InputPanel_And_Label.setLayout( new BoxLayout( jPanel_InputPanel_And_Label, BoxLayout.PAGE_AXIS ) );

		jPanel_InputPanel_And_Label.add( jPanel_Label_ConsoleCommand );
		jPanel_InputPanel_And_Label.add( inputPanel );

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout( new BoxLayout( mainPanel, BoxLayout.PAGE_AXIS ) );

		mainPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

		mainPanel.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = 1;
		c.weightx = 0.9;
		mainPanel.add( chatTabsPanel, c );

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		c.weighty = 1;
		c.weightx = 0.01;
		mainPanel.add( chatPlayersList, c );

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 2;
		c.weighty = 0;
		c.anchor = GridBagConstraints.PAGE_END;
		mainPanel.add( jPanel_InputPanel_And_Label, c );

		buttonSubmit.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				sendMessageToChatRoom( inputField.getText() );
			}
		} );
		chatTabsPanel.addChangeListener( new ChangeListener()
		{
			@Override
			public void stateChanged( ChangeEvent e )
			{
				changeVisibleRoom( chatTabsPanel.getTitleAt( chatTabsPanel.getSelectedIndex() ) );
			}
		} );

		this.setJMenuBar( menuBar );

		this.setMinimumSize( new Dimension( 500, 300 ) );

		this.setTitle( "Advance Warfare - Client " );
		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.add( mainPanel );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );

		PopupListener popupListener = new PopupListener();
		addMouseListener( popupListener );

		setupMenuBar_ChatMenu();
	}

	/**
	 * Changes the "visible" room that the {@link Client} is in. This relates to the currently selected tab, and handles updating the players-in-room list.
	 * This calls {@link ClientFrontend#changeVisibleRoom(String, boolean)} with its requestPlayersInRoomList boolean true
	 *
	 * @param roomName name of the room that is now visible
	 */
	public void changeVisibleRoom( String roomName )
	{
		changeVisibleRoom( roomName, true );
	}

	/**
	 * Changes the "visible" room that the {@link Client} is in. This relates to the currently selected tab, and handles updating the players-in-room list.
	 *
	 * @param roomName                 the name of the room that is now visible
	 * @param requestPlayersInRoomList should we request a new list of the players in the room? This is for if we're joining a room for the first time
	 *                                 since the list will be provided for us.
	 */
	public void changeVisibleRoom( String roomName, boolean requestPlayersInRoomList )
	{
		//Check that we're not already in the desired room, and that a tab with that ChatRoom actually exists
		if( !currentVisibleChatRoom.equals( roomName ) && findChatRoomTabIndexByTitle( roomName ) != -1 )
		{
			currentVisibleChatRoom = roomName;
			chatTabsPanel.setSelectedIndex( findChatRoomTabIndexByTitle( roomName ) );

			checkChatRoomPanelForDirt( (ChatRoomPanel) chatTabsPanel.getComponentAt( chatTabsPanel.getSelectedIndex() ) );

			if( requestPlayersInRoomList )
				Client.getInstance().requestPlayersInCurrentChatRoomList();

			menuItem_ChangeChatRoomSettings.setEnabled( Client.getInstance().isRoomOwner( currentVisibleChatRoom ) );
		}
	}

	public String getCurrentVisibleChatRoom()
	{
		return currentVisibleChatRoom;
	}

	public void setCurrentVisibleChatRoom( String currentVisibleChatRoom )
	{
		this.currentVisibleChatRoom = currentVisibleChatRoom;
	}

	class PopupListener extends MouseAdapter
	{
		public void mousePressed( MouseEvent e )
		{
			maybeShowPopup( e );
		}

		public void mouseReleased( MouseEvent e )
		{
			maybeShowPopup( e );
		}

		private void maybeShowPopup( MouseEvent e )
		{
			if( e.isPopupTrigger() )
				contextMenu.show( e.getComponent(), e.getX(), e.getY() );
		}
	}
}