package nz.zhang.gui;

import nz.zhang.client.Client;
import nz.zhang.gui.listmodels.PlayerListModel;
import nz.zhang.server.Server;
import nz.zhang.utils.PlayerNameOnlineComparator;
import nz.zhang.utils.PlayerStatus;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * ChatPlayersList
 * <p>
 * Created by G-Rath on 10/05/2015.
 */
public class ChatPlayersList extends JPanel
{
	private JPopupMenu contextMenu_PlayerList;

	/**
	 * List of players - showing one of three list models
	 */
	private JList<String> playersList;

	private PlayerListModel<String> playerListModel_AllOnline = new PlayerListModel<>();
	private PlayerListModel<String> playerListModel_Friends = new PlayerListModel<>();
	private PlayerListModel<String> playerListModel_ChatRoom = new PlayerListModel<>();

	private JButton listModeButton;

	private JScrollPane playerListScroll;

	private String listMode = "all-online";

	public ChatPlayersList()
	{
		setupUI();
	}

	public ArrayList<String> getAllPlayersOnline()
	{
		ArrayList<String> playersOnline = new ArrayList<>();

		for( int i = 0; i < playerListModel_AllOnline.getSize(); i++ )
		{
			playersOnline.add( playerListModel_AllOnline.getElementAt( i ) );
		}

		return playersOnline;
	}

	public void rebuildFriendsList(HashSet<String> friendList)
	{
		playerListModel_Friends.clear();

		for( String name : friendList )
		{
			System.out.println( "\t" + name + " : " + Client.getInstance().getPlayerDataByTrueName( name ).toString() );
			playerListModel_Friends.add( name );
		}
	}

	public boolean isPlayerOnline(String playerName)
	{
		return playerListModel_AllOnline.containsElementWithString( playerName );
	}

	public void updatePlayersList(JSONObject playersList, String topic)
	{
		ArrayList<String> playerArrayList = buildArrayOfPlayerPairsFromJSON( playersList );
		playerArrayList.sort( new PlayerNameOnlineComparator() );

		switch( topic )
		{
			case "in-room":
				playerListModel_ChatRoom.clear();
				playerListModel_ChatRoom.addAll( playerArrayList );
				break;

			case "friends":
				playerListModel_Friends.clear();
				playerListModel_Friends.addAll( playerArrayList );
				break;

			case "all-online":
				playerListModel_AllOnline.clear();
				playerListModel_AllOnline.addAll( playerArrayList );
				break;

			default:
				System.out.println( "Trying to update players-list but unknown topic given: " + topic );
				break;
		}
	}

	public ArrayList<String> buildArrayOfPlayerPairsFromJSON(JSONObject playersList)
	{
		ArrayList<String> arrayList = new ArrayList<>();
		Iterator<?> keys = playersList.keys();

		while( keys.hasNext() )
		{
			String key = (String) keys.next();

			arrayList.add( key );
		}

		return arrayList;
	}

	public void changeListMode(String newMode)
	{
		if( !listMode.equals( newMode ) )
		{
			listMode = newMode;

			switch( listMode )
			{
				case "in-room":
					listModeButton.setText( "in-room" );
					playersList.setModel( playerListModel_ChatRoom );
					//listMode = "friends";
					break;
				case "friends":
					listModeButton.setText( "friends" );
					playersList.setModel( playerListModel_Friends );
					//listMode = "all-online";
					break;
				case "all-online":
					listModeButton.setText( "all-online" );
					playersList.setModel( playerListModel_AllOnline );
					//listMode = "in-room";
					break;
			}
		}
	}

	public void switchToNextListMode()
	{
		switch( listMode )
		{
			case "in-room":
				changeListMode( "friends" );
				break;
			case "friends":
				changeListMode( "all-online" );
				break;
			case "all-online":
				changeListMode( "in-room" );
				break;
		}
	}

	public void setupUI()
	{
		contextMenu_PlayerList = new JPopupMenu();
		contextMenu_PlayerList.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

		playersList = new JList<>( playerListModel_AllOnline );
		playersList.setFocusable( false );

		PlayerListRenderer renderer = new PlayerListRenderer();

		//noinspection unchecked
		playersList.setCellRenderer( renderer );

		playersList.setFont( Server.FONT_MONO_NORMAL_BOLD );

		playerListScroll = new JScrollPane( playersList );
		playerListScroll.setMinimumSize( new Dimension( 200, 100 ) );

		listModeButton = new JButton( "players online" );
		listModeButton.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				switchToNextListMode();
			}
		} );

		this.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		c.weightx = 0.5;

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = 0;
		c.insets = new Insets( 1, 1, 1, 1 );
		this.add( listModeButton, c );

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 1;
		c.weighty = 1;
		this.add( playerListScroll, c );

		PopupListener popupListener = new PopupListener();

		playersList.addMouseListener( popupListener );
//		addMouseListener( popupListener );

		contextMenu_PlayerList.add( new JMenuItem( "hello" ) );
	}

	public void setupListContextMenu()
	{

	}

	public void updatePlayerListContextMenu()
	{
//		System.out.println( playersList.getSelectedValue() );

		contextMenu_PlayerList.removeAll();

//		try
//		{
		JSONObject data = Client.getInstance().getPlayerDataByTrueName( playersList.getSelectedValue() );

		if( Client.getInstance().isThisPlayer( playersList.getSelectedValue() ) )
		{
			contextMenu_PlayerList.add( new JLabel( "You" ) );
		}
		else
		{
			//Add/Remove friend-menu option goes first...
			if ( Client.getInstance().isFriendOfPlayer( playersList.getSelectedValue() ) )
			{
				JMenuItem item = new JMenuItem( "remove as friend..." );
				item.addActionListener( new ActionListener()
				{
					@Override
					public void actionPerformed( ActionEvent e )
					{
						Client.getInstance().tryRemovePlayerAsFriend( playersList.getSelectedValue() );
					}
				} );

				contextMenu_PlayerList.add( item );
			}
			else
			{
				JMenuItem item = new JMenuItem( "add as friend..." );
				item.addActionListener( new ActionListener()
				{
					@Override
					public void actionPerformed( ActionEvent e )
					{
						Client.getInstance().tryAddPlayerAsFriend( playersList.getSelectedValue() );
					}
				} );

				contextMenu_PlayerList.add( item );
			}

//			//If we're selecting on the "in-room" list then we don't want to invite players since they are already in the room.
//			if( !listMode.equals( "in-room" ) && )
//			{
//
//			}

			PlayerStatus status = Client.getInstance().getPlayerStatusByTrueName(playersList.getSelectedValue() );

			switch ( status )
			{
				case OFFLINE: //Nothing special
					break;
				case AWAY: //Nothing special
					break;
				case ONLINE: //Allow invite-to-game
				case LOOKING_FOR_GAME:
					if( Client.getInstance().inLobby() && Client.getInstance().doesLobbyHaveFreeSlot() )
					{
						JMenuItem item = new JMenuItem( "invite to game" );
						item.addActionListener( new ActionListener()
						{
							@Override
							public void actionPerformed( ActionEvent e )
							{
								Client.getInstance().tryHandleInvitingPlayerToGame( playersList.getSelectedValue() );
							}
						} );
					}
					break;
				case IN_GAME: //Nothing special
					break;
			}
		}

		contextMenu_PlayerList.pack();
//
//		} catch( JSONException e )
//		{
//			e.printStackTrace();
//		}
	}

	class PopupListener extends MouseAdapter
	{
		public void mousePressed(MouseEvent e)
		{
			maybeShowPopup( e );

//			System.out.print( playersList.locationToIndex( e.getLocationOnScreen() ) );

//			if( playersList.locationToIndex( e.getLocationOnScreen() )
//			{
//			}
		}

		public void mouseReleased(MouseEvent e)
		{
			maybeShowPopup( e );
		}

		private void maybeShowPopup(MouseEvent e)
		{
			if( e.isPopupTrigger() )
			{
				playersList.setSelectedIndex( playersList.locationToIndex( e.getPoint() ) ); //select the item
				contextMenu_PlayerList.show( playersList, e.getX(), e.getY() ); //and show the menu
				updatePlayerListContextMenu();
			}
		}
	}
}