package nz.zhang.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * PlayerTeamPanel
 * <p/>
 * Created by G-Rath on 22/05/2015.
 */
public class PlayerTeamPanel extends JPanel
{
	/** The true name of the {@code Player} that this panel is showing data for */
	private String playerTrueName = "";
	/** can the user interact with us, or are we just showing them data? */
	private boolean isReadOnly = false;
	/** Is the client this panel is showing for the host of the game lobby? Effects some read-only fields. */
	private boolean isClientHost = false;
	/** Is this panel showing the host? */
	private boolean isShowingHost = false;

	private JLabel labelPlayerName;
	private JLabel panelStatus;
	private JToggleButton buttonReady;

	private JComboBox<String> comboBoxModeSelection;

	public PlayerTeamPanel()
	{
		this( true );
	}

	public PlayerTeamPanel( boolean openForPlayer )
	{
		this( openForPlayer, true );
	}

	public PlayerTeamPanel( boolean openForPlayer, boolean isReadOnly )
	{
		this( openForPlayer, isReadOnly, false );
	}

	public PlayerTeamPanel( boolean openForPlayer, boolean isReadOnly, boolean isClientHost )
	{
		this.isReadOnly = isReadOnly;
		this.isClientHost = isClientHost;

		setupGUI();

		if( openForPlayer )
		{
			comboBoxModeSelection.setSelectedItem( "open" );
		}
		else
			comboBoxModeSelection.setSelectedItem( "closed" );
	}

	public PlayerTeamPanel( String playerTrueName )
	{
		this.playerTrueName = playerTrueName;
	}

	public void addComboBoxModeSelectionChangedActionListener( ActionListener listener )
	{
		comboBoxModeSelection.addActionListener( listener );
	}

	public void changeIsClientHost( boolean isClientHost )
	{
		setIsClientHost( isClientHost );

		if( !isShowingHost && this.isClientHost )
			comboBoxModeSelection.setEnabled( true );
		else
			comboBoxModeSelection.setEnabled( false );
	}

	public void setIsClientHost( boolean isClientHost )
	{
		this.isClientHost = isClientHost;
	}

	public void setIsReadOnly( boolean isReadOnly )
	{
		this.isReadOnly = isReadOnly;
	}

	public String getPlayerTrueName()
	{
		return playerTrueName;
	}

	public void setPlayerTrueName( String playerTrueName )
	{
		this.playerTrueName = playerTrueName;
	}

	public boolean hasPlayer()
	{
		return !playerTrueName.equals( "" ); //If the player-true-name is "" then this panel doesn't have a player
	}

	public boolean isFree()
	{
		return comboBoxModeSelection.getSelectedItem().equals( "open" );
	}

	public void setupGUI()
	{
		labelPlayerName = new JLabel( "true-name" );
		buttonReady = new JToggleButton( "ready" );
		buttonReady.setAlignmentX( Component.CENTER_ALIGNMENT );
		buttonReady.setPreferredSize( new Dimension( 100, 25 ) );
//
//		JPanel panel = new JPanel();
//		panel.setLayout( new BoxLayout( panel, BoxLayout.LINE_AXIS ) );
//
//		panel.add( Box.createHorizontalGlue() );
//		panel.add( buttonReady );
//		panel.add( Box.createHorizontalGlue() );

		comboBoxModeSelection = new JComboBox<>();

		comboBoxModeSelection.addItem( "open" );
		comboBoxModeSelection.addItem( "closed" );

		comboBoxModeSelection.setEnabled( isClientHost && !isShowingHost );
		buttonReady.setEnabled( !isReadOnly );

//		JPanel dropDownBoxPanel = new JPanel();
//		{
//			dropDownBoxPanel.setLayout( new GridBagLayout() );
//			GridBagConstraints c = new GridBagConstraints();
//
//			c.gridx = 0;
//			c.gridy = 0;
//			c.weightx = 1;
//			c.weighty = 0;
//
//			dropDownBoxPanel.add( comboBoxModeSelection );
//		}

//		{
//			this.setLayout( new GridBagLayout() );
//			GridBagConstraints c = new GridBagConstraints();
//
//			c.gridy = 0;
//			c.weightx = 1;
//			c.weighty = 0;
//
//			{
//				c.gridx = 0;
////				this.add( Box.createVerticalGlue(), c );
//
//				c.gridx = 1;
//				c.anchor = GridBagConstraints.PAGE_START;
//				this.add( labelPlayerName, c );
//
//				c.gridx = 2;
////				this.add( Box.createVerticalGlue(), c );
//			}
//
//			c.gridy = 1;
//			{
//				c.gridx = 0;
////				this.add( Box.createVerticalGlue(), c );
//
//				c.gridx = 1;
//				c.anchor = GridBagConstraints.PAGE_START;
//				this.add( comboBoxModeSelection, c );
//
//				c.gridx = 2;
////				this.add( Box.createVerticalGlue(), c );
//			}
//
//			c.gridy = 2;
//			{
//				c.gridx = 0;
//				this.add( Box.createHorizontalGlue(), c );
//
//				c.gridx = 1;
//				this.add( Box.createHorizontalGlue(), c );
//
//				c.gridx = 2;
//				this.add( Box.createHorizontalGlue(), c );
//			}
//
//			c.gridy = 3;
//			{
//				c.gridx = 0;
//				this.add( Box.createVerticalGlue(), c );
//
//				c.gridx = 1;
//				c.anchor = GridBagConstraints.PAGE_END;
//				this.add( buttonReady, c );
//
//				c.gridx = 2;
//				this.add( Box.createVerticalGlue(), c );
//			}
//		}

		JPanel topPanel = new JPanel();
		{
			topPanel.setLayout( new GridBagLayout() );
			GridBagConstraints c = new GridBagConstraints();

			c.gridx = 0;
			c.gridy = 0;

			topPanel.add( labelPlayerName, c );

			c.gridx = 0;
			c.gridy = 1;

			c.weightx = 1;
			c.weighty = 0;
			topPanel.add( comboBoxModeSelection, c );
		}

//		topPanel.setBorder( BorderFactory.createLineBorder( Color.BLACK ) );

//		topPanel.setAlignmentY( Component.TOP_ALIGNMENT );

		{
			this.setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );

			this.add( topPanel );
			this.add( Box.createVerticalGlue() );
			this.add( buttonReady );
		}

		this.setBorder( BorderFactory.createLineBorder( Color.BLACK, 1 ) );

		buttonReady.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				if( buttonReady.isSelected() )
				{
					PlayerTeamPanel.this.tryBeReady();
					buttonReady.setText( "unready" );
				}
				else
				{
					PlayerTeamPanel.this.tryBeUnready();
					buttonReady.setText( "ready" );
				}
			}
		} );

		labelPlayerName.setText( playerTrueName );
	}

	public void tryBeReady()
	{
		System.out.println( "Player is ready!" );
	}

	public void tryBeUnready()
	{
		System.out.println( "Player is un-ready!" );
	}

	public void setupAsReadOnly()
	{

	}

	public boolean isClientHost()
	{
		return isClientHost;
	}

	public void setIsShowingForHost( boolean isShowingForHost )
	{
		this.isClientHost = isShowingForHost;
	}

	public boolean isShowingHost()
	{
		return isShowingHost;
	}

	public void setIsShowingHost( boolean isShowingHost )
	{
		this.isShowingHost = isShowingHost;

		if( this.isShowingHost )
		{
			comboBoxModeSelection.removeAllItems();
			comboBoxModeSelection.addItem( "host" );
			comboBoxModeSelection.setEnabled( !this.isShowingHost );
		}
	}

	public void setAsThisPlayer( String playerTrueName )
	{
		this.playerTrueName = playerTrueName;

		labelPlayerName.setText( playerTrueName );

		isReadOnly = false;
		buttonReady.setEnabled( !isReadOnly );
	}
}
