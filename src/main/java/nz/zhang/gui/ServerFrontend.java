package nz.zhang.gui;

import javax.swing.*;
import java.awt.*;
import java.util.List;

//import java.util.List;

/**
 * ServerFrontend - Provides an interactive GUI for the Server when not running in HEADLESS mode.
 * <p>
 * Created by G-Rath on 3/05/2015.
 */
public class ServerFrontend extends JFrame
{
	/** Panel that lists all games currently in play */
	private GameList gameList;
	/** Panel that lists all players currently connected */
	private PlayerList playerList;
	/** Panel that holds the server console, listing console output and providing console input */
	private ServerConsole serverConsole;

	public ServerFrontend() throws HeadlessException
	{
		setupServerFrontend();
	}

	/** Pass-though method that calls clear on {@code PlayerList}'s {@code PlayerListModel} in order to clear it and remove all elements in the model */
	public void clearPlayerListDisplayModel()
	{
		playerList.clearDisplayModel();
	}

	/**
	 * Pass-though method that has the PlayerList add all elements in the passed list collection
	 *
	 * @param list collection of elements to add to the PlayerList
	 */
	public void addAllToPlayerListDisplayModel( List<String> list )
	{
		playerList.addListToDisplayModel( list );
	}

	/**
	 * Pass-though method that has the {@link GameList} add all elements in the passed {@code List} collection to its {@code Game} list-model.
	 *
	 * @param list collection of elements to add to the {@code GameList}
	 */
	public void addAllToGameListDisplayModel( List<String> list )
	{
		gameList.addListToDisplayModel( list );
	}

	/** Pass-though method that calls clear on {@code GameList}'s {@code GameListModel} in order to clear it and remove all elements in the model */
	public void clearGameListDisplayModel()
	{
		gameList.clearDisplayModel();
	}

	/** Calls repaint on all paintable components to have them re-drawTile themselves */
	public void repaintComponents()
	{
		repaint();
	}

	/** Writes a line to the {@code Server}'s output console */
	public void writeToConsole( String line )
	{
		serverConsole.writeLine( line );
	}

	public void setupServerFrontend()
	{
		gameList = new GameList( new BorderLayout() );
		playerList = new PlayerList( new BorderLayout() );

		JPanel jPanel_VerticalList = new JPanel();
		jPanel_VerticalList.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		c.weightx = 0.5;
		c.weighty = 0.5;

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		jPanel_VerticalList.add( gameList, c );

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 1;
		c.gridy = 0;
		jPanel_VerticalList.add( playerList, c );

		serverConsole = new ServerConsole();

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout( new BoxLayout( mainPanel, BoxLayout.PAGE_AXIS ) );

		mainPanel.add( jPanel_VerticalList );
		mainPanel.add( Box.createVerticalStrut( 10 ) );
		mainPanel.add( serverConsole );

		mainPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

//		sendBtn.addActionListener( new ActionListener()
//		{
//			@Override
//			public void actionPerformed( ActionEvent e )
//			{
//				if( !Objects.equals( inputField.getText(), "" ) )
//				{
//					gameList.addGameDisplayString( inputField.getText() );
//					inputField.setText( "" );
//				}
//			}
//		} );

		//JFrame frame = new JFrame( "Foo" );
		this.setMinimumSize( new Dimension( 1000, 600 ) );

		this.setTitle( "Advance Warfare - Server " );
		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.add( mainPanel );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );
	}

	/** Clears the selections of all lists */
	public void clearListSelections()
	{
		gameList.clearGameListSelection();
		playerList.clearPlayerListSelection();
		serverConsole.clearConsoleOutputListSelection();
	}
}
