package nz.zhang.gui;

import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;
import java.awt.*;
import java.awt.event.*;

/**
 * ChatTabHead
 * <p>
 * Created by G-Rath on 10/05/2015.
 */
public class ChatTabHead extends JPanel
{
	private int unreadMessages = 0;

	private final static MouseListener BUTTON_MOUSE_LISTENER = new MouseAdapter()
	{
		public void mouseEntered( MouseEvent e )
		{
			Component component = e.getComponent();
			if( component instanceof AbstractButton )
			{
				AbstractButton button = (AbstractButton) component;
				button.setBorderPainted( true );
			}
		}

		@Override
		public void mousePressed( MouseEvent e )
		{
			super.mousePressed( e );
		}

		public void mouseExited( MouseEvent e )
		{
			Component component = e.getComponent();
			if( component instanceof AbstractButton )
			{
				AbstractButton button = (AbstractButton) component;
				button.setBorderPainted( false );
			}
		}
	};

	/** The TabbedPane that holds us */
	private final JTabbedPane pane;
	/** Can this {@code ChatTabHead} be closed? (If it can be, then the the {@code ChatRoom} it relates to can be left) */
	private boolean canBeClosed = true;

	private JLabel textLabel;

	public ChatTabHead( final JTabbedPane pane )
	{
		this( pane, true );
	}

	public ChatTabHead( final JTabbedPane pane, boolean canBeClosed )
	{
		//unset default FlowLayout' gaps
		super( new FlowLayout( FlowLayout.LEFT, 0, 0 ) );

		if( pane == null )
			throw new NullPointerException( "TabbedPane is null" );

		this.pane = pane;
		this.canBeClosed = canBeClosed;

		setupTabHead();
	}

	public String getTabbedPaneText()
	{
		int i = pane.indexOfTabComponent( ChatTabHead.this );

		if( i != -1 )
			return pane.getTitleAt( i );

		return null;
	}

	public void setupTabHead()
	{
		setOpaque( false );

		textLabel = new JLabel() //Read the titles from the JTabbedPane
		{
			public String getText()
			{
				return getTabbedPaneText() + ( unreadMessages > 0 ? " (" + unreadMessages + ")" : "" );
			}
		};

		add( textLabel );

		if( canBeClosed )
		{
			//add more space between the label and the button
			textLabel.setBorder( BorderFactory.createEmptyBorder( 0, 0, 0, 5 ) );

			JButton button = new TabButton(); //tab button

			add( button );
		}

		//add more space to the top of the component
		setBorder( BorderFactory.createEmptyBorder( 2, 0, 0, 0 ) );
	}

	/**
	 * Updates the title of this {@code ChatTabHead} to reflect the number of unread-messages in the {@code ChatRoom} this {@code ChatTabHead}
	 * holds. If the number of messages is 0
	 *
	 * @param numberOfUnreadMessages the number of messages that are unread for this {@code ChatPanel}
	 */
	public void updateTitleForUnreadMessages( int numberOfUnreadMessages )
	{
		textLabel.setText( "dirty" );
		unreadMessages = numberOfUnreadMessages;
		textLabel.setText( getTabbedPaneText() + ( unreadMessages > 0 ? " (" + unreadMessages + ")" : "" ) );
	}

	private class TabButton extends JButton implements ActionListener
	{
		public TabButton()
		{
			int size = 12;
			setPreferredSize( new Dimension( size, size ) );
			setToolTipText( "close" );

			setUI( new BasicButtonUI() ); //Make the button looks the same for all Look and Feels
			setContentAreaFilled( false );

			setFocusable( false );
			setBorder( BorderFactory.createEtchedBorder() );
			setBorderPainted( false );

			addMouseListener( BUTTON_MOUSE_LISTENER );
			setRolloverEnabled( true );

			addActionListener( this ); //Close the proper tab by clicking the button
		}

		public void actionPerformed( ActionEvent e )
		{
			int i = pane.indexOfTabComponent( ChatTabHead.this );
			if( i != -1 )
			{
				pane.remove( i );
			}
		}

		//we don't want to update UI for this button
		public void updateUI()
		{
		}

		//paint the cross
		protected void paintComponent( Graphics g )
		{
			super.paintComponent( g );
			Graphics2D g2 = (Graphics2D) g.create();

			if( getModel().isPressed() )
				g2.translate( 1, 1 ); //shift the image for pressed buttons

			g2.setStroke( new BasicStroke( 2 ) );
			g2.setColor( Color.GRAY );

			if( getModel().isRollover() )
				g2.setColor( Color.DARK_GRAY );

			int delta = 9;
			g2.drawLine( delta, delta, getWidth() - delta - 1, getHeight() - delta - 1 );
			g2.drawLine( getWidth() - delta - 1, delta, delta, getHeight() - delta - 1 );
			g2.dispose();
		}
	}
}
