package nz.zhang.gui;

import nz.zhang.gui.listmodels.ConsoleListModel;
import nz.zhang.server.Server;
import nz.zhang.utils.SmartScroller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Objects;

/**
 * ServerConsole
 * <p>
 * Created by G-Rath on 5/05/2015.
 */
public class ServerConsole extends JPanel
{
	private JTextField inputField;

	private JList<String> listOfConsoleOutput;

	private ConsoleListModel<String> consoleOutputModel = new ConsoleListModel<>();

	private JScrollPane consoleScroll;

//	public ServerConsole( LayoutManager layout, boolean isDoubleBuffered )
//	{
//		super( layout, isDoubleBuffered );
//		setupConsolePanel();
//	}
//
//	public ServerConsole( LayoutManager layout )
//	{
//		super( layout );
//		setupConsolePanel();
//	}
//
//	public ServerConsole( boolean isDoubleBuffered )
//	{
//		super( isDoubleBuffered );
//		setupConsolePanel();
//	}

	public ServerConsole()
	{
		setupConsolePanel();
	}

	/**
	 * Submits a console command to the server for execution.
	 * This doesn't have to be in JSON format - If its not it will be parsed before hand.
	 *
	 * @param command command to pass
	 */
	private void submitCommand( String command )
	{
		command += " "; //Add a space to the end of the string so that we don't have to worry about words-within-words when looking for spaces

		if( command.startsWith( "repaint " ) )
		{
			Server.getInstance().repaintUI();
		}
		else if( command.startsWith( "ping " ) )
		{
			command = command.substring( command.indexOf( ' ' ) );
			if( command.contains( " " ) )
			{
				String pingFlag;

				if( command.indexOf( '"' ) != -1 && command.indexOf( '"', command.indexOf( '"' ) ) != -1 )
					pingFlag = command.substring( command.indexOf( '"' ) + 1, command.indexOf( '"', command.indexOf( '"' ) + 1 ) );
				else
					pingFlag = command.substring( command.indexOf( ' ' ) + 1 );

				System.out.println( "\t" + pingFlag );

				Server.getInstance().pingPlayer( pingFlag );
			}
			else
				writeLine( "> Ping > Incorrect usage" );
		}
		else if( command.startsWith( "list " ) )
		{
			command = command.substring( command.indexOf( ' ' ) ); //remove the "list " part of the command
			Server.getInstance().listToConsole( command.trim() );
		}
		else if( command.startsWith( "announce " ) )
		{
			command = command.substring( command.indexOf( ' ' ) );

			if( command.contains( " " ) )
			{
				String message;

				if( command.indexOf( '"' ) != -1 && command.indexOf( '"', command.indexOf( '"' ) ) != -1 )
					message = command.substring( command.indexOf( '"' ) + 1, command.indexOf( '"', command.indexOf( '"' ) + 1 ) );
				else
					message = command.substring( command.indexOf( ' ' ) + 1 );

				System.out.println( "\tAnnouncement:" + message );
				Server.getInstance().sendAnnouncement( message );
			}
			else
				writeLine( "> announce > Incorrect usage" );
		}
//		else if( command.startsWith( "message-room " ) )
//		{
//			command = command.substring( command.indexOf( ' ' ) );
//
//			if( command.contains( " " ) )
//			{
//				String roomName;
//				if( command.contains( "room:" ) )
//				{
//					roomName = command.substring( command.indexOf( "room:" ) + "room:".length(), command.indexOf( ' ' ) );
//				}
//				else
//				{
//					writeLine( "> message-room > no room given" );
//					return;
//				}
//
//				if( !Server.getInstance().doesChatRoomExist( roomName ) )
//				{
//					writeLine( "> message-room > room '" + roomName + "' doesn't exist" );
//					return;
//				}
//
//				String message;
//
//				if( command.indexOf( '"' ) != -1 && command.indexOf( '"', command.indexOf( '"' ) ) != -1 )
//					message = command.substring( command.indexOf( '"' ) + 1, command.indexOf( '"', command.indexOf( '"' ) + 1 ) );
//				else
//					message = command.substring( command.indexOf( ' ' ) + 1 );
//
//				System.out.println( "\t" + message );
//
//
//			}
//			else
//				writeLine( "> message-room > Incorrect usage" );
//		}
	}

	/**
	 * Writes a line to the server console.
	 *
	 * @param line string to be written
	 */
	public void writeLine( String line )
	{
		clearConsoleOutputListSelection();
		consoleOutputModel.add( line );
	}

	/** Clears the selection of the listOfConsoleOutput JList */
	public void clearConsoleOutputListSelection()
	{
		listOfConsoleOutput.clearSelection();
	}

	private void setupConsolePanel()
	{
		//@todo retrieve commands when pressing up and down keys in JTextField (Like other consoles, such as CMD)
		//@todo have JScrollPanel scroll down to newest entry automatically
		//@todo look to make JList wrap text

//		consoleOutputModel.add( "game 1" );
		listOfConsoleOutput = new JList<>( consoleOutputModel );
//		listOfConsoleOutput.setFocusable( false );

		listOfConsoleOutput.setFont( Server.FONT_MONO_NORMAL_BOLD );

		consoleScroll = new JScrollPane( listOfConsoleOutput );

		new SmartScroller( consoleScroll, SmartScroller.VERTICAL, SmartScroller.END );

		inputField = new JTextField( 40 );
		JButton buttonSubmit = new JButton( "submit" );

		final JPanel inputPanel = new JPanel();
		inputPanel.setLayout( new BoxLayout( inputPanel, BoxLayout.LINE_AXIS ) );
		inputPanel.add( inputField );
		inputPanel.add( buttonSubmit );

		JPanel jPanel_Label_ConsoleCommand = new JPanel();
		jPanel_Label_ConsoleCommand.setLayout( new BoxLayout( jPanel_Label_ConsoleCommand, BoxLayout.LINE_AXIS ) );

		JLabel jLabel = new JLabel( "Console Command: " );
		jPanel_Label_ConsoleCommand.add( jLabel );
		jPanel_Label_ConsoleCommand.add( Box.createHorizontalGlue() );

		JPanel jPanel_InputPanel_And_Label = new JPanel();
		jPanel_InputPanel_And_Label.setLayout( new BoxLayout( jPanel_InputPanel_And_Label, BoxLayout.PAGE_AXIS ) );

		jPanel_InputPanel_And_Label.add( jPanel_Label_ConsoleCommand );
		jPanel_InputPanel_And_Label.add( inputPanel );

		this.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		c.weightx = 0.5;

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;
		c.gridy = 0;
		c.weighty = 1;
		this.add( consoleScroll, c );

		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 1;
		c.weighty = 0;
		c.anchor = GridBagConstraints.PAGE_END;
		this.add( jPanel_InputPanel_And_Label, c );

		inputField.addKeyListener( new KeyListener()
		{
			@Override
			public void keyTyped( KeyEvent e )
			{
			}

			@Override
			public void keyPressed( KeyEvent e )
			{
				System.out.println( "test " + e.getKeyCode() );

				switch( e.getKeyCode() )
				{
					case KeyEvent.VK_UP:
					{
						if( listOfConsoleOutput.isSelectionEmpty() && consoleOutputModel.getSize() > 0 )
							listOfConsoleOutput.setSelectedIndex( consoleOutputModel.getSize() - 1 );
						else if( listOfConsoleOutput.getSelectedIndex() - 1 >= 0 )
							listOfConsoleOutput.setSelectedIndex( listOfConsoleOutput.getSelectedIndex() - 1 );

						String selectedValue = listOfConsoleOutput.getSelectedValue();

						if( selectedValue.startsWith( ">> " ) )
							selectedValue = selectedValue.substring( ">> ".length() );

						inputField.setText( selectedValue );

						break;
					}

					case KeyEvent.VK_DOWN:
					{
//						System.out.println( consoleOutputModel.getSize() - 1 );

						if( listOfConsoleOutput.isSelectionEmpty() && consoleOutputModel.getSize() > 0 )
							listOfConsoleOutput.setSelectedIndex( 0 );
						else if( listOfConsoleOutput.getSelectedIndex() + 1 < consoleOutputModel.getSize() )
							listOfConsoleOutput.setSelectedIndex( listOfConsoleOutput.getSelectedIndex() + 1 );

						String selectedValue = listOfConsoleOutput.getSelectedValue();

//						consoleScroll.

						if( selectedValue.startsWith( ">> " ) )
							selectedValue = selectedValue.substring( ">> ".length() );

						inputField.setText( selectedValue );

						break;
					}
				}
			}

			@Override
			public void keyReleased( KeyEvent e )
			{

			}
		} );

		listOfConsoleOutput.addKeyListener( new KeyListener()
		{
			@Override
			public void keyTyped( KeyEvent e )
			{
				inputField.setText( inputField.getText() + e.getKeyChar() );
				inputField.requestFocus();
			}

			@Override
			public void keyPressed( KeyEvent e )
			{

			}

			@Override
			public void keyReleased( KeyEvent e )
			{

			}
		} );

		buttonSubmit.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				ServerConsole.this.consoleInputFieldSubmit();
			}
		} );
		inputField.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				ServerConsole.this.consoleInputFieldSubmit();
			}
		} );
	}

	private void consoleInputFieldSubmit()
	{
		//System.out.println( "blah" );

		if( !Objects.equals( inputField.getText(), "" ) )
		{
			System.out.println( inputField.getText() );

			writeLine( ">> " + inputField.getText() );

			submitCommand( inputField.getText() );

			inputField.setText( "" );
		}
	}
}
