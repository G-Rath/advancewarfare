package nz.zhang.gui;

import nz.zhang.client.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * SetupGame
 * <p>
 * Created by G-Rath on 22/05/2015.
 */
public class GameLobby extends JFrame
{
	/** HashMap of the players that are in this game Lobby */
	private HashMap<String, PlayerTeamPanel> playerPanels = new HashMap<>();

	private PlayerTeamPanel playerPanel_Red; //red-team is always host
	private PlayerTeamPanel playerPanel_Blue;
	private PlayerTeamPanel playerPanel_Yellow;
	private PlayerTeamPanel playerPanel_Green;

	private boolean lobbyOwner = false;

	private String gameID;

	private JButton buttonStartGame;

	private String selectedMapName = "none";

	private JComboBox<String> comboBoxMapSelection;

	private ArrayList<String> mapList = new ArrayList<>();

	public GameLobby( boolean lobbyOwner, ArrayList<String> mapList, String gameID ) throws HeadlessException
	{
		this.lobbyOwner = lobbyOwner;
		this.mapList = mapList;
		this.gameID = gameID;

		setupGameLobby();
	}

	public void playerEnterLobby( String playerName, String team )
	{
		switch( team )
		{
			case "red": //red-team
//				playerPanel_Red.setPlayerTrueName( playerName );
				setupForPerspective( playerName, 0 );
				break;
			case "blue": //blue-team
				setupForPerspective( playerName, 1 );
				break;
			case "yellow": //yellow-team
				setupForPerspective( playerName, 2 );
				break;
			case "green": //green-team
				setupForPerspective( playerName, 3 );
				break;
			default:
				break;
		}
	}

	public boolean hasFreePlayerSlot()
	{
		return playerPanel_Blue.isFree() || playerPanel_Green.isFree() || playerPanel_Yellow.isFree();
	}

	public void playerLeaveLobby( String playerName )
	{

	}

	public void closeSlot( int slotNumber )
	{

	}

	public void enterLobby( String playerName )
	{

	}

	public void leaveLobby( String playerName )
	{

	}

	public void setupForPerspective( String playerTrueName, int spot )
	{
		switch( spot )
		{
			case 0:
				if( lobbyOwner )
				{
					playerPanel_Red.setAsThisPlayer( playerTrueName );
					playerPanel_Red.setIsShowingHost( true );
				}
				else
					System.out.println( "GameLobby - setupForPerspective: player tried to take owners spot without being the owner" );

				break;

			case 1:
				playerPanel_Blue.setAsThisPlayer( playerTrueName );
				break;

			case 2:
				playerPanel_Yellow.setAsThisPlayer( playerTrueName );
				break;

			case 3:
				playerPanel_Green.setAsThisPlayer( playerTrueName );
				break;
		}
	}

	public void setupForOwner()
	{
//		System.out.println( "lobbyOwner = " + lobbyOwner );

		playerPanel_Red.changeIsClientHost( lobbyOwner );
		playerPanel_Blue.changeIsClientHost( lobbyOwner );
		playerPanel_Yellow.changeIsClientHost( lobbyOwner );
		playerPanel_Green.changeIsClientHost( lobbyOwner );

		comboBoxMapSelection.setEnabled( lobbyOwner );
		buttonStartGame.setEnabled( lobbyOwner );
	}

	public void setupGameLobby()
	{
		playerPanel_Red = new PlayerTeamPanel();
		playerPanel_Blue = new PlayerTeamPanel();
		playerPanel_Yellow = new PlayerTeamPanel();
		playerPanel_Green = new PlayerTeamPanel();

		buttonStartGame = new JButton( "submit" );
		buttonStartGame.setAlignmentX( Component.RIGHT_ALIGNMENT );

		comboBoxMapSelection = new JComboBox<String>();

		for( String s : mapList )
		{
			comboBoxMapSelection.addItem( s );
		}

		JLabel label_Map = new JLabel( "Map:" );

		JPanel mapPanel = new JPanel();
		{
			mapPanel.setLayout( new BoxLayout( mapPanel, BoxLayout.LINE_AXIS ) );

			mapPanel.add( label_Map );
			mapPanel.add( comboBoxMapSelection );
		}

		JPanel submitButtonPanel = new JPanel();
		{
			submitButtonPanel.setLayout( new BoxLayout( submitButtonPanel, BoxLayout.LINE_AXIS ) );

			submitButtonPanel.add( buttonStartGame );
		}

		JPanel settingsPanel = new JPanel();
		{
			settingsPanel.setLayout( new BoxLayout( settingsPanel, BoxLayout.PAGE_AXIS ) );

			settingsPanel.add( mapPanel );
			settingsPanel.add( Box.createVerticalStrut( 5 ) );
			settingsPanel.add( submitButtonPanel );
		}

		JPanel playerPanels = new JPanel();
		{
			playerPanels.setLayout( new BoxLayout( playerPanels, BoxLayout.LINE_AXIS ) );

			playerPanels.add( playerPanel_Red );
			playerPanels.add( Box.createHorizontalStrut( 5 ) );
			playerPanels.add( playerPanel_Blue );
			playerPanels.add( Box.createHorizontalStrut( 5 ) );
			playerPanels.add( playerPanel_Yellow );
			playerPanels.add( Box.createHorizontalStrut( 5 ) );
			playerPanels.add( playerPanel_Green );
		}

		JPanel mainPanel = new JPanel();
		{
			mainPanel.setLayout( new GridBagLayout() );
			GridBagConstraints c = new GridBagConstraints();

			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 1;
			c.weighty = 1;
			c.insets = new Insets( 5, 5, 5, 5 );
			mainPanel.add( playerPanels, c );

			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 1;
			c.weightx = 1;
			c.weighty = 0.1;
			mainPanel.add( Box.createVerticalStrut( 5 ), c );

			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 2;
			c.weightx = 1;
			c.weighty = 0;
			mainPanel.add( settingsPanel, c );
		}

		buttonStartGame.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				GameLobby.this.tryStartGame();
			}
		} );
		comboBoxMapSelection.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				GameLobby.this.mapSelectionChanged();
			}
		} );

		setupForOwner();
		selectedMapName = (String) comboBoxMapSelection.getSelectedItem();

		this.setTitle( "Advance Warfare - Game Lobby " );
		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.setMinimumSize( new Dimension( 300, 300 ));
		this.add( mainPanel );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );
	}

	public void mapSelectionChanged()
	{
		selectedMapName = (String) comboBoxMapSelection.getSelectedItem();
		Client.getInstance().tryChangeSelectedMapForGame( gameID,selectedMapName );
	}

	public void tryStartGame()
	{
		//@todo checks that everyone is ready
		Client.getInstance().startGame( selectedMapName );
		System.out.println( "trying to start game" );
	}

	public String getGameID()
	{
		return gameID;
	}

	public void setGameID( String gameID )
	{
		this.gameID = gameID;
	}
}
