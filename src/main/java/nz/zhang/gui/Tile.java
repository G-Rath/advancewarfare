package nz.zhang.gui;

import nz.zhang.units.Unit;

import java.awt.*;

/**
 * Tile class - represents one grid space
 * <p>
 * Created by G-Rath on 30/04/2015.
 */
public class Tile
{
	/** The Unit which is currently occupying this tile, if any */
	private Unit occupant = null;
	/** This tiles grid location */
	private Point gridLoc;

	public Unit getOccupant()
	{
		return occupant;
	}

	public void setOccupant( Unit occupant )
	{
		this.occupant = occupant;
	}

	public float getDefRating()
	{
		return 0;
	}
}
