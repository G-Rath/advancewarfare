package nz.zhang.gui;

import nz.zhang.client.Client;
import nz.zhang.gui.listmodels.PlayerListModel;
import nz.zhang.server.Server;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 * ChatRoomSettingsDialog
 * <p>
 * Created by G-Rath on 25/05/2015.
 */
public class ChatRoomSettingsDialog extends JFrame
{
	private PlayerListModel<String> allowedPlayersModel = new PlayerListModel<>();

	private JList<String> allowedPlayersList;

	private JTextField jTextField_RoomName;

	private String roomName = "";

	private JCheckBox checkBox_IsPrivateSettings;

	private JSONObject chatRoomData;

	private boolean creatingRoom = false;

	private JButton button_AddPlayerToAllowedList;
	private JButton button_RemovePlayerFromAllowedList;
	private JButton button_SubmitSettings;
	private JLabel label_FeedbackLabel;

	public ChatRoomSettingsDialog( boolean creatingRoom, String roomName, JSONObject data )
	{
		this.creatingRoom = creatingRoom;
		this.roomName = roomName;

		setupForChatRoom( data );
	}

	private boolean isPrivate()
	{
		try
		{
			return chatRoomData.getBoolean( "private" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return false;
	}

	public void setupForChatRoom( JSONObject data )
	{
		if( data == null )
		{
			try
			{
				chatRoomData = new JSONObject();
				chatRoomData.put( "owner", Client.getInstance().thisPlayersTrueName() );
				chatRoomData.put( "private", true );

				chatRoomData.put( "allowed-players", new ArrayList<String>() );
				chatRoomData.put( "players-in-chat", new ArrayList<String>() );
			}
			catch( JSONException e )
			{
				e.printStackTrace();
			}
		}
		else
		{
			chatRoomData = data;

//			try
//			{
//				data.getBoolean( "" );
//			}
//			catch( JSONException e )
//			{
//				e.printStackTrace();
//			}

			//fillAllowedPlayersModel();
		}

		setupGUI();
	}

	public void fillAllowedPlayersModel()
	{
		System.out.println( "reloading allowed-players model" );
		allowedPlayersModel.clear();

		try
		{
			JSONArray jArray = chatRoomData.getJSONArray( "allowed-players" );
			for( int i = 0; i < jArray.length(); i++ )
			{
				if( !jArray.getString( i ).equals( chatRoomData.getString( "owner" ) ) || !jArray.getString( i ).equals( "all" ) )
				{
					allowedPlayersModel.add( jArray.getString( i ) );
				}
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void tryAddPlayerToAllowedList( String playerName )
	{
		//Get the player's true-name. If the provided name is already their true-name then getPlayerTrueNameByDisplayName will return that instead.
		String playerTrueName = Client.getInstance().getPlayerTrueNameByDisplayName( playerName );

		//If the player isn't already in the allowed-players list
		if( !isPlayerOnAllowedList( playerTrueName ) )
		{
			addPlayerToAllowedList( playerTrueName );
			refreshGUI();
		}
		else
			System.out.println( "player '" + playerName + "' is already allowed in this room" );
	}

	public void addPlayerToAllowedList( String trueName )
	{
		try
		{
			JSONArray jArray = chatRoomData.getJSONArray( "allowed-players" );
			jArray.put( trueName );
			chatRoomData.put( "allowed-players", jArray );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void tryRemovePlayerFromAllowedList( String playerName )
	{
		//Get the player's true-name. If the provided name is already their true-name then getPlayerTrueNameByDisplayName will return that instead.
		String playerTrueName = Client.getInstance().getPlayerTrueNameByDisplayName( playerName );

		//If the player the player is on the allowed-list remove them.
		if( isPlayerOnAllowedList( playerTrueName ) )
		{
			removePlayerFromAllowedList( playerTrueName );
			refreshGUI();
		}
	}

	/**
	 * Removes a {@code Player} from the {@code ChatRoom}s JSON object based on their true-name.
	 *
	 * @param trueName true-name of the {@code Player} to remove.
	 */
	public void removePlayerFromAllowedList( String trueName )
	{
		try
		{
			JSONArray jArray = chatRoomData.getJSONArray( "allowed-players" );
			JSONArray newArray = new JSONArray();

			for( int i = 0; i < jArray.length(); i++ )
			{
				if( !jArray.get( i ).equals( trueName ) )
					newArray.put( jArray.get( i ) );
			}

			chatRoomData.put( "allowed-players", newArray );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	private boolean isPlayerOnAllowedList( String playerTrueName )
	{
		try
		{
			JSONArray jArray = chatRoomData.getJSONArray( "allowed-players" );
			for( int i = 0; i < jArray.length(); i++ )
			{
				if( jArray.getString( i ).equals( "all" ) || jArray.getString( i ).equals( playerTrueName ) )
					return true;
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return false;
	}

	public void refreshGUI()
	{
		checkBox_IsPrivateSettings.setSelected( isPrivate() );

		allowedPlayersList.setEnabled( isPrivate() );
		button_AddPlayerToAllowedList.setEnabled( isPrivate() );
		button_RemovePlayerFromAllowedList.setEnabled( isPrivate() );

		jTextField_RoomName.setEnabled( creatingRoom );

		if( creatingRoom )
			button_SubmitSettings.setText( "create" );
		else
			button_SubmitSettings.setText( "submit" );

		fillAllowedPlayersModel();
		listSelectionChanged();
	}

	public void setupGUI()
	{
		allowedPlayersList = new JList<>( allowedPlayersModel );
		allowedPlayersList.setFocusable( false );

		PlayerListRenderer renderer = new PlayerListRenderer();

		//noinspection unchecked
		allowedPlayersList.setCellRenderer( renderer );
		allowedPlayersList.setFont( Server.FONT_MONO_NORMAL_BOLD );

		JScrollPane allowedPlayersScrollPanel = new JScrollPane( allowedPlayersList );

		//RoomName panel
		JLabel label_RoomName = new JLabel( "room-name:" );
		jTextField_RoomName = new JTextField( roomName, 40 );

		JPanel panel_RoomName = new JPanel();

		panel_RoomName.setLayout( new BoxLayout( panel_RoomName, BoxLayout.LINE_AXIS ) );
		panel_RoomName.add( label_RoomName );
		panel_RoomName.add( Box.createHorizontalStrut( 10 ) );
		panel_RoomName.add( jTextField_RoomName );

		//isPrivate setting
		JLabel label_IsPrivateSettings = new JLabel( "private:" );
		checkBox_IsPrivateSettings = new JCheckBox();

		JPanel panel_PrivateSettings = new JPanel();

		panel_PrivateSettings.setLayout( new BoxLayout( panel_PrivateSettings, BoxLayout.LINE_AXIS ) );
		panel_PrivateSettings.add( label_IsPrivateSettings );
		panel_PrivateSettings.add( Box.createHorizontalGlue() );
		panel_PrivateSettings.add( checkBox_IsPrivateSettings );

		//Add and Remove buttons
		button_AddPlayerToAllowedList = new JButton( "add" );
		button_RemovePlayerFromAllowedList = new JButton( "remove" );

		JPanel panel_AddRemoveButtons = new JPanel();

		panel_AddRemoveButtons.setLayout( new BoxLayout( panel_AddRemoveButtons, BoxLayout.LINE_AXIS ) );
		panel_AddRemoveButtons.add( button_AddPlayerToAllowedList );
		panel_AddRemoveButtons.add( Box.createHorizontalStrut( 10 ) );
		panel_AddRemoveButtons.add( button_RemovePlayerFromAllowedList );

		//JList + Add/Remove buttons panel
		JPanel panel_AllowedPlayersList = new JPanel();
		panel_AllowedPlayersList.setLayout( new GridBagLayout() );

		{
			GridBagConstraints c = new GridBagConstraints();

			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 0;
			c.weightx = 0;
			c.weighty = 0;
			panel_AllowedPlayersList.add( new JLabel( "allowed players: " ), c );

			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 1;
			c.weightx = 1;
			c.weighty = 1;
			panel_AllowedPlayersList.add( allowedPlayersScrollPanel, c );

			c.fill = GridBagConstraints.BOTH;
			c.gridx = 0;
			c.gridy = 2;
			c.weightx = 0;
			c.weighty = 0;
			panel_AllowedPlayersList.add( Box.createVerticalStrut( 5 ), c );

			c.fill = GridBagConstraints.CENTER;
			c.gridx = 0;
			c.gridy = 3;
			c.weightx = 1;
			c.weighty = 0;
			panel_AllowedPlayersList.add( panel_AddRemoveButtons, c );
		}

		//Submit button and feedback label panel
		button_SubmitSettings = new JButton( "submit" );
		label_FeedbackLabel = new JLabel( "" );

		JPanel panel_SubmitSettingsButton = new JPanel();

		panel_SubmitSettingsButton.setLayout( new BoxLayout( panel_SubmitSettingsButton, BoxLayout.LINE_AXIS ) );
		panel_SubmitSettingsButton.add( label_FeedbackLabel );
		panel_SubmitSettingsButton.add( Box.createHorizontalGlue() );
		panel_SubmitSettingsButton.add( button_SubmitSettings );

		//Setup mainPanel
		JPanel mainPanel = new JPanel();

		mainPanel.setBorder( new EmptyBorder( 10, 10, 10, 10 ) );

		mainPanel.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		c.weightx = 0.5;
		c.weighty = 0.5;

		c.fill = GridBagConstraints.BOTH;
		c.gridx = 0;

		c.weightx = 0.5;
		c.weighty = 0;
		c.gridy = 0;
		mainPanel.add( panel_RoomName, c );

		c.weightx = 0;
		c.weighty = 0;
		c.gridy = 1;
		mainPanel.add( Box.createVerticalStrut( 10 ), c );

		c.weightx = 0;
		c.weighty = 0;
		c.gridy = 2;
		mainPanel.add( panel_PrivateSettings, c );

		c.weightx = 0;
		c.weighty = 0;
		c.gridy = 3;
		mainPanel.add( Box.createVerticalStrut( 10 ), c );

		c.weightx = 1;
		c.weighty = 1;
		c.gridy = 4;
		mainPanel.add( panel_AllowedPlayersList, c );

		c.weightx = 0;
		c.weighty = 0;
		c.gridy = 5;
		mainPanel.add( Box.createVerticalStrut( 10 ), c );

		c.weightx = 0;
		c.weighty = 0;
		c.gridy = 6;
		mainPanel.add( panel_SubmitSettingsButton, c );

		jTextField_RoomName.getDocument().addDocumentListener( new DocumentListener()
		{
			@Override
			public void insertUpdate( DocumentEvent e )
			{
				System.out.println( "updated" );
			}

			@Override
			public void removeUpdate( DocumentEvent e )
			{
				System.out.println( "removed" );
			}

			@Override
			public void changedUpdate( DocumentEvent e )
			{
				textFieldChanged_RoomName();
			}
		} );

		checkBox_IsPrivateSettings.addActionListener( new AbstractAction()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				checkBoxStateChanged_IsPrivate();
			}
		} );
		button_SubmitSettings.addActionListener( new AbstractAction()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				buttonPressed_Submit();
			}
		} );
		button_AddPlayerToAllowedList.addActionListener( new AbstractAction()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				buttonPressed_AddPlayer();
			}
		} );
		button_RemovePlayerFromAllowedList.addActionListener( new AbstractAction()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				buttonPressed_RemovePlayer();
			}
		} );

		allowedPlayersList.addListSelectionListener( new ListSelectionListener()
		{
			@Override
			public void valueChanged( ListSelectionEvent e )
			{
				listSelectionChanged();
			}
		} );

		this.setMinimumSize( new Dimension( 300, 300 ) );
		this.setPreferredSize( new Dimension( 300, 300 ) );
		this.setTitle( "Advance Warfare - Create new ChatRoom " );
		this.add( mainPanel );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );

		refreshGUI();
	}

	public void listSelectionChanged()
	{
		button_RemovePlayerFromAllowedList.setEnabled( !allowedPlayersList.isSelectionEmpty() );
	}

	/**
	 * Sets this {@code ChatRoom}s "private" status in the {@code JSONObject} for this {@code ChatRoom}.
	 *
	 * @param newStatus the new {@code boolean} value for the private field.
	 */
	private void setRoomPrivateStatusAs( boolean newStatus )
	{
		try
		{
			chatRoomData.put( "private", newStatus );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void checkBoxStateChanged_IsPrivate()
	{
		setRoomPrivateStatusAs( checkBox_IsPrivateSettings.isSelected() );
		refreshGUI();
	}

	public void textFieldChanged_RoomName()
	{
		System.out.println( "\t" + "ChatRoom: " + "new room name is: " + jTextField_RoomName.getText() );
	}

	public void buttonPressed_AddPlayer()
	{
		System.out.println( "\t" + "ChatRoom: " + "add player button" );

		String playerName = (String) JOptionPane.showInputDialog( this,
		                                                          "player name:",
		                                                          "allow player in list",
		                                                          JOptionPane.PLAIN_MESSAGE,
		                                                          null,
		                                                          null,
		                                                          null );

		String result = Client.getInstance().getPlayerTrueNameByDisplayName( playerName );

		if( result != null )
		{
			tryAddPlayerToAllowedList( result );
		}
		else
			System.out.println( "\t" + "ChatRoom: " + "invalid user: " + playerName );
	}

	public void buttonPressed_RemovePlayer()
	{
		System.out.println( "\t" + "ChatRoom: " + "remove player button" );
		tryRemovePlayerFromAllowedList( allowedPlayersList.getSelectedValue() );
	}

	public void buttonPressed_Submit()
	{
		if( creatingRoom && !jTextField_RoomName.getText().equals( "" ) )
		{
			Client.getInstance().requestCheckIfChatRoomNameIsFree( jTextField_RoomName.getText() );

			label_FeedbackLabel.setVisible( true );
			label_FeedbackLabel.setText( "checking room-name..." );
			label_FeedbackLabel.setForeground( Color.BLACK );
			System.out.println( "\t" + "checking room name" );

			//Disable all components while creating the room as any changes won't be passed on mid-creation.
			for( Component component : this.getComponents() )
			{
				component.setEnabled( false );
			}
		}
		else if( jTextField_RoomName.getText().equals( "" ) )
		{
			label_FeedbackLabel.setVisible( true );
			label_FeedbackLabel.setForeground( Color.RED );
			label_FeedbackLabel.setText( "room-name can't be blank" );
		}
		else //By this point creatingRoom is false and jTextField_RoomName isn't blank
		{
			label_FeedbackLabel.setVisible( false );

			this.dispatchEvent( new WindowEvent( this, WindowEvent.WINDOW_CLOSING ) );
			this.dispose();
		}
	}

	/**
	 * Callback method for when the {@code Server} sends a reply-package to the {@code Client} about if a {@code ChatRoom} name is free or not.
	 * The {@code Client} passes on the "reply" {@code Boolean} field from the {@code Server}'s {@code JSONObject}'s "reply" field.
	 *
	 * @param reply {@code true} if the {@code ChatRoom} name is free (no {@code ChatRoom} exists with that name}, {@code false} otherwise.
	 */
	public void checkRoomFreeReply( boolean reply )
	{
		if( reply )
			createRoom();
		else
		{
			label_FeedbackLabel.setVisible( true );
			label_FeedbackLabel.setForeground( Color.RED );
			label_FeedbackLabel.setText( "name already taken" );
		}
	}

	public void createRoom()
	{
		System.out.println( "\t" + "ChatRoom: " + "creating room" );

		label_FeedbackLabel.setVisible( true );
		label_FeedbackLabel.setText( "creating room..." );
		label_FeedbackLabel.setForeground( Color.BLACK );

		Client.getInstance().requestChatRoomCreation( jTextField_RoomName.getText(), chatRoomData );

		this.dispatchEvent( new WindowEvent( this, WindowEvent.WINDOW_CLOSING ) );
		this.dispose();
	}
}