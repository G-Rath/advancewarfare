package nz.zhang.gui;

import nz.zhang.client.Client;
import nz.zhang.server.Server;
import nz.zhang.utils.PlayerStatus;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;

/**
 * PlayerListRenderer
 * <p>
 * Created by G-Rath on 9/05/2015.
 */
public class PlayerListRenderer extends JPanel implements ListCellRenderer
{
	private JLabel mainText;
	private JLabel subText;

	public PlayerListRenderer()
	{
		setupPanel();
	}

	public void setupPanel()
	{
		mainText = new JLabel();
		subText = new JLabel();
		subText.setFont( Server.FONT_MONO_SMALL_BOLD );

		this.setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );

		this.add( mainText );
		this.add( subText );
	}

	@Override
	public Component getListCellRendererComponent( JList list, Object object, int index, boolean isSelected, boolean cellHasFocus )
	{
		if( isSelected )
		{
			setBackground( list.getSelectionBackground() );
			setForeground( list.getSelectionForeground() );
		}
		else
		{
			setBackground( list.getBackground() );
			setForeground( list.getForeground() );
		}

		if( object instanceof String )
		{
			try
			{
				String trueName = (String) object;
				JSONObject playerData = Client.getInstance().getPlayerDataByTrueName( trueName );

				switch( PlayerStatus.fromString( playerData.getString( "status" ) ) ) //Color players based on status
				{
					case OFFLINE:
						mainText.setForeground( Color.GRAY );
						subText.setForeground( Color.GRAY );

						if( isSelected ) //If selected and offline make the color black for better readability.
						{
							mainText.setForeground( Color.BLACK );
							subText.setForeground( Color.BLACK );
						}

						subText.setText( "offline" );
						break;

					case ONLINE:
						mainText.setForeground( Color.DARK_GRAY );
						subText.setForeground( Color.DARK_GRAY );
						subText.setText( "online" );

						if( Client.getInstance().isFriendOfPlayer( trueName ) ) //If the player is a friend is this player make it easier to tell
						{
							mainText.setBackground( Color.DARK_GRAY );
							mainText.setForeground( Color.BLUE );
						}

						break;

					case AWAY:
						mainText.setForeground( Color.LIGHT_GRAY );
						subText.setForeground( Color.LIGHT_GRAY );
						subText.setText( "away" );
						break;

					case LOOKING_FOR_GAME:
						mainText.setForeground( Color.PINK );
						subText.setForeground( Color.PINK );
						subText.setText( "looking for a match" );
						break;

					case IN_GAME:
						mainText.setForeground( Color.ORANGE );
						subText.setForeground( Color.ORANGE );
						subText.setText( "in-game" );
						break;
				}

				mainText.setText( playerData.getString( "display-name" ) );
			}
			catch( JSONException e )
			{
				e.printStackTrace();
			}
		}

		return this;
	}
}