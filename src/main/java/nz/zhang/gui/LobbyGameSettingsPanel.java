package nz.zhang.gui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * LobbyGameSettingsPanel
 * <p>
 * Created by G-Rath on 22/05/2015.
 */
public class LobbyGameSettingsPanel extends JPanel
{
	private boolean readyOnlyMode = false;

	private String selectedMapName = "none";

	private JComboBox<String> comboBoxMapSelection;

	private ArrayList<String> mapList = new ArrayList<>();
	private GameLobby lobby;

	public LobbyGameSettingsPanel( GameLobby lobby )
	{
		this.lobby = lobby;
	}

	public void setupReadOnlyGUI()
	{

	}

	public void setupSettingsGUI()
	{
		JButton buttonStartGame = new JButton( "start game" );

		comboBoxMapSelection = new JComboBox<String>();

		for( String s : mapList )
		{
			comboBoxMapSelection.addItem( s );
		}

		JLabel label_Map = new JLabel( "Map:" );
		JPanel panel_MapSelection = new JPanel();

		panel_MapSelection.setLayout( new BoxLayout( panel_MapSelection, BoxLayout.PAGE_AXIS ) );

		panel_MapSelection.add( label_Map );
		panel_MapSelection.add( comboBoxMapSelection );

		JPanel mainPanel = new JPanel();

		mainPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

		mainPanel.setLayout( new GridBagLayout() );
		GridBagConstraints c = new GridBagConstraints();

		//
//		c.fill = GridBagConstraints.BOTH;
//		c.gridx = 1;
//		c.gridy = 0;
//		c.gridwidth = 1;
//		c.weighty = 1;
//		c.weightx = 0.01;
//		mainPanel.add( chatPlayersList, c );
//
//		c.fill = GridBagConstraints.HORIZONTAL;
//		c.gridx = 0;
//		c.gridy = 1;
//		c.gridwidth = 2;
//		c.weighty = 0;
//		c.anchor = GridBagConstraints.PAGE_END;
//		mainPanel.add( jPanel_InputPanel_And_Label, c );
//
//		buttonStartGame.addActionListener( new ActionListener()
//		{
//			@Override
//			public void actionPerformed( ActionEvent e )
//			{
//				sendMessageToChatRoom( inputField.getText() );
//			}
//		} );
//
//		this.setMinimumSize( new Dimension( 500, 300 ) );
//
//		this.setTitle( "Advance Warfare - Client " );
//		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
//		this.add( mainPanel );
//		this.pack();
//		this.setLocationRelativeTo( null );
//		this.setVisible( true );
//
//		PopupListener popupListener = new PopupListener();
//		addMouseListener( popupListener );
	}
}
