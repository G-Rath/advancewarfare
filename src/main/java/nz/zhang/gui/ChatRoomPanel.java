package nz.zhang.gui;

import nz.zhang.client.Client;
import nz.zhang.gui.listmodels.ChatListModel;
import nz.zhang.server.Server;
import nz.zhang.utils.SmartScroller;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * ChatRoomPanel
 * <p>
 * Created by G-Rath on 10/05/2015.
 */
public class ChatRoomPanel extends JPanel
{
	public String chatName;

	public ChatListModel<String> chatLogModel = new ChatListModel<>();

	public JList chatLogList;

	private ArrayList<JSONObject> chatLog = new ArrayList<>();

	/** Is this panel dirty? represents if its changed since the last time the {@code Client} saw it */
	private boolean dirty = false;
	/** Counter for the number of new messages that has been added to this chat since it was made "dirty" */
	private int numberOfNewMessagesSinceDirty = 0;

	public ChatRoomPanel()
	{
		setupPanel();
	}

	public void addNewMessage( JSONObject messageObject )
	{
		chatLog.add( messageObject );
		reGenerateChatLogModel();

		dirty = true; //Mark this chat as dirty, and increase the count of messages-since-dirty by 1
		numberOfNewMessagesSinceDirty++;
	}

	public void setDirty( boolean dirty )
	{
		this.dirty = dirty;
	}

	public void clean()
	{
		dirty = false;
		numberOfNewMessagesSinceDirty = 0;
	}

	public void reGenerateChatLogModel()
	{
		try
		{
//			ChatListModel<String> tempModel = new ChatListModel<>();
			//chatLogModel.clear();

			ArrayList<String> messageArray = new ArrayList<>();

			for( JSONObject jsonObject : chatLog )
			{
				String message = jsonObject.getString( "message" );
				String sender = "";

				switch( jsonObject.getString( "sender" ) )
				{
					case "announcer":
						sender = "announcement";
						break;

					case "server":
						sender = "server";
						break;

					case "god":
						sender = "god";
						break;

					default:
						sender = Client.getInstance().getPlayerDisplayNameByTrueName( jsonObject.getString( "sender" ) );
						//sender = Client.getInstance().get
				}

				messageArray.add( sender + ": " + message );
			}

			chatLogModel.clear();
			chatLogModel.addAll( messageArray );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void setupPanel()
	{
		this.setLayout( new BorderLayout() );

		chatLogList = new JList<>( chatLogModel );
		chatLogList.setFocusable( false );

		chatLogList.setFont( Server.FONT_MONO_NORMAL_BOLD );

		JScrollPane chatLogListScroll = new JScrollPane( chatLogList );
		this.add( chatLogListScroll );

		new SmartScroller( chatLogListScroll, SmartScroller.VERTICAL, SmartScroller.END );
	}

	public boolean isDirty()
	{
		return dirty;
	}

	public int getNumberOfNewMessagesSinceDirty()
	{
		return numberOfNewMessagesSinceDirty;
	}
}