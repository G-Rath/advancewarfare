package nz.zhang.gui;

import nz.zhang.client.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import static nz.zhang.utils.TextPrinter.getBytes;

/**
 * GetServerConnectionDialog
 * <p>
 * Created by G-Rath on 3/05/2015.
 */
public class GetServerConnectionDialog extends JFrame
{
	private JButton button_Exit;
	private JButton button_Retry;
	private Component rigidArea_ExitRetry_Button;

	public GetServerConnectionDialog() throws HeadlessException
	{
		setupComponents();
	}

	public GetServerConnectionDialog( GraphicsConfiguration gc )
	{
		super( gc );
	}

	public GetServerConnectionDialog( String title ) throws HeadlessException
	{
		super( title );
	}

	public GetServerConnectionDialog( String title, GraphicsConfiguration gc )
	{
		super( title, gc );
	}

	public void failedToGetConnection()
	{
		rigidArea_ExitRetry_Button.setVisible( true );
		button_Retry.setVisible( true );

		button_Retry.setEnabled( true );
	}

	public void retryConnection()
	{
		button_Retry.setEnabled( false );
		Client.getInstance().retryForServerConnection();
	}

	public void setupComponents()
	{
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout( new BoxLayout( mainPanel, BoxLayout.PAGE_AXIS ) );
		mainPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout( new BoxLayout( buttonPanel, BoxLayout.LINE_AXIS ) );

		button_Exit = new JButton( "exit" );
		button_Retry = new JButton( "retry" );

		button_Exit.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				System.exit( 0 );
			}
		} );
		button_Retry.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				retryConnection();
			}
		} );

		rigidArea_ExitRetry_Button = Box.createRigidArea( new Dimension( 10, 0 ) );

		buttonPanel.add( button_Exit );
		buttonPanel.add( rigidArea_ExitRetry_Button );
		buttonPanel.add( button_Retry );

		rigidArea_ExitRetry_Button.setVisible( false );
		button_Retry.setVisible( false );

		JPanel loadingPanel = new JPanel();
		//loadingPanel.setLayout( new BoxLayout( loadingPanel, BoxLayout.PAGE_AXIS ) );
		loadingPanel.setLayout( new BorderLayout() );

		Image image;

		try
		{
			String filePath = "/loading_1.gif";
			InputStream in = getClass().getResourceAsStream( filePath );
			//getBytes( in );

			//image = ImageIO.read( in );
			image = Toolkit.getDefaultToolkit().createImage( getBytes( in ) );

			//BufferedImage buffImage = toBufferedImage( image );
			//image = scale( buffImage, buffImage.getType(), image.getWidth( null ), image.getHeight( null ), 2, 2 );
		}
		catch( IOException e )
		{
			e.printStackTrace();
			return; //If we can't read the loading image, return since we don't want to continue
		}

		Icon icon = new ImageIcon( image );
		JLabel loadingLabel = new JLabel( "connecting to server", icon, JLabel.CENTER );
		loadingLabel.setHorizontalTextPosition( JLabel.CENTER );
		loadingLabel.setVerticalTextPosition( JLabel.BOTTOM );

		loadingPanel.add( loadingLabel, BorderLayout.CENTER );

		mainPanel.add( loadingPanel );
		mainPanel.add( Box.createVerticalStrut( 20 ) );
		mainPanel.add( Box.createVerticalGlue() );
		mainPanel.add( buttonPanel );

		mainPanel.setMinimumSize( new Dimension( 100, 100 ) );
		this.add( mainPanel );

		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.setUndecorated( true );
		this.setResizable( false );
		this.pack();
		this.setLocationRelativeTo( null );
		this.setVisible( true );

		Client.getInstance().setGettingConnectionDialog( this );
//		Client.getInstance().tryForServerConnection();
	}
}
