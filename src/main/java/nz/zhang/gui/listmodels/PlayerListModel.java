package nz.zhang.gui.listmodels;

import nz.zhang.utils.Pair;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * PlayerListModel - Custom list model
 * Created by G-Rath on 1/05/2015.
 */
public class PlayerListModel<T> extends AbstractListModel<T>
{
	private List<T> list;

	public PlayerListModel( List<T> list )
	{
		this.list = list;
	}

	public PlayerListModel()
	{
		this.list = new ArrayList<>();
	}

	@Override
	public int getSize()
	{
		return list.size();
	}

	@Override
	public T getElementAt( int i )
	{
		return list.get( i );
	}

	/**
	 * Adds an element to the list
	 *
	 * @param data element to add
	 */
	public void add( T data )
	{
		list.add( data );
		fireIntervalAdded( this, 0, getSize() );
	}

	/**
	 * Add all the elements from the given list into this model's list
	 *
	 * @param elementsToAdd list of elements to be added
	 */
	public void addAll( List<T> elementsToAdd )
	{
		list.addAll( elementsToAdd );
		fireIntervalAdded( this, 0, getSize() );
	}

	/** Clears the model of all elements */
	public void clear()
	{
		list.clear();
		fireIntervalAdded( this, 0, getSize() );
	}

	public boolean containsElementWithString( String string )
	{
		//@todo comment and explain this method - otherwise this looks like a strange and unsafe/non-standard method for a generic class
		for( T t : list )
		{
			if( t instanceof Pair && ( (Pair) t ).getKey() instanceof String && ( (Pair) t ).getValue() instanceof String )
			{
				Pair pair = (Pair) t;
				String key = (String) pair.getKey();
				String value = (String) pair.getKey();

				if( key.equals( string ) )
					return true;
			}
		}

		return false;
	}
}
