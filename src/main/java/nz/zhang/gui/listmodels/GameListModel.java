package nz.zhang.gui.listmodels;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * GameListModel - Custom list model
 * Created by G-Rath on 1/05/2015.
 */
public class GameListModel<T> extends AbstractListModel<T>
{
	private List<T> list;

	public GameListModel( List<T> list )
	{
		this.list = list;
	}

	public GameListModel()
	{
		this.list = new ArrayList<>();
	}

	@Override
	public int getSize()
	{
		return list.size();
	}

	@Override
	public T getElementAt( int i )
	{
		return list.get( i );
	}

	/**
	 * Adds an element to the list
	 *
	 * @param data element to add
	 */
	public void add( T data )
	{
		list.add( data );
		fireIntervalAdded( this, 0, getSize() );
	}

	/**
	 * Add all the elements from the given list into this model's list
	 *
	 * @param elementsToAdd list of elements to be added
	 */
	public void addAll( List<T> elementsToAdd )
	{
		list.addAll( elementsToAdd );
		fireIntervalAdded( this, 0, getSize() );
	}

	/** Clears the model of all elements */
	public void clear()
	{
		list.clear();
		fireIntervalAdded( this, 0, getSize() );
	}
}
