package nz.zhang.gui;

import nz.zhang.gui.listmodels.PlayerListModel;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * PlayerList
 * <p>
 * Created by G-Rath on 5/05/2015.
 */
public class PlayerList extends JPanel
{
	private JList<String> listOfPlayers;

	private PlayerListModel<String> playerDisplayModel = new PlayerListModel<>();

	public PlayerList( LayoutManager layout, boolean isDoubleBuffered )
	{
		super( layout, isDoubleBuffered );
		setupPlayerList();
	}

	public PlayerList( LayoutManager layout )
	{
		super( layout );
		setupPlayerList();
	}

	public PlayerList( boolean isDoubleBuffered )
	{
		super( isDoubleBuffered );
		setupPlayerList();
	}

	public PlayerList()
	{
		setupPlayerList();
	}

	public void setupPlayerList()
	{
		playerDisplayModel.add( "No Players" );
		listOfPlayers = new JList<>( playerDisplayModel );
		listOfPlayers.setFocusable( false );

		JScrollPane gameScroll = new JScrollPane( listOfPlayers );

		//this.setLayout( new BoxLayout( this, BoxLayout.PAGE_AXIS ) );
		this.add( new JLabel( "Players:", SwingConstants.LEFT ), BorderLayout.PAGE_START );
		this.add( gameScroll );

		//addGameDisplayString( "game 1" );
	}

	public void addPlayerDisplayString( String gameString )
	{
		playerDisplayModel.add( gameString );
		repaintUI();
	}

	/** Clears the selection of the listOfPlayers JList */
	public void clearPlayerListSelection()
	{
		listOfPlayers.clearSelection();
	}

	public void addListToDisplayModel( List<String> list )
	{
		playerDisplayModel.addAll( list );
		repaintUI();
	}

	public void clearDisplayModel()
	{
		playerDisplayModel.clear();
		repaintUI();
	}

	public void repaintUI()
	{
		listOfPlayers.repaint();
	}

}