package nz.zhang.utils;

/**
 * CryptoException - used to throw an error with cryptography instead of throwing one of a handful of exceptions that are possible during cryptography
 * <p>
 * Created by G-Rath on 30/04/2015.
 */
public class CryptoException extends Exception
{

	public CryptoException()
	{
	}

	public CryptoException( String message, Throwable throwable )
	{
		super( message, throwable );
	}
}
