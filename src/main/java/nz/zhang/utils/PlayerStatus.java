package nz.zhang.utils;

/**
 * PlayerStatus
 * <p>
 * Created by G-Rath on 17/05/2015.
 */
public enum PlayerStatus
{
	OFFLINE, AWAY, ONLINE, LOOKING_FOR_GAME, IN_GAME;

	public static PlayerStatus fromString( String status )
	{
		return fromString( status, true );
	}

	public static PlayerStatus fromString( String status, boolean ignoreCase )
	{
		if( ignoreCase ) //Convert to lower-case if we're ignoring cases
			status = status.toLowerCase();

		switch( status.trim() ) //Not  wanting to piss around with leading and trailing spaces, trim without care.
		{
			case "offline":
				return OFFLINE;

			case "away":
				return AWAY;

			case "online":
				return ONLINE;

			case "looking-for-game":
				return LOOKING_FOR_GAME;

			case "in-game":
				return IN_GAME;

			default:
				return null;
		}
	}

	@Override
	public String toString()
	{
		switch( this )
		{
			case OFFLINE:
				return "offline";

			case AWAY:
				return "away";

			case ONLINE:
				return "online";

			case LOOKING_FOR_GAME:
				return "looking-for-game";

			case IN_GAME:
				return "in-game";

			default:
				return "offline";
		}
	}

}
