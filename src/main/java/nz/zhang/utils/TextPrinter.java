package nz.zhang.utils;

import nz.zhang.server.Server;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.AttributedString;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * TextPrinter utility JPanel component for use printing text to screen
 * <p>
 * Created by G-Rath on 13/04/2015.
 */
public class TextPrinter extends JPanel
{
	private ArrayList<String> textLines = new ArrayList<>();

	/** The location to drawTile this TextPrinter */
	private Point location;
	/** The max number of text lines to be keep in the array and hence printed to screen */
	private int maxLines = 10;

	public TextPrinter( Point location, int maxLines )
	{
		this.location = location;
		this.maxLines = maxLines;
	}

	/**
	 * Gets the current system time in the date format "HH:mm:ss" ( Hour, minutes, seconds)
	 *
	 * @return - Current time as string
	 */
	public static String getCurrentTime()
	{
		return new SimpleDateFormat( "HH:mm:ss" ).format( Calendar.getInstance().getTime() );
	}

	/**
	 * scales an image using AffineTransform by the given x/y factors. Stored here until a better single class is free to hold it
	 *
	 * @param sbi       image to scale
	 * @param imageType type of image
	 * @param dWidth    width of destination image
	 * @param dHeight   height of destination image
	 * @param fWidth    x-factor for transformation / scaling
	 * @param fHeight   y-factor for transformation / scaling
	 *
	 * @return scaled image
	 */
	public static BufferedImage scale( BufferedImage sbi, int imageType, int dWidth, int dHeight, double fWidth, double fHeight )
	{
		BufferedImage dbi = null;
		if( sbi != null )
		{
			dbi = new BufferedImage( (int) ( dWidth * fWidth ), (int) ( dHeight * fHeight ), imageType );
			Graphics2D g = dbi.createGraphics();
			AffineTransform at = AffineTransform.getScaleInstance( fWidth, fHeight );
			g.drawRenderedImage( sbi, at );
		}
		return dbi;
	}

	/**
	 * Converts a given Image into a BufferedImage
	 * <a href="http://stackoverflow.com/questions/13605248/java-converting-image-to-bufferedimage">source</a>
	 *
	 * @param img The Image to be converted
	 *
	 * @return The converted BufferedImage
	 */
	public static BufferedImage toBufferedImage( Image img )
	{
		if( img instanceof BufferedImage )
		{
			return (BufferedImage) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage( img.getWidth( null ), img.getHeight( null ), BufferedImage.TYPE_INT_ARGB );

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage( img, 0, 0, null );
		bGr.dispose();

		// Return the buffered image
		return bimage;
	}

	public static byte[] getBytes( InputStream inputStream ) throws IOException
	{
		int bufferLength;
		int bufferSize = 1024;
		byte[] buffer;

		//CHeck if the InputStream is an instance of ByteArrayInputStream
		if( inputStream instanceof ByteArrayInputStream )
		{
			bufferSize = inputStream.available();
			buffer = new byte[bufferSize];

			//noinspection UnusedAssignment
			bufferLength = inputStream.read( buffer, 0, bufferSize );
		}
		else
		{
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			buffer = new byte[bufferSize];

			//While there is information in the buffer, write to the ByteArrayOutputStream
			while( ( bufferLength = inputStream.read( buffer, 0, bufferSize ) ) != -1 )
			{
				byteArrayOutputStream.write( buffer, 0, bufferLength );
			}

			buffer = byteArrayOutputStream.toByteArray();
		}

		return buffer;
	}

	public void addText( String textToAdd )
	{
		textLines.add( "[" + getCurrentTime() + "] " + textToAdd );

		if( textLines.size() > maxLines )
			textLines.remove( 0 );
	}

	public void doDrawing( Graphics g )
	{
		g.setFont( Server.FONT_MONO_NORMAL_BOLD );

		Graphics2D g2d = (Graphics2D) g;

		g2d.setRenderingHint( RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON );

		//g2d.drawString( text, location.width, location.height );
		g2d.setColor( Color.WHITE );

		Point pen = new Point( location.x, location.y );
		FontRenderContext frc = g2d.getFontRenderContext();

		for( int i = textLines.size() - 1; i >= 0; i-- )
		{
			String line = textLines.get( i );
			LineBreakMeasurer measurer = new LineBreakMeasurer( new AttributedString( line ).getIterator(), frc );
			float wrappingWidth = getSize().width - 15;

			while( measurer.getPosition() < line.length() )
			{
				TextLayout layout = measurer.nextLayout( wrappingWidth );

				pen.y -= ( layout.getAscent() );
				float dx = layout.isLeftToRight() ? 0 : ( wrappingWidth - layout.getAdvance() );

				layout.draw( g2d, pen.x + dx, pen.y );
				pen.y -= layout.getDescent() + layout.getLeading();
			}
		}

		Toolkit.getDefaultToolkit().sync(); //Sync the swing so that any animations we do will be smooth on Linux
		g2d.dispose();                      //Dispose the copy of the Graphic we made
	}

	@Override
	public void paintComponent( Graphics g )
	{
		super.paintComponent( g );
		doDrawing( g );
	}
}