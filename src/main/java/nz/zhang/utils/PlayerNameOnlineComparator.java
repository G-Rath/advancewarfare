package nz.zhang.utils;

import nz.zhang.client.Client;
import nz.zhang.server.Player;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;

/**
 * PlayerNameOnlineComparator - Sorts {@link Player} data by getting the {@code Player} JSON data via {@link Client#getPlayerDataByTrueName(String)}.
 * The comparison is done by the {@link PlayerStatus} field of the {@link JSONObject}.
 * <p>
 * This class is very specific, assuming that the {@code Pair<String, String>} holds a {@code Player}s true-name for its key, and a {@link JSONObject}
 * as a string for its value.
 * <p>
 * Created by G-Rath on 17/05/2015.
 */
public class PlayerNameOnlineComparator implements Comparator<String>
{
	@Override
	public int compare( String firstName, String secondName )
	{
		try
		{
			JSONObject firstPlayerData = Client.getInstance().getPlayerDataByTrueName( firstName );
			JSONObject secondPlayerData = Client.getInstance().getPlayerDataByTrueName( secondName );

			PlayerStatus firstStatus = PlayerStatus.fromString( firstPlayerData.getString( "status" ) );
			PlayerStatus secondStatus = PlayerStatus.fromString( secondPlayerData.getString( "status" ) );

			return firstStatus.compareTo( secondStatus );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return 0;
	}
}