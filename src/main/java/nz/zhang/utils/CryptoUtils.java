package nz.zhang.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.Key;

/**
 * CryptoUtils class - Provides functions for encrypting and decrypting files.
 * Built with aid from <a href="http://www.codejava.net/coding/file-encryption-and-decryption-simple-example">codejava.net</a>
 * <p>
 * Created by G-Rath on 30/04/2015.
 */
public class CryptoUtils
{
	private static final String ALGORITHM = "AES";
	private static final String TRANSFORMATION = "AES";

	public static void encrypt( String key, File inputFile, File outputFile ) throws CryptoException
	{
		doCrypto( Cipher.ENCRYPT_MODE, key, inputFile, outputFile );
	}

	public static void decrypt( String key, File inputFile, File outputFile ) throws CryptoException
	{
		doCrypto( Cipher.DECRYPT_MODE, key, inputFile, outputFile );
	}

	private static void doCrypto( int cipherMode, String key, File inputFile, File outputFile ) throws CryptoException
	{
		try
		{
			Key secretKey = new SecretKeySpec( key.getBytes(), ALGORITHM );
			Cipher cipher = Cipher.getInstance( TRANSFORMATION );
			cipher.init( cipherMode, secretKey );

			FileInputStream inputStream = new FileInputStream( inputFile );
			byte[] inputBytes = new byte[(int) inputFile.length()];
			inputStream.read( inputBytes );

			byte[] outputBytes = cipher.doFinal( inputBytes );

			FileOutputStream outputStream = new FileOutputStream( outputFile );
			outputStream.write( outputBytes );

			inputStream.close();
			outputStream.close();

		}
		catch( Exception e )
		{
			throw new CryptoException( "Error encrypting/decrypting file", e );
		}
	}
}