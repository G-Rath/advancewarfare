package nz.zhang.utils;

/**
 * Pair
 * <p>
 * Created by G-Rath on 15/05/2015.
 */
public class Pair<K, V>
{
	private K key;
	private V value;

	public Pair( K key, V value )
	{
		this.key = key;
		this.value = value;
	}

	public K getKey()
	{
		return key;
	}

	public V getValue()
	{
		return value;
	}

	public void setValue( V value )
	{
		this.value = value;
	}
}
