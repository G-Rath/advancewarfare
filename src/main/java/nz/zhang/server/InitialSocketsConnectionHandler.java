package nz.zhang.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * InitialSocketsConnectionHandler
 * <p>
 * Created by G-Rath on 5/05/2015.
 */
public class InitialSocketsConnectionHandler implements Runnable
{
	private ServerSocket serverSocket;

	private boolean acceptNewSockets = false;

	private boolean alive = true;

	public InitialSocketsConnectionHandler( ServerSocket serverSocket )
	{
		this.serverSocket = serverSocket;
		startAcceptingConnections();
	}

	public void stopAcceptingConnections()
	{
		acceptNewSockets = false;
	}

	public void startAcceptingConnections()
	{
		acceptNewSockets = true;
	}

	/** Causes this InitialSocketsConnectionHandler to stop "living", meaning that the thread will terminate */
	public void die()
	{
		alive = false;
	}

	@Override
	public void run()
	{
		while( alive )
		{
			if( acceptNewSockets )
			{
				try
				{
					Socket clientSocket = serverSocket.accept();
					Server.getInstance().passSocket( clientSocket );
				}
				catch( IOException e )
				{
					e.printStackTrace();
				}
			}
		}
	}
}
