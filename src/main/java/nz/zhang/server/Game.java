package nz.zhang.server;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Game - represents an instance of a game between players
 * <p/>
 * Created by G-Rath on 1/05/2015.
 */
public class Game implements Runnable
{
	/** Unique game id */
	private String gameID = "";

	private String gameName = "game";

	/** If we're not playing, then we're in the lobby */
	private boolean playing = false;

	private boolean alive = true;

	private String hostPlayer = "";

	private String selectedMap = "none";

	private Player player_Red;
	private Player player_Blue;
	private Player player_Yellow;
	private Player player_Green;

	public Game( String hostPlayer )
	{
		this.hostPlayer = hostPlayer;
	}

	@Override
	public void run()
	{
		while( alive )
		{
			while( playing )
			{

			}
		}
	}

	public String getFirstFreeSlot()
	{
		if( player_Red == null )
			return "red";
		else if( player_Blue == null)
			return "blue";
		else if( player_Yellow == null)
			return "yellow";
		else if( player_Green == null )
			return "green";
		else
			return "none";
	}

	public void invitePlayerToGame( Player playerToInvite )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "supply-players-in-room-list" );
			command.put( "team-name", getFirstFreeSlot() );
			command.put( "game-id", gameID );

			playerToInvite.writeToSocket( command.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void kickPlayer( Player player )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "kicked-from-game" );
			command.put( "game-id", gameID );

			player.writeToSocket( command.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void kickPlayerInTeam( String team )
	{
		switch( team )
		{
			case "red": //red-team
				kickPlayer( player_Red );
				break;
			case "blue": //blue-team
				kickPlayer( player_Blue );
				break;
			case "yellow": //yellow-team
				kickPlayer( player_Yellow );
				break;
			case "green": //green-team
				kickPlayer( player_Green );
				break;
			default:
				break;
		}
	}

	public void playerJoinGame( Player player )
	{
		playerJoinGame( player, getFirstFreeSlot() );
	}

	public void playerJoinGame( Player player, String team )
	{
		switch( team )
		{
			case "red": //red-team
				player_Red = player;
				break;
			case "blue": //blue-team
				player_Blue = player;
				break;
			case "yellow": //yellow-team
				player_Yellow = player;
				break;
			case "green": //green-team
				player_Green = player;
				break;
			default:
				break;
		}

		tellPlayerTheyJoinedGame( player, team );
		updateGameInformation();
	}

	private void tellPlayerTheyJoinedGame( Player player, String teamName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "reply", "joined-game" );
			command.put( "result", true );
			command.put( "game-id", gameID );
			command.put( "team-name", teamName );

			player.writeToSocket( command.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void changeSelectedMap( String newSelectedMap )
	{
		selectedMap = newSelectedMap;

		updateGameInformation();
	}

	public void updateGameInformation()
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "update-game-information" );
			command.put( "game-id", gameID );

			command.put( "map", selectedMap );

			command.put( "player-red", ( player_Red != null ? player_Red.getTrueName() : "" ) );
			command.put( "player-blue", ( player_Blue != null ? player_Blue.getTrueName() : "" ) );
			command.put( "player-yellow", ( player_Yellow != null ? player_Yellow.getTrueName() : "" ) );
			command.put( "player-green", ( player_Green != null ? player_Green.getTrueName() : "" ) );

			sendCommandToPlayers( command.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void startGame( String mapName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "game-command", "pre-start-game" );
			command.put( "map-name", mapName );

			command.put( "player-red", ( player_Red != null ? player_Red.getTrueName() : "" ) );
			command.put( "player-blue", ( player_Blue != null ? player_Blue.getTrueName() : "" ) );
			command.put( "player-yellow", ( player_Yellow != null ? player_Yellow.getTrueName() : "" ) );
			command.put( "player-green", ( player_Green != null ? player_Green.getTrueName() : "" ) );

			sendCommandToPlayers( command.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void sendCommandToPlayers( String command )
	{
		if( player_Red != null )
			player_Red.writeToSocket( command );

		if( player_Blue != null )
			player_Blue.writeToSocket( command );

		if( player_Yellow != null )
			player_Yellow.writeToSocket( command );

		if( player_Green != null )
			player_Green.writeToSocket( command );
	}

	public void changeSelectedMap( JSONObject data )
	{
		try
		{
			String mapName = data.getString( "map-name" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Checks if a given string is the name of a map, and so a valid map that can be used and loaded from file.
	 *
	 * @param mapName name of the map to check for
	 *
	 * @return {@code true} if the given name is for a valid map-file, {@code false} otherwise.
	 */
	public boolean isValidMap( String mapName )
	{
		return false;
	}

	public String getGameName()
	{
		return gameName;
	}

	public void setGameName( String gameName )
	{
		this.gameName = gameName;
	}

	public String getGameID()
	{
		return gameID;
	}

	public void setGameID( String gameID )
	{
		this.gameID = gameID;
	}
}
