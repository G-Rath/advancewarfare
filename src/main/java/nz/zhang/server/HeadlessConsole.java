package nz.zhang.server;

import java.util.Scanner;

/**
 * HeadlessConsole - Allows for console command input to the server while running in Headless mode (Without a GUI).
 * This class runs on a separate thread using the "Scanner" class of Java
 * <p>
 * Created by G-Rath on 14/05/2015.
 */
public class HeadlessConsole implements Runnable
{
	private Scanner console;

	private boolean scan = true;

	public HeadlessConsole()
	{
		setupScanner();
	}

	private void setupScanner()
	{
		console = new Scanner( System.in );
	}

	@Override
	public void run()
	{
		while( scan )
		{
			if( console.hasNextLine() )
			{
				String command = console.nextLine();
				Server.getInstance().writeToConsole( ">> " + command );
				submitCommand( command );
			}
		}
	}

	/**
	 * Submits a console command to the server for execution.
	 * This doesn't have to be in JSON format - If its not it will be parsed before hand.
	 *
	 * @param command command to pass
	 */
	private void submitCommand( String command )
	{
		command += " "; //Add a space to the end of the string so that we don't have to worry about words-within-words when looking for spaces

		if( command.startsWith( "repaint " ) )
		{
			Server.getInstance().writeToConsole( "That command is invalid when HEADLESS" );
		}
		else if( command.startsWith( "ping " ) )
		{
			command = command.substring( command.indexOf( ' ' ) ); //Remove the "ping " part of the command
			if( command.contains( " " ) )
			{
				String pingFlag;

				if( command.indexOf( '"' ) != -1 && command.indexOf( '"', command.indexOf( '"' ) ) != -1 )
					pingFlag = command.substring( command.indexOf( '"' ) + 1, command.indexOf( '"', command.indexOf( '"' ) + 1 ) );
				else
					pingFlag = command.substring( command.indexOf( ' ' ) + 1 );

				System.out.println( "\t" + pingFlag );

				Server.getInstance().pingPlayer( pingFlag );
			}
			else
				Server.getInstance().writeToConsole( "> Ping > Incorrect usage" );
		}
		else if( command.startsWith( "list " ) )
		{
			command = command.substring( command.indexOf( ' ' ) ); //remove the "list " part of the command
			Server.getInstance().listToConsole( command.trim() );
		}
	}
}
