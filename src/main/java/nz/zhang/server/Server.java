package nz.zhang.server;

import nz.zhang.client.Client;
import nz.zhang.gui.ServerFrontend;
import nz.zhang.utils.PlayerStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.List;

/**
 * Server
 * <p/>
 * Created by G-Rath on 29/04/2015.
 */
public class Server
{
	//@todo create error codes to shorten bandwidth

	public final static Charset UTF8 = Charset.forName( "UTF-8" );

	public final static String HOST_NAME = "";

	public final static String FOLDER_ROOT = "server/";
	public final static String FOLDER_ACCOUNTS = FOLDER_ROOT + "accounts/";
	public final static String FOLDER_CHATROOM = FOLDER_ROOT + "chat-rooms/";
	public final static String FILE_CHATROOM_GLOBAL = FOLDER_CHATROOM + "global-chat.json";

	public final static int SERVER_PORT = 1225; //As picked from "Baby Kaisius"

	public static final Font FONT_MONO_LARGE_BOLD = new Font( "Monospaced", Font.BOLD, 20 );
	public static final Font FONT_MONO_NORMAL_BOLD = new Font( "Monospaced", Font.BOLD, 12 );
	public static final Font FONT_MONO_SMALL_BOLD = new Font( "Monospaced", Font.BOLD, 10 );
	public final static HashSet<String> RESERVED_DISPLAY_NAMES = new HashSet<>( Arrays.asList( "server", "announcer", "god" ) );
	private final static Server INSTANCE = new Server();
	private static boolean printToSystem = false;

	@SuppressWarnings( "FieldCanBeLocal" )
	private final int screenX = 640;
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int screenY = 640;

	/** Counter of the number of clients that have connected in this session since the server started */
	private int clientCounter = 0;
	/** Counter of the number of games that have been created in this session since the server started */
	private int gameCounter = 0;
	/** The {@code Server}s connection socket, used for handling initial {@code Client} connections */
	private ServerSocket serverSocket;

	/** ArrayList of all players that are currently connected */
	private ArrayList<Player> playerList = new ArrayList<>();
	/** ArrayList of all games that are currently active (including lobbies) */
	private ArrayList<Game> gamesList = new ArrayList<>();
	/** Server GUI */
	private ServerFrontend frontend;

	private InitialSocketsConnectionHandler socketsConnectionHandler;

	/** HashMap containing the data-objects for all chatRooms. Mapped to the chat-rooms file name */
	private HashMap<String, ChatRoom> chatRooms = new HashMap<>();
	/** HashSet of all "true names" of all user accounts - Used for referencing vs having to access files in a loop all the time */
	private HashMap<String, JSONObject> playerDataMap = new HashMap<>();

	private Server()
	{
	}

	public static Server getInstance()
	{
		return INSTANCE;
	}

	public static void main( String[] args )
	{
		Server.getInstance().setupSever();
	}

	public static void writeToPlayerFile( JSONObject playerData, String trueName )
	{
		try( BufferedWriter writer = Files.newBufferedWriter( Paths.get( Server.FOLDER_ACCOUNTS + trueName + ".json" ), Charset.forName( "UTF-8" ), StandardOpenOption.TRUNCATE_EXISTING ) )
		{
			writer.write( playerData.toString() );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Pings a {@code Player} - the {@code Player's} {@link Client} should response with a "pong". Can be used to test connection.
	 * This method takes a string which is used to find the {@code Player} - Because of this the method can be called through the Server console.
	 *
	 * @param playerName the true-name of the player to ping.
	 */
	public void pingPlayer( String playerName )
	{
		try
		{
			JSONObject pingCommand = new JSONObject();
			pingCommand.put( "command", "ping" );

			if( playerName.equals( "all" ) )
			{
				for( Player player : playerList )
				{
					writeToConsole( "Server: @" + player.getDisplayName() + " > Ping!" );
					player.writeToSocket( pingCommand.toString() );
				}
			}
			else
			{
				Player player = findPlayer( playerName );

				if( player != null )
				{
					writeToConsole( "Server: @" + player.getDisplayName() + ": Ping!" );
					player.writeToSocket( pingCommand.toString() );
				}
				else
					writeToConsole( "Server - Ping @ " + playerName + " : Error, unable to find player" );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Finds a {@code Player} based on their true-name.
	 *
	 * @param trueName true-name of the {@code Player} we wish to find.
	 *
	 * @return {@code Player} instance of the {@code Player} with the given true-name, {@code NULL} if no {@code Player} exists with that true-name
	 */
	public Player getPlayerByTrueName( String trueName )
	{
		for( Player player : playerList )
		{
			if( player.getDisplayName().equals( trueName ) )
			{
				return player;
			}
		}

		return null;
	}

	public Player findPlayer( String playerName )
	{
		return getPlayerByTrueName( playerName );
	}

	/**
	 * Sets up the {@code Server} for handling {@code Player}s, {@code ChatRoom}s, and {@code Game}s, along with loading in the required file data.
	 * <p/>
	 * The setup order is:
	 * <ul>
	 * <li>{@link Server#createUI()} - Handles creating the UI. This method handles if the {@code Server} is running in {@code HEADLESS} mode.</li>
	 * <li>{@link Server#setupServerSocket()} - Handles creating the {@code ServerSocket}.</li>
	 * <li>{@link Server#setupFiles()} - Handles checking the directory structure and default files.</li>
	 * <li>{@link Server#readChatRoomsFromDisk()} - Handles loading {@code ChatRoom} file data.</li>
	 * <li>{@link Server#loadPlayerDataMap()} - Handles loading {@code Player} file data.</li>
	 * <li>Creating a new InitialSocketsConnectionHandler with the {@code ServerSocket} and running it on a new thread. </li>
	 * </ul>
	 */
	public void setupSever()
	{
		createUI();
		setupServerSocket();
		setupFiles();

		readChatRoomsFromDisk();
		loadPlayerDataMap();

		socketsConnectionHandler = new InitialSocketsConnectionHandler( serverSocket );
		new Thread( socketsConnectionHandler ).start(); //Start the InitialSocketsConnectionHandler on a new thread so we don't block other threads
	}

	/** Sets up the folder structures and default files (like the global {@link ChatRoom}) for the {@code Server} */
	public void setupFiles()
	{
		Path rootPath = Paths.get( FOLDER_ROOT );

		writeToConsole( "\t" + "setting up system files" );

		try
		{
			if( Files.notExists( rootPath ) )
				Files.createDirectory( rootPath );

			if( Files.notExists( Paths.get( FOLDER_ACCOUNTS ) ) )
				Files.createDirectory( Paths.get( FOLDER_ACCOUNTS ) );

			if( Files.notExists( Paths.get( FOLDER_CHATROOM ) ) )
				Files.createDirectory( Paths.get( FOLDER_CHATROOM ) );

			if( Files.notExists( Paths.get( FILE_CHATROOM_GLOBAL ) ) )
				Files.createFile( Paths.get( FILE_CHATROOM_GLOBAL ) );

			writeGlobalChatRoomDefault();
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
	}

	/** Reads in all the {@link ChatRoom} JSON files from disk */
	public void readChatRoomsFromDisk()
	{
		File chatRoomFolder = new File( FOLDER_CHATROOM );

		//@todo check conditions - Some conditions here should be removed once the code nears final form.
		if( chatRoomFolder.isDirectory() ) //Don't know what else it could be but we check anyway for safety...
		{
			File[] files = chatRoomFolder.listFiles( new FileFilter()
			{
				@Override
				public boolean accept( File filteredFile )
				{
					return filteredFile.isFile() && filteredFile.getName().endsWith( ".json" );
				}
			} );

			for( File file : files )
			{
				String chatRoomName = file.getName();

				if( chatRoomName.endsWith( ".json" ) )
					chatRoomName = chatRoomName.substring( 0, chatRoomName.lastIndexOf( '.' ) );

				JSONObject chatRoomData = ChatRoom.tryLoadRoomDataFromDisk( chatRoomName );

				if( chatRoomData == null )
				{
					//System.out.println( "error reading chat room file: " + chatRoomName );
					writeToConsole( "error reading chat room file: " + chatRoomName );
				}
				else
				{
					ChatRoom newChatRoom = new ChatRoom( chatRoomName, chatRoomData );
					chatRooms.put( chatRoomName, newChatRoom );
				}
			}

			//System.out.println( "setup " + chatRooms.size() + "chat-rooms from file" );
			writeToConsole( "setup " + chatRooms.size() + " chat-rooms from file" );
		}
	}

	private void markPlayersAsOffline()
	{
		File accountsFolder = new File( FOLDER_ACCOUNTS );

		//@todo check conditions - Some conditions here should be removed once the code nears final form.
		if( accountsFolder.isDirectory() ) //Don't know what else it could be but we check anyway for safety...
		{
			File[] files = accountsFolder.listFiles( new FileFilter()
			{
				@Override
				public boolean accept( File filteredFile )
				{
					return filteredFile.isFile() && filteredFile.getName().endsWith( ".json" );
				}
			} );

			for( File file : files )
			{
				String accountName = file.getName();

				if( accountName.endsWith( ".json" ) )
					accountName = accountName.substring( 0, accountName.lastIndexOf( '.' ) );

				try
				{
					JSONObject playerData = Player.loadAccountDataFromDisk( accountName );

					if( playerData != null && playerData.has( "status" ) && !playerData.getString( "status" ).equals( PlayerStatus.OFFLINE + "" ) )
					{
						playerData.put( "status", "offline" );
						writeToPlayerFile( playerData, accountName );
					}
				}
				catch( JSONException e )
				{
					e.printStackTrace();
				}
			}

			//System.out.println( "setup " + chatRooms.size() + "chat-rooms from file" );
			writeToConsole( "read " + playerDataMap.size() + " user accounts" );
		}
	}

	/** Loads the {@link Player} data-map from disk */
	private void loadPlayerDataMap()
	{
		File accountsFolder = new File( FOLDER_ACCOUNTS );

		//@todo check conditions - Some conditions here should be removed once the code nears final form.
		if( accountsFolder.isDirectory() ) //Don't know what else it could be but we check anyway for safety...
		{
			File[] files = accountsFolder.listFiles( new FileFilter()
			{
				@Override
				public boolean accept( File filteredFile )
				{
					//Return only files that are files (not directories) and that are of JSON type (end with ".json").
					return filteredFile.isFile() && filteredFile.getName().endsWith( ".json" );
				}
			} );

			for( File file : files )
			{
				String accountName = file.getName();

				if( accountName.endsWith( ".json" ) ) //If the accountName has the ".json" file extension on the end, remove it.
					accountName = accountName.substring( 0, accountName.lastIndexOf( '.' ) );

				try
				{
					JSONObject playerData = Player.loadAccountDataFromDisk( accountName );

					//Check if the playerData has a status field, and if so check if its set to "offline".
					if( playerData != null && playerData.has( "status" ) && !playerData.getString( "status" ).equals( PlayerStatus.OFFLINE + "" ) )
					{
						playerData.put( "status", "offline" ); //If it's not then we need to change it to be "offline"
						writeToPlayerFile( playerData, accountName ); //Or else players that are actually "offline" will be reported as "online"
					}

					playerDataMap.put( accountName, playerData ); //Don't reload the player-data from disk.
				}
				catch( JSONException e )
				{
					System.out.println( "loadPlayerDataMap: Tried to load .JSON file but its contents are invalid as JSON" );
					e.printStackTrace();
				}
			}

			writeToConsole( "read " + playerDataMap.size() + " user accounts" );
		}
	}

	/** Reloads the {@link Player} data-map from disk */
	private void reloadPlayerDataMap()
	{
		File accountsFolder = new File( FOLDER_ACCOUNTS );

		//@todo check conditions - Some conditions here should be removed once the code nears final form.
		if( accountsFolder.isDirectory() ) //Don't know what else it could be but we check anyway for safety...
		{
			File[] files = accountsFolder.listFiles( new FileFilter()
			{
				@Override
				public boolean accept( File filteredFile )
				{
					//Return only files that are files (not directories) and that are of JSON type (end with ".json").
					return filteredFile.isFile() && filteredFile.getName().endsWith( ".json" );
				}
			} );

			for( File file : files )
			{
				String accountName = file.getName();

				if( accountName.endsWith( ".json" ) ) //If the accountName has the ".json" file extension on the end, remove it.
					accountName = accountName.substring( 0, accountName.lastIndexOf( '.' ) );

				playerDataMap.put( accountName, Player.loadAccountDataFromDisk( accountName ) );
			}

			writeToConsole( "read " + playerDataMap.size() + " user accounts" );
		}
	}

	/** Creates a new global-chat {@link ChatRoom} file using hardcoded defaults. */
	public void writeGlobalChatRoomDefault()
	{
		ArrayList<String> allowedPlayersArray = new ArrayList<>();
		allowedPlayersArray.add( "all" );

		try
		{
			JSONObject globalChat = new JSONObject();
			globalChat.put( "owner", "server" );
			globalChat.put( "private", false );
			globalChat.put( "allowed-players", allowedPlayersArray );
			globalChat.put( "players-in-chat", new ArrayList<String>() );

			try( BufferedWriter writer = Files.newBufferedWriter( Paths.get( FILE_CHATROOM_GLOBAL ), Charset.forName( "UTF-8" ), StandardOpenOption.TRUNCATE_EXISTING ) )
			{
				writer.write( globalChat.toString() );
			}
			catch( IOException e )
			{
				e.printStackTrace();
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Handles trying to add a {@link Player} to the sending {@code Player}s friend-list.
	 *
	 * @param player                The {@code Player} who wants to add a {@code Player} to their friends-list.
	 * @param trueNameOfPlayerToAdd true-name of the {@code Player} that the sending {@code Player} wants to add to their friends-list.
	 */
	public void handleAddPlayerAsFriend( Player player, String trueNameOfPlayerToAdd )
	{
		if( doesUsernameExist( trueNameOfPlayerToAdd ) )
		{
			player.addFriend( trueNameOfPlayerToAdd );
		}
	}

	/**
	 * Handles trying to remove a {@link Player} from the sending {@code Player}s friends-list.
	 *
	 * @param player                   {@code Player} who wants to remove a {@code Player} from their friends-list.
	 * @param trueNameOfPlayerToRemove true-name of the {@code Player} the sending {@code Player} wants removed from their friends-list.
	 */
	public void handleRemovePlayerAsFriend( Player player, String trueNameOfPlayerToRemove )
	{
		if( doesUsernameExist( trueNameOfPlayerToRemove ) )
		{
			player.removeFriend( trueNameOfPlayerToRemove );
		}
	}

	public void getChatRoomData( String roomName )
	{

	}

	/**
	 * Creates the {@code Server} UI. If the {@code Server} is running in HEADLESS mode then this instead creates a new {@link HeadlessConsole},
	 * Otherwise it creates a new {@link ServerFrontend}
	 */
	public void createUI()
	{
		if( !GraphicsEnvironment.isHeadless() ) //Check with the GraphicsEnvironment to see if we're running HEADLESS.
			frontend = new ServerFrontend();
		else //If we are headless start a new HeadlessConsole on a new thread, so that the Server can be interfaced with on the command line.
			new Thread( new HeadlessConsole() ).start();
	}

	/** Repaints the {@code Server} UI components. If {@code Server} is running in HEADLESS mode then this does nothing. */
	public void repaintUI()
	{
		if( !GraphicsEnvironment.isHeadless() )
			frontend.repaintComponents(); //Check with the GraphicsEnvironment to see if we're running HEADLESS, and only repaint if we're not.
	}

	/**
	 * Passes a {@link Socket} to this Server to create a new {@link Player} instance with, and start the communication chain
	 *
	 * @param newSocket the {@code Socket} that the client is using to communicate with the server
	 */
	public void passSocket( Socket newSocket )
	{
		System.out.println( "Getting new player - Player #" + ( clientCounter + 1 ) );

		Player newPlayer = new Player( newSocket ); //Create a new player and give it the passed socket.
		new Thread( newPlayer ).start(); //Put the new Player onto its own thread so it can run without blocking the rest of the Server.
		clientCounter++; //Add one to the clientCounter because a new client has connected

		playerList.add( newPlayer );

		newPlayer.setDisplayName( "Player #" + clientCounter );
		reBuildPlayerDisplayList();
	}

	/** Rebuilds the {@code Player} display lists for the {@code Server} {@link ServerFrontend}. */
	public void reBuildPlayerDisplayList()
	{
		if( !GraphicsEnvironment.isHeadless() )
		{
			frontend.clearListSelections();

			frontend.clearPlayerListDisplayModel();
			frontend.addAllToPlayerListDisplayModel( buildPlayerDisplayStringList() );
		}
	}

	public void reBuildGameDisplayList()
	{
		if( !GraphicsEnvironment.isHeadless() )
		{
			frontend.clearListSelections();

			frontend.clearGameListDisplayModel();
			frontend.addAllToGameListDisplayModel( buildGameDisplayStringList() );
		}
	}

	/**
	 * Build a String list of all {@link Game}s with the format of "game-id" for use as a reference list.
	 *
	 * @return {@link List} List of {@code Game}s as String.
	 */
	public List<String> buildGameDisplayStringList()
	{
		List<String> displayStringList = new ArrayList<>();

		for( Game game : gamesList )
		{
			displayStringList.add( game.getGameID() );
		}

		return displayStringList;
	}

	/**
	 * Build a String list of all {@link Player}s with the format of "true-name | display-name" for use as a reference list.
	 *
	 * @return {@link List} List of {@code Player}s as String.
	 */
	public List<String> buildPlayerDisplayStringList()
	{
		List<String> displayStringList = new ArrayList<>();

		for( Player player : playerList )
		{
			displayStringList.add( player.getTrueName() + " | " + player.getDisplayName() );
		}

		return displayStringList;
	}

	/**
	 * Writes a line to the {@code Server} console.
	 * If the {@code Server} is running in HEADLESS mode then this writes to {@link System#out}.
	 * Otherwise it writes to console handles by the {@link ServerFrontend}
	 *
	 * @param consoleLine String to write
	 */
	public void writeToConsole( String consoleLine )
	{
		if( GraphicsEnvironment.isHeadless() || printToSystem )
			System.out.println( consoleLine );
		else if( !GraphicsEnvironment.isHeadless() )
			frontend.writeToConsole( consoleLine );
	}

	public void processCommand( JSONObject command, Player player )
	{
		writeToConsole( "Command from " + player.getDisplayName() + ": " + command.toString() );

		try
		{
			switch( command.getString( "command" ) )
			{
				case "pong": //Reply to server PING command, telling the server the pinged client still lives
					writeToConsole( player.getDisplayName() + ": @Server > Pong!" );
					break;

				case "create-account": //Try and create a new account using the provided credentials
					handleCreatingNewAccount( player, command.getString( "username" ), command.getString( "password" ) );
					break;

				case "login": //Try and login to an account using the provided credentials
					handleTryLogin( player, command.getString( "username" ), command.getString( "password" ) );
					break;

				case "check-chat-room-name-is-free":
					handleCheckChatRoomNameIsFree( player, command.getString( "room-name" ) );
					break;

				case "create-new-chat-room": //Create a new ChatRoom with the calling player as the owner
					handleCreateNewChatRoom( player, command.getString( "room-name" ), command.getJSONObject( "room-data" ) );
					break;

				case "allow-in-room": //Allow the given player to enter the given private ChatRoom
					handleAllowPlayerInRoom( player, command.getString( "room-name" ), command.getString( "player-to-allow" ) );
					break;

				case "disallow-in-room": //Disallow the given player to enter the given private ChatRoom
					handleDisallowPlayerInRoom( player, command.getString( "room-name" ), command.getString( "player-to-disallow" ) );
					break;

				case "join-room":   //Try and join the given chat-room
					handleTryJoinChatRoom( player, command.getString( "room-name" ) );
					break;

				case "request-players-in-room-list": //Request a list of all players in the given room
					handleSupplyPlayersInRoomList( player, command.getString( "room-name" ) );
					break;

				case "request-all-players-list": //Request the master-list of all players
					handleSupplyAllPlayersList( player );
					break;

				case "send-message-to-room": //Send message to every player in the chat-room
					handleSendMessageToRoom( player, command.getString( "room-name" ), command.getString( "message" ) );
					break;

				case "add-friend": //Add a player to the calling player's friend-list
					handleAddPlayerAsFriend( player, command.getString( "player-name" ) );
					break;

				case "remove-friend": //Remove a player from the calling player's friend-list
					handleRemovePlayerAsFriend( player, command.getString( "player-name" ) );
					break;

				case "change-display-name": //Change the display-name of a player
					handleChangeDisplayName( player, command.getString( "new-display-name" ) );
					break;

				case "change-player-status": //Change the status of a player
					handleChangePlayerStatus( player, PlayerStatus.fromString( command.getString( "new-status" ) ) );
					break;

				case "create-game": //Create a new game with the calling player as the host
					handleCreateNewGame( player );
					break;

				case "look-for-game": //Mark the calling player as looking for a game
					break;

				case "join-game": //Join a game in session (including at the lobby)
					handleTryJoinGame( player, command.getString( "game-id" ) );
					break;

				case "start-game": //Start a game with the given player and game options
					handleStartGame( player, command.getString( "game-id" ), command.getString( "map-name" ) );
					break;

				case "setup-new-game": //Setup a new game for configuration
					break;

				case "invite-player-to-game": //Invite the given player to the calling player's game
					handleInvitePlayerToGame( player, command.getString( "player-to-invite" ), command.getString( "game-id" ) );
					break;

				case "kick-player-from-team-slot": //Kick a player out of a team slot, either to make it open again or to close it
					handleTryKickPlayerFromTeamSlot( player, command.getString( "team-name" ), command.getString( "game-id" ) );
					break;

				case "change-selected-map-for-game": //Change the selected map for a game
					handleChangeSelectedMapForGame( player, command.getString( "game-id" ), command.getString( "new-map-selection") );
					break;

				default:
					break;
			}
		}
		catch( JSONException e )
		{
			writeToConsole( "error parsing json command" );
			e.printStackTrace();
		}
	}

	private void handleChangeSelectedMapForGame( Player player, String gameID, String newSelectedMap )
	{
		if( doesGameExist( gameID ) ) //@todo check for game-owner
			getGameByID( gameID ).changeSelectedMap( newSelectedMap );
	}

	public void handleTryJoinGame( Player player, String gameID )
	{
		//@todo replace getFirstFreeSlot().equals( "none" ) with a method call of "hasFreeSlot" or similar
		if( doesGameExist( gameID ) && !getGameByID( gameID ).getFirstFreeSlot().equals( "none" ) )
		{
			getGameByID( gameID ).playerJoinGame( player );
		}
	}

	/**
	 * Checks a {@code Game} with the given game-id exists.
	 *
	 * @param gameID the ID of the {@code Game} to check for.
	 *
	 * @return {@code true} if a {@code Game} with the given game-id exists, {@code false} otherwise.
	 */
	public boolean doesGameExist( String gameID )
	{
		return getGameByID( gameID ) != null;
	}

	public void handleTryKickPlayerFromTeamSlot( Player player, String teamSlot, String gameID )
	{
		if( doesGameExist( gameID ) )
		{
			getGameByID( gameID ).kickPlayerInTeam( teamSlot ); //We don't send any reply to the player since the kicked player will send "leaveGame"
		}
	}

	public void handleInvitePlayerToGame( Player player, String invitedTrueName, String gameID )
	{
		try
		{
			if( doesPlayerExist( invitedTrueName ) && doesGameExist( gameID ) )
			{
				JSONObject result = new JSONObject();
				result.put( "reply", "game-invite-send" );
				result.put( "invited-player", invitedTrueName );
				result.put( "result", true );
				player.writeToSocket( result.toString() );

				getGameByID( gameID ).invitePlayerToGame( getPlayerByTrueName( invitedTrueName ) );
			}
			else
			{
				JSONObject result = new JSONObject();
				result.put( "reply", "game-invite-send" );
				result.put( "result", false );
				player.writeToSocket( result.toString() );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void handleStartGame( Player player, String gameID, String mapName )
	{
		Game game = getGameByID( gameID );

		if( game != null )
		{
			game.startGame( mapName );

			try
			{
				JSONObject reply = new JSONObject();
				reply.put( "reply", "starting-game" );
				reply.put( "result", true );

				if( reply.getBoolean( "result" ) )
				{
					reply.put( "game-id", game.getGameID() );
					writeToConsole( "Trying to start game for " + player.getTrueName() + ": Success" );
				}
				else //For now this can't occur, but its here just in case. Same with the conditional logic.
					writeToConsole( "Trying to start game for " + player.getTrueName() + ": Failed" );

				player.writeToSocket( reply.toString() );
			}
			catch( JSONException e )
			{
				e.printStackTrace();
			}
		}
	}

	public Game getGameByID( String gameID )
	{
		for( Game game : gamesList )
		{
			if( game.getGameID().equals( gameID ) )
				return game;
		}

		return null;
	}

	private void handleCreateNewGame( Player player )
	{
		Game newGame = new Game( player.getTrueName() );
		new Thread( newGame ).start(); //Start the game on a new thread so it can talk to the players without blocking anything else.

		newGame.setGameID( gameCounter + "" );
		newGame.setGameName( "Game #" + gameCounter + " | " + newGame.getGameID() );

		gamesList.add( newGame );
		reBuildGameDisplayList();

		gameCounter++;

		try
		{
			JSONObject reply = new JSONObject();
			reply.put( "reply", "create-new-game" );
			reply.put( "result", true );

			if( reply.getBoolean( "result" ) )
			{
				reply.put( "game-id", newGame.getGameID() );
				reloadPlayerDataMap();
				writeToConsole( "Trying to create new game for " + player.getTrueName() + ": Success" );
			}
			else //For now this can't occur, but its here just in case. Same with the conditional logic.
				writeToConsole( "Trying to create new game for " + player.getTrueName() + ": Failed" );

			player.writeToSocket( reply.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Handles changing the {@link PlayerStatus} of a {@link Player} from their current status to the give new status.
	 *
	 * @param player    the {@code Player} who's status to change.
	 * @param newStatus the new status of the calling {@code Player}
	 */
	public void handleChangePlayerStatus( Player player, PlayerStatus newStatus )
	{

	}

	/**
	 * Handles trying to change the display-name of a {@code Player}. No checks are required for this method since
	 * the calls only effect the caller, who must be valid to make the call in the first place.
	 *
	 * @param player         the {@link Player} who wants to change their display-name.
	 * @param newDisplayName the new display-name that the calling {@code Player} wants to change to.
	 */
	private void handleChangeDisplayName( Player player, String newDisplayName )
	{
		player.changeDisplayName( newDisplayName );
	}

	/**
	 * Sends an announcement to all chat-rooms with the true-name of "announce" from {@code Server}.
	 *
	 * @param message message to send as an announcement.
	 */
	public void sendAnnouncement( String message )
	{
		for( Map.Entry<String, ChatRoom> entry : chatRooms.entrySet() )
		{
			entry.getValue().sendMessageToChat( "announcer", message );
		}
	}

	/**
	 * Handles sending a message from a {@link Player} to a specific {@link ChatRoom}.
	 * Checks if the given room exists, and if it does passes it the {@code Player} and message to send to all players in the room.
	 *
	 * @param player   the player who is sending the message.
	 * @param roomName the name of the room to send the message to.
	 * @param message  the actual message to send.
	 */
	private void handleSendMessageToRoom( Player player, String roomName, String message )
	{
		if( doesChatRoomExist( roomName ) )
			chatRooms.get( roomName ).sendMessageToChat( player.getTrueName(), message );
		else
			writeToConsole( "handleSendMessageToRoom: room '" + roomName + "' doesn't exist" );
	}

	/**
	 * Update the {@link Player} JSON HashMap. Overrides the value mapped to the given true-name with the new {@link JSONObject}.
	 *
	 * @param trueName the true-name of the {@code Player's} JSON data to update.
	 * @param info     the updated JSON data to replace the {@code Player's} current data with.
	 */
	public void updatePlayerJSON( String trueName, JSONObject info )
	{
		playerDataMap.put( trueName, info );

		for( Map.Entry<String, ChatRoom> chatRoomEntry : chatRooms.entrySet() )
		{
			chatRoomEntry.getValue().sendPlayersUpdatedRoomList();
		}

		handleUpdateAllPlayersList();
	}

	/**
	 * Generates a {@link JSONObject} that contains all user-accounts from file by iterating through the playerDataMap HashMap.
	 * The JSONObject is stored with the key-value pair of {@code { "true-name" : "json-data" } }
	 *
	 * @return {@code JSONObject} with the data of each user-account stored with the key of the accounts true-name
	 */
	private JSONObject getAllPlayersListAsJSON()
	{
		try
		{
			JSONObject allPlayersList = new JSONObject();

			for( Map.Entry<String, JSONObject> usernamePair : playerDataMap.entrySet() )
			{
				allPlayersList.put( usernamePair.getKey(), usernamePair.getValue() );
			}

			return allPlayersList;
		}
		catch( JSONException e )
		{
			e.printStackTrace();
			return null;
		}
	}

	/** Handles sending all {@code Player}s an updated version of the master all-players list */
	private void handleUpdateAllPlayersList()
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "update-all-players-list" );

			JSONObject allPlayersList = getAllPlayersListAsJSON();
			command.put( "all-players", allPlayersList );

			for( Player player : playerList )
			{
				player.writeToSocket( command.toString() );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Handles supplying a {@link Player} with the master-list of all {@code Player}s.
	 *
	 * @param player {@code Player} to supply the master-list of all {@code Player}s too.
	 */
	private void handleSupplyAllPlayersList( Player player )
	{
		try
		{
			JSONObject result = new JSONObject();
			result.put( "reply", "supply-all-players-list" );
			result.put( "result", true );

			JSONObject allPlayersList = getAllPlayersListAsJSON();

			result.put( "all-players", allPlayersList );

			player.writeToSocket( result.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Handles supplying a {@link Player} with the players-in-room list for the given room.
	 *
	 * @param player   {@code Player} requesting the list
	 * @param roomName the name of the room to supply the list for.
	 */
	public void handleSupplyPlayersInRoomList( Player player, String roomName )
	{
		try
		{
			if( doesChatRoomExist( roomName ) )
			{
				JSONObject result = new JSONObject();
				result.put( "reply", "supply-players-in-room-list" );
				result.put( "room-name", roomName );
				result.put( "result", true );
				result.put( "players-in-room", chatRooms.get( roomName ).getPlayersInRoomAsJSONString() );
				player.writeToSocket( result.toString() );
			}
			else
			{
				JSONObject result = new JSONObject();
				result.put( "reply", "supply-players-in-room-list" );
				result.put( "result", false );
				result.put( "room-name", roomName );
				player.writeToSocket( result.toString() );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Handles disallowing a {@link Player} into the given chat-room - the {@code Player} instance is found by using its true-name.
	 * This only has effect in a private room by removing the given {@code Player} off the allowed-players-list
	 *
	 * @param player           the {@code Player} trying to do the disallowing. If this isn't the owner of the room the disallowing will fail.
	 * @param roomName         the room to disallow the player in.
	 * @param playerToDisallow true-name of the {@code Player} to look for and disallow
	 */
	public void handleDisallowPlayerInRoom( Player player, String roomName, String playerToDisallow )
	{
		try
		{
			JSONObject result = new JSONObject();
			result.put( "reply", "disallow-player-in-room" );
			result.put( "result", false ); //Default to false just in case.

			if( doesChatRoomExist( roomName ) ) //Check that the room exists (Client would check this, but this is a package-hack protection)
			{
				ChatRoom room = chatRooms.get( roomName );

				if( room.isPlayerOwner( player ) ) //Check the player calling this is the room owner. Package-hack protection
				{
					if( room.isPlayerAllowedInRoom( findPlayer( playerToDisallow ) ) )
						room.disallowPlayer( findPlayer( playerToDisallow ) );

					result.put( "result", true );
				}
				else
					result.put( "reason", "not room owner" ); //Error code (EC#000019): not owner
			}
			else
				result.put( "reason", "room does not exist" ); //Error code (EC#000020): chat room doesn't exist

			player.writeToSocket( result.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Handles allowing a {@link Player} into the given chat-room. Adds the given {@code Player} to the allowed-players list.
	 * This will only have an effect if the room is private - otherwise the allowed-players list will be ignored anyway.
	 *
	 * @param player        the {@code Player} trying to do the adding. If this {@code Player} isn't the owner of the room then the allowing will fail.
	 * @param roomName      name of the room to try and add the player to.
	 * @param playerToAllow the true-name of the player to try and allow
	 */
	public void handleAllowPlayerInRoom( Player player, String roomName, String playerToAllow )
	{
		try
		{
			JSONObject result = new JSONObject();
			result.put( "reply", "allow-player-in-room" );
			result.put( "result", false ); //Default to false just in case.

			if( doesChatRoomExist( roomName ) ) //Check that the room exists (Client would check this, but this is a package-hack protection)
			{
				ChatRoom room = chatRooms.get( roomName );

				if( room.isPlayerOwner( player ) ) //Check the player calling this is the room owner. Package-hack protection
				{
					if( !room.isPlayerAllowedInRoom( findPlayer( playerToAllow ) ) )
						room.allowPlayer( findPlayer( playerToAllow ) );

					result.put( "result", true );
				}
				else
					result.put( "reason", "not room owner" ); //Error code (EC#000017): not owner
			}
			else
				result.put( "reason", "room does not exist" ); //Error code (EC#000003): chat room doesn't exist

			player.writeToSocket( result.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public boolean doesPlayerExist( String playerName )
	{
		return findPlayer( playerName ) != null;
	}

	public void handleCheckChatRoomNameIsFree( Player player, String chatRoomName )
	{
		try
		{
			JSONObject result = new JSONObject();
			result.put( "reply", "check-chat-room-name-free" );
			result.put( "result", !doesChatRoomExist( chatRoomName ) ); //Default to false just in case.

			player.writeToSocket( result.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Checks if a {@link ChatRoom} with the given name exists by looking for a file with the room name.
	 *
	 * @param roomName Name of the chat room
	 *
	 * @return {@code true} if the ChatRoom exists, {@code false} otherwise
	 */
	public boolean doesChatRoomExist( String roomName )
	{
		return Files.exists( Paths.get( FOLDER_CHATROOM + roomName + ".json" ) );
	}

	public void handleCreateNewChatRoom( Player player, String roomName, JSONObject roomData )
	{
		try
		{
			JSONObject result = new JSONObject();
			result.put( "reply", "create-chat-room" );
			result.put( "result", false ); //Default to false just in case.
			result.put( "room-name", roomName );

			if( !doesChatRoomExist( roomName ) ) //If a ChatRoom with the given name doesn't already exist
			{
				createNewChatRoom( player, roomName, roomData );
				result.put( "result", true );
			}
			else //ChatRoom doesn't exist
				result.put( "reason", "error making room" ); //Error code (EC#0000): unknown error

			player.writeToSocket( result.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Create a new {@code ChatRoom} for the calling {@code Player}.
	 *
	 * @param player   {@code Player} creating the {@code ChatRoom}.
	 * @param roomName the name of the {@code ChatRoom} to make.
	 * @param roomData {@code JSONObject} data for this room as created by the {@code Client} for the calling {@code Player}.
	 *
	 * @return {@code true} if the {@code ChatRoom} is created successfully, {@code false} otherwise
	 */
	public boolean createNewChatRoom( Player player, String roomName, JSONObject roomData )
	{
		if( createNewChatRoomFile( roomName, player.getTrueName(), roomData ) )
		{
			writeToConsole( "Created room for " + player.getTrueName() + " under file " + roomName + ".json" );
			return true;
		}
		else //Check if the room already exists and if it doesn't then we can create it. Else return false.
			return false;
	}

	/**
	 * Creates a new {@code ChatRoom} {@code JSON} file.
	 *
	 * @param roomName name of the {@code ChatRoom} we're creating.
	 * @param owner    true-name of the {@code Player} that owns this {@code ChatRoom} (and who created it).
	 * @param roomData room-data for the {@code ChatRoom} as provided by the {@code Player}.
	 *
	 * @return {@code true} if room was created successfully, {@code false} otherwise
	 */
	public boolean createNewChatRoomFile( String roomName, String owner, JSONObject roomData )
	{
		try
		{
			roomData.put( "owner", owner );
			roomData.put( "players-in-chat", new ArrayList<String>() );

			StandardOpenOption[] options = new StandardOpenOption[]{ StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE };
			Path chatRoomPath = Paths.get( Server.FOLDER_CHATROOM + roomName + ".json" );

			try( BufferedWriter writer = Files.newBufferedWriter( chatRoomPath, Charset.forName( "UTF-8" ), options ) )
			{
				writer.write( roomData.toString() );
				return true;
			}
			catch( IOException e )
			{
				e.printStackTrace();
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Handle a {@link Client} trying to join a {@code ChatRoom}.
	 * Checks that the {@code ChatRoom} exists, and that if the {@code Player} trying to join is allowed to join.
	 *
	 * @param player   the {@code Player} trying to join the {@code ChatRoom}.
	 * @param roomName the {@code ChatRoom} the {@code Player} is trying to join.
	 */
	public void handleTryJoinChatRoom( Player player, String roomName )
	{
		try
		{
			JSONObject result = new JSONObject();
			result.put( "reply", "enter-chat-room" );
			result.put( "result", false ); //Default to false just in case. This JSONObject is only written on fail, as ChatRoom handles success.

			if( doesChatRoomExist( roomName ) )
			{
				if( canPlayerJoinChatRoom( player, roomName ) ) //If the player is able to join the chat room (mainly if they are on the allowed list)
				{
					chatRooms.get( roomName ).enterRoom( player ); //Room data is passed by the room, so don't do it here
					return; //Return to prevent the writeToSocket from being called
				}
				else //@todo Change the reason message to the same as the doesChatRoomExist message to help hide the room. Difference is for debugging
					result.put( "reason", "not allowed" ); //error code
			}
			else
				result.put( "reason", "can't find room" ); //error code

			player.writeToSocket( result.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Checks if the given {@link Player} can join the given {@code ChatRoom}.
	 * This calls {@link ChatRoom#isPlayerAllowedInRoom(Player)} after getting the instance from {@code HashSet}.
	 *
	 * @param player   the {@code Player} we're interested in.
	 * @param roomName the name of the {@code ChatRoom} that the {@code Player} wants to join.
	 *
	 * @return {@code true} if the {@code Player} can join the room, {@code false} otherwise.
	 */
	public boolean canPlayerJoinChatRoom( Player player, String roomName )
	{
		return chatRooms.get( roomName ).isPlayerAllowedInRoom( player );
	}

	/**
	 * Checks to see if a user-account exists with the given username.
	 * Used to shortcut password checking by making sure the actual account name exists before anything else.
	 *
	 * @param username account username to check for
	 *
	 * @return {@code true} if an account with that username exists, {@code false} otherwise.
	 */
	public boolean doesUsernameExist( String username )
	{
		return playerDataMap.containsKey( username );
	}

	/** Called by the {@code Player} when it dies. Runs though all {@code ChatRoom}s and gets them to check their {@code Player}s for death */
	public void playerDied()
	{
		for( Map.Entry<String, ChatRoom> roomEntry : chatRooms.entrySet() )
		{
			roomEntry.getValue().checkPlayerList();
		}
	}

	/**
	 * Checks if the given password matches the password stored on file for the given user account name.
	 *
	 * @param username user-account username for the account we're trying to authenticate
	 * @param password password to check against
	 *
	 * @return {@code true} if the account with the given username matches for the given password, {@code false} otherwise.
	 */
	public boolean doesPasswordMatchForUsername( String username, String password )
	{
		if( doesUsernameExist( username ) )
		{
			try
			{
				JSONObject accountDataFromDisk = Player.loadAccountDataFromDisk( username );
				return accountDataFromDisk != null && password.equals( accountDataFromDisk.getString( "password" ) );
			}
			catch( JSONException e )
			{
				e.printStackTrace();
			}
		}

		return false;
	}

	/**
	 * Handles an attempt to login by a {@link Client}
	 *
	 * @param player   {@code Player} instance that is trying to login
	 * @param username the username the {@code Client} is trying to login as
	 * @param password the password the {@code Client} is trying to use to login as the given user
	 */
	private void handleTryLogin( Player player, String username, String password )
	{
		try
		{
			JSONObject reply = new JSONObject();
			reply.put( "reply", "login" );
			reply.put( "result", tryLogin( username, password ) );

			if( reply.getBoolean( "result" ) )
			{
				//@todo look to make this better - we effectively call "loadAccountDataFromDisk" twice. Once here and then again to loadPlayerData
				reply.put( "response", Player.loadAccountDataFromDisk( username ) );
				reply.put( "true-name", username );
				player.loadPlayerData( username );
				reBuildPlayerDisplayList();
			}

			player.writeToSocket( reply.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Try to login with the given user credentials
	 *
	 * @param username the username to try login as
	 * @param password the password to use to try and login
	 *
	 * @return {@code true} if the username and password match, {@code false} otherwise
	 */
	public boolean tryLogin( String username, String password )
	{
		return doesPasswordMatchForUsername( username, password );
	}

	/**
	 * Handles an attempt to create a new user by a {@link Client}
	 *
	 * @param player   {@code Player} instance that is trying to create a new user account
	 * @param username the username to make the account with
	 * @param password the password for the user-account
	 */
	private void handleCreatingNewAccount( Player player, String username, String password )
	{
		try //@todo better feedback
		{
			JSONObject reply = new JSONObject();
			reply.put( "reply", "create-account" );
			reply.put( "result", tryCreateNewAccount( username, password ) );

			if( reply.getBoolean( "result" ) )
			{
				reloadPlayerDataMap();
				writeToConsole( "Trying to create account: " + username + " for " + player.getDisplayName() + ": Success" );
			}
			else
				writeToConsole( "Trying to create account: " + username + " for " + player.getDisplayName() + ": Failed" );

			player.writeToSocket( reply.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Tries to create a new user-account given the username and password supplied.
	 * This adds a new entry into the user-accounts file and sets it up with all other account related files.
	 *
	 * @param username account username for the new account
	 * @param password account password for the new account
	 *
	 * @return {@code true} if the account was created successfully, {@code false} otherwise
	 */
	public boolean tryCreateNewAccount( String username, String password )
	{
		return !doesUsernameExist( username ) && createNewAccountFile( username, password );
	}

	/**
	 * Creates a new account file using the given username and password
	 *
	 * @param username username of the account to create
	 * @param password password of the account to create
	 *
	 * @return {@code true} if account was created successfully, {@code false} otherwise.
	 */
	public boolean createNewAccountFile( String username, String password )
	{
		try
		{
			JSONObject newRoomData = new JSONObject();
			newRoomData.put( "display-name", username );
			newRoomData.put( "password", password );
			newRoomData.put( "status", "offline" );
			newRoomData.put( "friends", new ArrayList<String>() );

			StandardOpenOption[] options = new StandardOpenOption[]{ StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.CREATE };
			Path accountPath = Paths.get( Server.FOLDER_ACCOUNTS + username + ".json" );

			try( BufferedWriter writer = Files.newBufferedWriter( accountPath, Charset.forName( "UTF-8" ), options ) )
			{
				writer.write( newRoomData.toString() );
				writer.flush();
				return true;
			}
			catch( IOException e )
			{
				e.printStackTrace();
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Called when a {@link Client} disconnects - Removes the {@code Player} from the {@link Server#playerList} and updates the display-lists as needed.
	 *
	 * @param player the {@code Player} that disconnected
	 */
	public void removePlayer( Player player )
	{
		writeToConsole( "Client left: " + player.getDisplayName() );

		playerList.remove( player );
		reBuildPlayerDisplayList();
	}

	public void setupServerSocket()
	{
		try
		{
			serverSocket = new ServerSocket( SERVER_PORT );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Lists all items of relevance to the given topic to console. Mainly for HEADLESS mode where we don't have a GUI to list stuff likes players
	 * The following are valid topics. Topics with multiple names are separated by ", ":
	 * <ul>
	 * <li>players, clients - Lists all connected players</li>
	 * <li>user-accounts - Lists all user accounts loaded into playerDataMap </li>
	 * <li>games - lists all games currently in play </li>
	 * <li>rooms, chat-rooms - lists all chat-rooms loaded into chatRooms </li>
	 * </ul>
	 *
	 * @param topic The scope of what we're to list
	 */
	public void listToConsole( String topic )
	{
		switch( topic )
		{
			case "players":
			case "clients":
				writeToConsole( ">> listing connected players in format >: true-name | display-name" );
				for( Player player : playerList )
				{
					writeToConsole( "\t >:" + player.getTrueName() + " | " + player.getDisplayName() );
				}
				break;

			case "user-accounts":
				writeToConsole( ">> listing all usernames with valid files" );
//				for(  s : playerDataMap )
//				{
//					writeToConsole( "\t >:" + s );
//				}
				break;

			case "games":
				writeToConsole( ">> listing connected players in format >: true-name | display-name" );
				break;

			case "rooms":
			case "chat-rooms":
				writeToConsole( ">> listing chat-rooms in format >: key | value" );
				Iterator it = chatRooms.entrySet().iterator();
				//noinspection WhileLoopReplaceableByForEach
				while( it.hasNext() )
				{
					Map.Entry pair = (Map.Entry) it.next();
					writeToConsole( "\t >:" + ( pair.getKey() + " | " + ( (ChatRoom) pair.getValue() ).getRoomData().toString() ) );
				}
				break;

			default:
				writeToConsole( ">> list: " + topic + " -- unknown topic" );
				break;
		}
	}
}