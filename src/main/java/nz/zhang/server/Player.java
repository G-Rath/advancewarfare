package nz.zhang.server;

import nz.zhang.utils.PlayerStatus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;

/**
 * Player - Represents a client relative to the current server instance
 * <p>
 * Created by G-Rath on 1/05/2015.
 */
public class Player implements Runnable
{
	private PrintWriter socketOut;
	private BufferedReader socketIn;

	private Socket socket;

	private boolean alive = true;

	private boolean online = true;

	private PlayerStatus status = PlayerStatus.fromString( "online" );

	private String displayName = "player";

	private String trueName;

	private HashSet<String> friends = new HashSet<>();

	/** JSONObject that holds the account data of this player - Set once authenticated */
	private JSONObject playerData;

	public Player( Socket socket )
	{
		setSocket( socket );

		try
		{
			socketOut = new PrintWriter( new BufferedWriter( new OutputStreamWriter( socket.getOutputStream(), "UTF-8" ) ), true );
			socketIn = new BufferedReader( new InputStreamReader( socket.getInputStream(), "UTF-8" ) );
		}
		catch( IOException e )
		{
			e.printStackTrace();
			System.exit( -1 );
		}
	}

	public static JSONObject loadAccountDataFromDisk( String accountName )
	{
		//@todo better error handling
		try( BufferedReader bufferedReader = Files.newBufferedReader( Paths.get( Server.FOLDER_ACCOUNTS + accountName + ".json" ), Charset.forName( "UTF-8" ) ) )
		{
			String line;
			String accountData = "";
			while( ( line = bufferedReader.readLine() ) != null )
			{
				accountData += line;
			}

			return new JSONObject( accountData );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
		catch( JSONException e )
		{
			System.out.println( "Warning - trying to read account data for " + accountName + " - JSON failed to parse correctly" );
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Changes the status of this {@code Player} to the status provided. This method calls {@link Player#updatePlayerJSON()}
	 * afterwards to make sure that the change is sent to other {@code Player}s, regardless of if the status actually changed
	 * for this {@code Player} (i.e if the new status is the same status as this {@code Player}s current status assignment
	 * will still take place, and the {@code Server} will still send connected {@code Client}s data.
	 *
	 * @param newStatus the new status for this {@code Player} that the caller wants to change to.
	 */
	public void changeStatus( PlayerStatus newStatus )
	{
		status = newStatus;
		updatePlayerJSON();
	}

	/**
	 * Adds a {@code Player} to this {@code Player}s friend-list
	 *
	 * @param trueName true-name of the {@code Player} to add.
	 */
	public void addFriend( String trueName )
	{
		friends.add( trueName );
		updatePlayerJSON();
	}

	/**
	 * Removes a {@code Player} from this {@code Player}s friend-list.
	 *
	 * @param trueName true-name of the {@code Player} to remove.
	 */
	public void removeFriend( String trueName )
	{
		friends.remove( trueName );
		updatePlayerJSON();
	}

	public void changeDisplayName( String newDisplayName )
	{
		displayName = newDisplayName;
		updatePlayerJSON();
	}

	public boolean isFriend( Player player )
	{
		return friends.contains( player.trueName );
	}

	public void updatePlayerJSON()
	{
		try
		{
			//playerData.put( "true-name", trueName );
			playerData.put( "display-name", displayName );
			playerData.put( "status", status );
			playerData.put( "password", playerData.getString( "password" ) );
			playerData.put( "friends", friends );

			Server.getInstance().updatePlayerJSON( trueName, playerData );

			updatePlayerFile();
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/** Writes back to the Player's JSON file with the new Player settings */
	public void updatePlayerFile()
	{
		try
		{
			playerData.put( "display-name", displayName );
			playerData.put( "status", status );
			playerData.put( "password", playerData.getString( "password" ) );
			playerData.put( "friends", friends );

			try( BufferedWriter writer = Files.newBufferedWriter( Paths.get( Server.FOLDER_ACCOUNTS + trueName + ".json" ), Charset.forName( "UTF-8" ), StandardOpenOption.TRUNCATE_EXISTING ) )
			{
				writer.write( playerData.toString() );
			}
			catch( IOException e )
			{
				e.printStackTrace();
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void setSocket( Socket socket )
	{
		this.socket = socket;
	}

	public void die()
	{
		alive = false;
	}

	@Override
	public void run()
	{
		String inputLine;

		while( alive )
		{
			try //While there is data to read from the socket that has been sent from the client to the server socket
			{
				while( ( inputLine = readFromSocket() ) != null )
				{
					try //Try and parse the line as JSON. If you fail then an invalid command has been sent, but its not a reason to crash.
					{
						passCommandToServer( new JSONObject( inputLine ) );
					}
					catch( JSONException e )
					{
						Server.getInstance().writeToConsole( "Invalid command was passed by " + getDisplayName() + ": " + inputLine );
					}
				}
			}
			catch( java.net.SocketException e )
			{
				Server.getInstance().removePlayer( this );
				die();
			}
			catch( IOException e )
			{
				e.printStackTrace();
			}
		}

		status = PlayerStatus.OFFLINE;
		Server.getInstance().playerDied();
		updatePlayerJSON();
	}

	public void loadPlayerData( String accountName )
	{
		trueName = accountName;
		playerData = loadAccountDataFromDisk( accountName );

		if( playerData == null )
		{
			//@todo better error handling
			System.exit( -1 ); //Once the rest of the systems are in place (or at least their framework is) this will be handled better
		}
		else
		{
			try
			{
				displayName = playerData.getString( "display-name" );

				friends = new HashSet<>();
				for( int i = 0; i < playerData.getJSONArray( "friends" ).length(); i++ )
				{
					friends.add( playerData.getJSONArray( "friends" ).getString( i ) );
				}
			}
			catch( JSONException e )
			{
				e.printStackTrace();
			}
			updatePlayerJSON();
		}
	}

	public boolean isOnline()
	{
		return status != PlayerStatus.OFFLINE;
	}

	@SuppressWarnings( "unused" )
	public void setPlayerData( JSONObject data )
	{
		playerData = data;
	}

	public String getTrueName()
	{
		return trueName;
	}

	public void passCommandToServer( final JSONObject command )
	{
		new Thread( new Runnable()
		{
			@Override
			public void run()
			{
				Server.getInstance().processCommand( command, Player.this );
			}
		} ).start();
	}

	/**
	 * Write data to this {@code Player}s socket for the {@code Client} to read .
	 *
	 * @param data the data to write as a string - this should be {@code JSON} since its expected on the {@code Client}s end.
	 */
	public void writeToSocket( String data )
	{
		socketOut.println( data );
	}

	/**
	 * Read a line of data from this {@code Player}s socket that the {@code Client} has sent this {@code Player}
	 *
	 * @return the data as {@code String} - This is expected to be {@code JSON} data.
	 * @throws IOException if the socket is closed or otherwise unreadable.
	 */
	public String readFromSocket() throws IOException
	{
		return socketIn.readLine();
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName( String displayName )
	{
		this.displayName = displayName;
	}
}
