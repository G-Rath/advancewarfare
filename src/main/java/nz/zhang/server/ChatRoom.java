package nz.zhang.server;

import nz.zhang.client.Client;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

/**
 * ChatRoom class - Data class for holding information about a chat-room
 * <p>
 * Created by G-Rath on 9/05/2015.
 */
public class ChatRoom
{
	private JSONObject roomData;

	private String roomName;
	/** Is this a private chat room */
	private boolean isPrivate = false;
	/** HashSet of the true-names of players that are allowed in this ChatRoom. If this room isn't private, then the list isn't used */
	private HashSet<String> allowedPlayers = new HashSet<>();
	/** The owner of this ChatRoom, and hence the person that can change the rooms settings */
	private String owner;
	/** HashSet containing a reference to all the players currently in this chat room */
	private HashSet<Player> playersInRoom = new HashSet<>();

	public ChatRoom( String roomName )
	{
		if( roomName.endsWith( ".json" ) ) //If the .json file extension is at the end of the name, remove it
			roomName = roomName.substring( 0, roomName.lastIndexOf( '.' ) ); //@todo check if this is required once this framework section is finished

		this.roomName = roomName;
		parseRoomData( loadRoomDataFromDisk( roomName ) );
	}

	public ChatRoom( String roomName, JSONObject data )
	{
		this.roomName = roomName;
		parseRoomData( data );
	}

	/**
	 * Tries to load {@link ChatRoom} data from the given roomName - This method is for testing that the file can be parsed as {@code JSON} first.
	 * This calls {@link ChatRoom#loadRoomDataFromDisk(String, boolean)} with the {@code failLoudly} parameter set to {@code false} so it won't make
	 * a fuse if the file is invalid {@code JSON}
	 *
	 * @param roomName name of the {@code ChatRoom} and hence of the file to try and read
	 *
	 * @return {@link JSONObject} if correctly read, {@code null} otherwise
	 */
	public static JSONObject tryLoadRoomDataFromDisk( String roomName )
	{
		return loadRoomDataFromDisk( roomName, false );
	}

	/**
	 * Loads {@link ChatRoom} data from the given {@code roomName} - If the given file can't be parsed as {@code JSON} then it will complain loudly.
	 * <p/>
	 * This calls {@link ChatRoom#loadRoomDataFromDisk(String, boolean)} with the {@code failLoudly} parameter set to {@code true} so it will make a
	 * fuse if the file is invalid JSON
	 *
	 * @param roomName name of the {@code ChatRoom} and hence of the file to read
	 *
	 * @return {@link JSONObject} if correctly read, {@code null} otherwise
	 */
	public static JSONObject loadRoomDataFromDisk( String roomName )
	{
		return loadRoomDataFromDisk( roomName, true );
	}

	/**
	 * Loads {@link ChatRoom} data from disk as a {@code JSON} file into a {@link JSONObject}.
	 *
	 * @param roomName   name of the {@code ChatRoom} and hence of the file to read
	 * @param failLoudly should this method "loudly" output errors if any occur when trying to parse the given file as {@code JSON}?
	 *
	 * @return {@link JSONObject} if correctly read, {@code null} otherwise
	 */
	private static JSONObject loadRoomDataFromDisk( String roomName, boolean failLoudly )
	{
		//@todo better error handling
		try( BufferedReader bufferedReader = Files.newBufferedReader( Paths.get( Server.FOLDER_CHATROOM + roomName + ".json" ), Charset.forName( "UTF-8" ) ) )
		{
			String line;
			String chatData = "";
			while( ( line = bufferedReader.readLine() ) != null )
			{
				chatData += line;
			}

			return new JSONObject( chatData );
		}
		catch( IOException e )
		{
			if( failLoudly )
			{
				e.printStackTrace();
			}
		}
		catch( JSONException e )
		{
			if( failLoudly )
			{
				System.out.println( "Warning - trying to read account data for " + roomName + " - JSON failed to parse correctly" );
				e.printStackTrace();
			}
		}

		return null;
	}

	private void parseRoomData( JSONObject data )
	{
		if( data == null )
		{
			//@todo better error handling
			System.exit( -1 );
		}

		roomData = data;

		try
		{
			owner = data.getString( "owner" );
			isPrivate = data.getBoolean( "private" );

			JSONArray jsonArray = data.getJSONArray( "allowed-players" );
			for( int i = 0; i < jsonArray.length(); i++ )
			{
				allowedPlayers.add( jsonArray.getString( i ) );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/** Writes back to the {@code ChatRoom}'s {@code JSON} file with the new {@code ChatRoom} settings */
	public void updateChatRoomFile()
	{
		try
		{
			JSONObject newRoomData = new JSONObject();
			newRoomData.put( "owner", owner );
			newRoomData.put( "private", false );
			newRoomData.put( "allowed-players", allowedPlayers );
			newRoomData.put( "players-in-chat", new ArrayList<String>() );

			try( BufferedWriter writer = Files.newBufferedWriter( Paths.get( Server.FOLDER_CHATROOM + roomName + ".json" ), Charset.forName( "UTF-8" ), StandardOpenOption.TRUNCATE_EXISTING ) )
			{
				writer.write( newRoomData.toString() );
			}
			catch( IOException e )
			{
				e.printStackTrace();
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/** Sends the new list to each {@link Player} in the room. Called when the players-in-room list is changed. */
	public void sendPlayersUpdatedRoomList()
	{
		for( Player player : playersInRoom )
		{
			try
			{
				JSONObject command = new JSONObject();
				command.put( "command", "update-players-in-room-list" );
				command.put( "room-name", roomName );
				command.put( "players-in-room", this.getPlayersInRoomAsJSONString() );
				player.writeToSocket( command.toString() );
			}
			catch( JSONException e )
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Sends a chat message to everyone in the chat room. This message is in {@code JSON} as follows:
	 * <pre>
	 * {@code
	 * {
	 *     ...
	 *     "message-object" : {
	 *         "sender" : "true-name",
	 *         "message" : "message"
	 *     },
	 *     ...
	 * }}
	 * </pre>
	 * <p/>
	 * The message-object's "sender" element represents a true-name'ed object - either the true-name of an account or a valid true-name keyword.
	 * Acceptable true-name keywords are "server", "god", and "announcer". These are for sending global messages to all users.
	 * By using true-name's the {@link Client} handles mapping the given true-name to the {@code Client}'s online-player list meaning that the chat-log will always
	 * reflect who said what with the correct display-names, in the event a {@link Player} changes their display-name.
	 * <p/>
	 *
	 * @param senderTrueName The true-name of the sender. This is either the true-name of a user account, or "server", "god" or "announcer".
	 * @param message        The actual message to send
	 */
	public void sendMessageToChat( String senderTrueName, String message )
	{
		try
		{
			JSONObject messageCommand = new JSONObject();
			messageCommand.put( "command", "new-chat-message" );
			messageCommand.put( "chat-room", roomName);

			JSONObject messageObject = new JSONObject();
			messageObject.put( "sender", senderTrueName );
			messageObject.put( "message", message );

			messageCommand.put( "message-object", messageObject );

			for( Player aPlayer : playersInRoom )
			{
				aPlayer.writeToSocket( messageCommand.toString() );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Called when a {@link Player} enters the room. Adds the {@code Player} to the {@code playersInRoom} list and sends the {@link Client} the room data.
	 * Since the player-in-room list is changed the {@link ChatRoom#sendPlayersUpdatedRoomList()} method is called to give the {@code Player}'s the new list.
	 *
	 * @param player the {@code Player} who is entering the room.
	 */
	public void enterRoom( Player player )
	{
		try
		{
			JSONObject chatRoomData = new JSONObject();
			chatRoomData.put( "reply", "enter-chat-room" );
			chatRoomData.put( "result", true );
			chatRoomData.put( "room-name", roomName );
			chatRoomData.put( "room-data", roomData.toString() );

			player.writeToSocket( chatRoomData.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		playersInRoom.add( player );
		sendPlayersUpdatedRoomList();
	}

	/**
	 * Creates a generic {@code JSONObject} holding a default template of a {@code ChatRoom}.
	 * Mainly for use with {@Link Server#createNewChatRoomFile}.
	 *
	 * @param owner     true-name of the {@code Player} that owns this {@code ChatRoom} (and who created it).
	 * @param isPrivate is this {@code ChatRoom} private or not.
	 *
	 * @return {@code JSONObject} with generic default data about a room.
	 */
	public static JSONObject createGenericJSONObjectRoomData( String owner, boolean isPrivate )
	{
		ArrayList<String> allowedPlayersArray = new ArrayList<>();

		if( isPrivate )
			allowedPlayersArray.add( owner );
		else
			allowedPlayersArray.add( "all" );

		try
		{
			JSONObject roomDataObject = new JSONObject();
			roomDataObject.put( "owner", owner );
			roomDataObject.put( "private", isPrivate );
			roomDataObject.put( "allowed-players", allowedPlayersArray );
			roomDataObject.put( "players-in-chat", new ArrayList<String>() );

			return roomDataObject;
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Called when a {@link Player} exits a room. Removes the player from the {@code playersInRoom} list.
	 * Since the player-in-room list is changed the {@link ChatRoom#sendPlayersUpdatedRoomList()} method is called to give the {@code Player}s the new list.
	 *
	 * @param player the {@code Player} who is leaving the room
	 */
	public void exitRoom( Player player )
	{
		playersInRoom.remove( player );
		sendPlayersUpdatedRoomList();
	}

	/**
	 * Adds a {@link Player} to the {@code allowedPlayers} HashSet, allowing them to enter the {@code ChatRoom} freely.
	 * This data is then written to the {@code ChatRoom}'s {@code JSON} file
	 *
	 * @param player the {@code Player} who's true-name to add to the {@code allowedPlayers} HashSet
	 */
	public void allowPlayer( Player player )
	{
		allowedPlayers.add( player.getTrueName() );
		updateChatRoomFile();
	}

	/**
	 * Removes a {@link Player} from the {@code allowedPlayers} HashSet, preventing them from entering the {@code ChatRoom} freely.
	 *
	 * @param player {@code Player} who's true-name to remove from the {@code allowedPlayers} HashSet
	 */
	public void disallowPlayer( Player player )
	{
		//@todo check if the player is in the HashSet
		allowedPlayers.remove( player.getTrueName() );
		updateChatRoomFile();
	}

	/**
	 * Gets the HashSet of the true-names of all the {@code Player}s allowed to enter this chat room freely
	 *
	 * @return HashSet of the true-names of all the {@code Player}s who are allowed to enter this {@code ChatRoom}
	 */
	@SuppressWarnings( "unused" )
	public HashSet<String> getAllowedPlayers()
	{
		return allowedPlayers;
	}

	/**
	 * Is the given {@link Player} the {@code Player} that owns this {@code ChatRoom}?
	 *
	 * @param player the {@code Player} we're interested in.
	 *
	 * @return {@code true} if the given {@code Player} owns this room, {@code false} otherwise
	 */
	public boolean isPlayerOwner( Player player )
	{
		return owner.equals( player.getTrueName() );
	}

	/**
	 * Checks if the given {@link Player} join this room? If the room isn't private then they can, else the allowedPlayers HashSet is checked.
	 *
	 * @param player The {@code Player} that we are interested in
	 *
	 * @return {@code true} if the given {@code Player} can enter, {@code false} otherwise
	 */
	//@todo check this - Suspect conditional statement is wrong (&& vs isPrivate vs results of contains)
	public boolean canPlayerJoinRoom( Player player )
	{
		return !isPrivate || ( isPlayerOwner( player ) || allowedPlayers.contains( player.getTrueName() ) );
	}

	/**
	 * Checks if the given {@link Player} join this room? If the room isn't private then they can, else the allowedPlayers HashSet is checked
	 *
	 * @param player the {@code Player} we are interested in
	 *
	 * @return {@code true} if the given {@code Player} can enter, {@code false} otherwise.
	 */
	//@todo change this - want to change the naming of this method, so look to method it with its calling method.
	public boolean isPlayerAllowedInRoom( Player player )
	{
		return canPlayerJoinRoom( player );
	}

	@SuppressWarnings( "unused" )
	public String getOwner()
	{
		return owner;
	}

	@SuppressWarnings( "unused" )
	public void setOwner( String owner )
	{
		this.owner = owner;
	}

	public JSONObject getRoomData()
	{
		return roomData;
	}

	@SuppressWarnings( "unused" )
	public void setRoomData( JSONObject roomData )
	{
		this.roomData = roomData;
	}

	/** Is this {@code ChatRoom} private */
	public boolean isPrivate()
	{
		return isPrivate;
	}

	/**
	 * Checks the list of {@code Player}s in this room for death - if a {@code Player} is "dead" then its placed in an array for removal.
	 * Once all {@code Player}s have been checked, all {@code Player}s in the "dead-array" are passed to {@link ChatRoom#exitRoom(Player)} for removal.
	 */
	public void checkPlayerList()
	{
		//Avoid a ConcurrentModificationException by filling an array of "dead" players, then calling exit-room on each entry in the ArrayList.
		ArrayList<Player> deadPlayers = new ArrayList<>();

		for( Player player : playersInRoom )
		{
			if( !player.isOnline() )
				deadPlayers.add( player );
		}

		for( Player deadPlayer : deadPlayers )
		{
			exitRoom( deadPlayer );
		}
	}

	public String getPlayersInRoomAsJSONString()
	{
		try
		{
			JSONObject playersInRoomAsJSON = new JSONObject();

			for( Iterator<Player> iterator = playersInRoom.iterator(); iterator.hasNext(); )
			{
				Player player = iterator.next();

				if( player.isOnline() ) //Check if the player is online - If they are not then just remove them from the list.
					playersInRoomAsJSON.put( player.getTrueName(), player.getDisplayName() );
				else
					iterator.remove();
			}

			return playersInRoomAsJSON.toString();

		}
		catch( JSONException e )
		{
			e.printStackTrace();
			return null;
		}
	}
}