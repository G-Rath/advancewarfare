package nz.zhang.weapons;

import nz.zhang.units.Unit;

import java.util.HashMap;

/**
 * Weapon abstract class
 * <p>
 * Created by G-Rath on 30/04/2015.
 */
public abstract class Weapon
{
	private String tag = "weapon";

	private String displayName = "weapon";
	private String displayDesc = "Abstract weapon class. This should be overridden";

	/** The range radius of this weapon - this is a radius, and hence can be thought of as the number of tiles in front of the unit */
	private int maxRange = 0;
	/** THe minimal range radius of this weapon - any unit in this radius can't be fired on. */
	private int minRange = 0;

	/**
	 * HashMap that contains the damage matrix for this unit.
	 * if a unit's tag is in this map, then the unit can attack that unit
	 * The amount of damage that that unit does is
	 */
	private HashMap<String, Integer> damageMap = new HashMap<>();

	/**
	 * Fires this weapon at the given target, returning the amount of damage that should be applied based on the vs of each unit
	 *
	 * @param target the target being fired on
	 *
	 * @return damage that was done and should be applied to the target
	 */
	public float fire( Unit target )
	{
		return 0;
	}

	public boolean canAttackUnit( Unit target )
	{
		return damageMap.containsKey( target.tag() );
	}

	public boolean isUnitInRange( Unit target, int maxRangeModifier )
	{
		//@todo calc range
		return false;
	}
}
