package nz.zhang.weapons;

import nz.zhang.units.Unit;

/**
 * PrimaryWeapon class
 * <p>
 * Created by G-Rath on 30/04/2015.
 */
public class PrimaryWeapon extends Weapon
{
	/** The max amount of ammo for this weapon that can be held at any one time */
	private final int AMMO_MAX = 0;
	private String displayDesc = "Primary Weapon abstract class";
	/** The current amount of ammo this weapon has - when this is 0 the weapon can't be used */
	private int currentAmmo = AMMO_MAX;

	@Override
	public float fire( Unit target )
	{
		currentAmmo--;
		return super.fire( target );
	}
}
