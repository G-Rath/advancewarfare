package nz.zhang;

import nz.zhang.client.Client;
import nz.zhang.server.Server;

import java.util.Collections;
import java.util.HashSet;

/**
 * StarterPoint - Entry pointer class: Maven builds one and only one .jar per project, and the server-side of this project shouldn't be run by "just anyone".
 * Hence this class works as the collective entry point, starting the server entry-point only if called with the "server" argument, other wise the client is started.
 * <p>
 * Created by G-Rath on 8/05/2015.
 */
public class StarterPoint
{
	public static void main( String[] args )
	{
		HashSet<String> arguments = new HashSet<>();
		Collections.addAll( arguments, args );

//		for( String s : args )
//		{
//			arguments.add( s );
//			//System.out.println( s );
//		}

		if( arguments.contains( "headless" ) )
		{
			System.setProperty( "java.awt.headless", "true" );
			System.out.println( "Running in HEADLESS mode" );
		}

		String customHost = "";

		for( String s : args )
		{
			if( s.startsWith( "host:" ) )
			{
				customHost = s.substring( s.indexOf( ":" ) + 1 );
				System.out.println( "Custom host of " + customHost );
				Client.getInstance().setHostName( customHost );
			}
		}

		//System.out.println( )

		if( arguments.contains( "server" ) )
			Server.getInstance().setupSever();
		else
			Client.getInstance().begin();
	}
}
