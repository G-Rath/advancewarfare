package nz.zhang.abilities;

/**
 * CanExplode interface
 * <p/>
 * Created by G-Rath on 21/05/2015.
 */
public interface CanExplode
{
	/** The explosion range of this explosion */
	int range = 5;
	/** Does this explosion harm units on the same team? */
	boolean friendlyFire = true;

	void explode();
}
