package nz.zhang.client;

import nz.zhang.gui.ClientFrontend;
import nz.zhang.gui.GameLobby;
import nz.zhang.gui.GetServerConnectionDialog;
import nz.zhang.gui.LoginDialog;
import nz.zhang.server.Player;
import nz.zhang.server.Server;
import nz.zhang.utils.PlayerStatus;
import nz.zhang.world.GameWorld;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.net.Socket;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

/**
 * Client
 * <p/>
 * Created by G-Rath on 29/04/2015.
 */
@SuppressWarnings( "Convert2Lambda" )
public class Client
{
	public final static Charset UTF8 = Charset.forName( "UTF-8" );

	public final static String FOLDER_GAME_SPRITES = "game-sprites/";
	public final static String FOLDER_GAME_DATA = "/game-data/";
	public final static String FOLDER_MAPS = FOLDER_GAME_DATA + "maps/";
	public final static String FOLDER_UNITS = FOLDER_GAME_DATA + "units/";

	private final static Client INSTANCE = new Client();

	@SuppressWarnings( "FieldCanBeLocal" )
	private final int screenX = 640;
	@SuppressWarnings( "FieldCanBeLocal" )
	private final int screenY = 640;

	private String hostName = "localhost";

	private Socket conSocket;

	private ClientFrontend frontend;

	private GetServerConnectionDialog gettingConnectionDialog;

	private LoginDialog loginDialog;

	private PrintWriter socketWriter;
	private BufferedReader socketReader;

	private boolean alive = true;

	private PlayerStatus clientStatus = PlayerStatus.fromString( "online" );

	private JSONObject playerData;

	private String userTrueName;
	private String thisPlayersDisplayName;

	private HashSet<String> playerFriends = new HashSet<>();

	private HashMap<String, JSONObject> roomsData = new HashMap<>();

	private HashMap<String, JSONObject> mapDataList = new HashMap<>();

	private JSONObject allPlayers;

	private GameLobby lobby = null;

	private GameWorld currentWorld = null;

	private String currentGameID;

	private Client()
	{

	}

	public static Client getInstance()
	{
		return INSTANCE;
	}

	public static void main( String[] args )
	{
		Client.getInstance().begin();
	}

	/**
	 * Returns the true-name of the {@code Player} that this {@code Client} represents and is connected to the {@code Server} as.
	 *
	 * @return true-name of the {@code Player} this {@code Client} is connected to the {@code Server} as.
	 */
	public String thisPlayersTrueName()
	{
		return userTrueName;
	}

	public void begin()
	{
		setupServerSocketConnection();
		loadMapsList();
		live();
	}

	public void loadMapsList()
	{
		try
		{
			URL url = getClass().getResource( FOLDER_MAPS );
			File mapsDataFolder = new File( url.toURI() );

			//@todo check conditions - Some conditions here should be removed once the code nears final form.
			if( mapsDataFolder.isDirectory() ) //Don't know what else it could be but we check anyway for safety...
			{
				File[] files = mapsDataFolder.listFiles( new FileFilter()
				{
					@Override
					public boolean accept( File filteredFile )
					{
						return filteredFile.isFile() && filteredFile.getName().endsWith( ".json" );
					}
				} );

				for( File file : files )
				{
					String mapName = file.getName();

					if( mapName.endsWith( ".json" ) )
						mapName = mapName.substring( 0, mapName.lastIndexOf( '.' ) );

					JSONObject mapData = tryLoadMapDataFromDisk( mapName );

					if( mapData == null )
					{
						System.out.println( "error reading map-data file: " + mapName );
					}
					else
					{
						mapDataList.put( mapName, mapData );
					}
				}

				System.out.println( "loaded " + mapDataList.size() + " maps from file" );
			}
			else
				System.out.println( "map folder is not a directory!" );
		}
		catch( URISyntaxException e )
		{
			e.printStackTrace();
		}
	}

	public JSONObject tryLoadMapDataFromDisk( String mapName )
	{
		String filePath = Client.FOLDER_MAPS + mapName + ".json";

		//@todo better error handling
		try( BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( getClass().getResourceAsStream( filePath ), UTF8 ) ) )
		{
			String line;
			String mapData = "";
			while( ( line = bufferedReader.readLine() ) != null )
			{
				mapData += line;
			}

			return new JSONObject( mapData );
		}
		catch( IOException e )
		{
			e.printStackTrace();
		}
		catch( JSONException e )
		{
			System.out.println( "Warning - trying to read map data for " + mapName + " - JSON failed to parse correctly" );
			e.printStackTrace();
		}

		return null;
	}

	public void startGame( String mapName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "start-game" );
			command.put( "game-id", lobby.getGameID() );
			command.put( "map-name", mapName );

			System.out.println( "startGame - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/** Sends a message to the {@code Server} to tell all other {@code Player}s in the game that this {@code Client} is ready for the next turn. */
	public void readyForNextTurn()
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "game-sync" );
			command.put( "game-id", currentGameID );
			command.put( "sync-point", "ready-for-next-turn" );

			System.out.println( "readyForNextTurn - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void setupServerSocketConnection()
	{
		SwingUtilities.invokeLater( new Runnable()
		{
			@Override
			public void run()
			{
				gettingConnectionDialog = new GetServerConnectionDialog();
			}
		} );

		Client.getInstance().tryForServerConnection();
	}

	public void createNewTab( String tabName )
	{
		frontend.addTab( tabName );
	}

	public void setGettingConnectionDialog( GetServerConnectionDialog gettingConnectionDialog )
	{
		this.gettingConnectionDialog = gettingConnectionDialog;
	}

	private void haveServerConnection()
	{
		try
		{
			socketWriter = new PrintWriter( new BufferedWriter( new OutputStreamWriter( conSocket.getOutputStream(), "UTF-8" ) ), true );
			socketReader = new BufferedReader( new InputStreamReader( conSocket.getInputStream(), "UTF-8" ) );
		}
		catch( IOException e )
		{
			e.printStackTrace();
			System.exit( -1 );
		}

		SwingUtilities.invokeLater( new Runnable()
		{
			@Override
			public void run()
			{
				loginDialog = new LoginDialog();
				gettingConnectionDialog.dispose();
				gettingConnectionDialog = null;
			}
		} );
	}

	public void die()
	{
		alive = false;
	}

	public void live()
	{
		String inputLine;

		while( alive )
		{
			if( conSocket != null && conSocket.isBound() )
			{
				try
				{
					while( ( inputLine = readCommandFromServer() ) != null )
					{
						handleServerCommand( inputLine );
						System.out.println( "Server says: " + inputLine );
					}
				}
				catch( java.net.SocketException e )
				{
					JOptionPane.showMessageDialog( frontend, "Server closed-connection" );
					die();
				}
				catch( IOException e )
				{
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Handles trying to invite a {@link Player} to a game with this {@code Client}.This is checked with the
	 * {@code allOnlinePlayers} JSONObject returned from {@link Client#generatePlayersOnlineListFromJSONObject(JSONObject)}
	 * to see if the {@code Player} actually exists, then their status is checked
	 *
	 * @param trueName the trueName of the player to try and invite to a game.
	 */
	public void tryHandleInvitingPlayerToGame( String trueName )
	{
		if( generatePlayersOnlineListFromJSONObject( allPlayers ).has( trueName ) )
			if( getPlayerStatusByTrueName( trueName ) == PlayerStatus.IN_GAME )
			{
				System.out.println( "Trying to invite " + trueName + " to a game but they are already in a game!" );
			}
			else
			{
				System.out.println( "Trying to invite " + trueName + " to a game" );

				try
				{
					JSONObject command = new JSONObject();
					command.put( "command", "invite-player-to-game" );

					command.put( "player-to-invite", trueName );
					command.put( "game-id", lobby.getGameID() );

					sendCommandToServer( command );
				}
				catch( JSONException e )
				{
					e.printStackTrace();
				}
			}
	}

	public void handleKickingPlayerFromTeamSlot( String teamSlot )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "kick-player-from-team-slot" );
			command.put( "team-name", teamSlot );
			command.put( "game-id", lobby.getGameID() );

			sendCommandToServer( command.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Tries to send a message from this {@code Client} to a {@code ChatRoom}, using the {@code currentVisibleChatRoom} as the {@code chatRoom} variable.
	 *
	 * @param message the actual message to be sent to the {@code currentVisibleChatRoom}.
	 */
	public void trySendMessageToChatRoom( String message )
	{
		trySendMessageToChatRoom( frontend.getCurrentVisibleChatRoom(), message );
	}

	/**
	 * Tries to send a message from this {@code Client} to a {@code ChatRoom}.
	 *
	 * @param chatRoom name of the {@code ChatRoom} we want to send the message to.
	 * @param message  the actual message to be sent to the {@code ChatRoom}.
	 */
	public void trySendMessageToChatRoom( String chatRoom, String message )
	{
		try
		{
			System.out.println( "Trying to send message to ChatRoom" );
			JSONObject command = new JSONObject();

			command.put( "command", "send-message-to-room" );

			command.put( "room-name", chatRoom );
			command.put( "sender", userTrueName );
			command.put( "message", message );

			sendCommandToServer( command );

		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Tries to send a message to a specific {@code Player}. Checks if the true-name given is an actual {@code Player}
	 * and that they are not {@link PlayerStatus#OFFLINE} before actually trying to send the message.
	 *
	 * @param trueName the true name of the {@code Player} we're trying to send the message to.
	 */
	@SuppressWarnings( "unused" )
	public void tryMessagePlayer( String trueName, String message )
	{
		try
		{
			if( allPlayers.has( trueName ) && isPlayerOnline( trueName ) )
			{
				System.out.println( "Trying to send message to player" );
				JSONObject command = new JSONObject();

				command.put( "command", "send-message-to-player" );

				JSONObject messageObject = new JSONObject();
				messageObject.put( "sender", userTrueName );
				messageObject.put( "message", message );

				command.put( "target", trueName );
				command.put( "message-object", messageObject );

				sendCommandToServer( command );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void handleServerCommand( String command )
	{
		try
		{
			JSONObject jsonObject = new JSONObject( command );

			if( jsonObject.has( "command" ) )
				processServerCommand( jsonObject );
			else if( jsonObject.has( "game-command" ) )
				processGameCommand( jsonObject );
			else if( jsonObject.has( "reply" ) )
				processServerReply( jsonObject );
			else
				System.out.println( "Server sent JSON object that was neither a COMMAND nor a REPLY" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void processGameCommand( JSONObject gameCommand )
	{
		if( currentWorld == null )
			currentWorld = new GameWorld( lobby.getGameID() );

		currentWorld.processGameCommand( gameCommand );
	}

	public void processServerCommand( JSONObject command )
	{
		try
		{
			switch( command.getString( "command" ) )
			{
				case "ping":
					System.out.println( "Pinged from server" );
					pongServer();
					break;

				case "update-all-players-list":
					updateAllPlayersList( command );
					break;

				case "update-players-in-room-list":
					updatePlayersInRoom( command );
					break;

				case "new-chat-message":
					parseNewChatMessage( command );
					break;

				case "player-join-game":
					handlePlayerJoinGame( command );
					break;

				case "game-update-information":
					handleGameUpdateTeamInformation( command );
					break;

				default:
					break;
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void handleGameUpdateTeamInformation( JSONObject command )
	{

	}

	public boolean doesLobbyHaveFreeSlot()
	{
		return lobby != null && lobby.hasFreePlayerSlot();
	}

	public void invitedToJoinGame( JSONObject command )
	{
		try
		{
			String gameID = command.getString( "game-id" );
			String playerTeam = command.getString( "team" );

//			lobby.playerEnterLobby( playerTrueName, playerTeam );

//			frontend.receiveNewMessageForRoom( command.getString( "chat-room" ), messageObject );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void handlePlayerJoinGame( JSONObject command )
	{
		try
		{
			String playerTrueName = command.getString( "player-name" );
			String playerTeam = command.getString( "team" );

			lobby.playerEnterLobby( playerTrueName, playerTeam );

//			frontend.receiveNewMessageForRoom( command.getString( "chat-room" ), messageObject );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void parseNewChatMessage( JSONObject command )
	{
		try
		{
			JSONObject messageObject = command.getJSONObject( "message-object" );
			frontend.receiveNewMessageForRoom( command.getString( "chat-room" ), messageObject );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void pongServer()
	{
		try
		{
			JSONObject pongReply = new JSONObject();
			pongReply.put( "command", "pong" );
			sendCommandToServer( pongReply.toString() );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void moveFromLoginToClientFrontend()
	{
		SwingUtilities.invokeLater( new Runnable()
		{
			@Override
			public void run()
			{
				frontend = new ClientFrontend();

				loginDialog.dispose();
				loginDialog = null;

				setupAfterLogin();
			}
		} );
	}

	public boolean isFriendOfPlayer( String playerName )
	{
		return playerFriends.contains( playerName );
	}

	/**
	 * Tries to add a {@link Player} to the players friends list.
	 *
	 * @param trueName true-name of the {@code Player} we're trying to add as a friend.
	 */
	public void tryAddPlayerAsFriend( String trueName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "add-friend" );
			command.put( "player-name", trueName );

			System.out.println( "tryAddPlayerAsFriend - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Tries to remove a {@link Player} from the players friends list.
	 *
	 * @param trueName true-name of the {@code Player} we're trying to remove as a friend.
	 */
	public void tryRemovePlayerAsFriend( String trueName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "remove-friend" );
			command.put( "player-name", trueName );

			System.out.println( "tryRemovePlayerAsFriend - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void tryCreateNewGame()
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "create-game" );

			System.out.println( "tryCreateNewGame - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void tryJoinGame( String gameID )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "join-game" );
			command.put( "game-id", gameID );

			System.out.println( "tryJoinGame - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void createNewGameSuccessful( JSONObject reply )
	{
		try
		{
			System.out.println( "createNewGameSuccessful: " + reply.toString() );

			lobby = new GameLobby( true, new ArrayList<>( mapDataList.keySet() ), reply.getString( "game-id" ) );

			tryJoinGame( reply.getString( "game-id" ) );
//			lobby.playerEnterLobby( userTrueName, "red" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void createNewGameFailed( JSONObject reply )
	{
		System.out.println( "createNewGameFailed: " + reply.toString() );
	}

	public void startGameSuccessful( JSONObject reply )
	{
		System.out.println( "startGameSuccessful: " + reply.toString() );

		lobby.setVisible( false ); //Hide the game lobby
		frontend.disableCreateNewGameButton();
	}

	public void startGameFailed( JSONObject reply )
	{
		System.out.println( "startGameFailed: " + reply.toString() );
	}

	public boolean isThisPlayer( String trueName )
	{
		return userTrueName.equals( trueName );
	}

	public void tryChangeSelectedMapForGame( String gameID, String newSelectedMap )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "change-selected-map-for-game" );
			command.put( "game-id", gameID );
			command.put( "new-map-selection", newSelectedMap );

			System.out.println( "tryChangeSelectedMapForGame - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Tries to join a {@code ChatRoom}.
	 * <p/>
	 * <p/>
	 * Callback Success {@link Client#joinChatRoomSuccessful(JSONObject)}
	 * <p/>
	 * Callback Failed {@link Client#joinChatRoomFailed(JSONObject)}
	 *
	 * @param roomName Name of the room we're wanting to try and join
	 *
	 * @see Client#joinChatRoomSuccessful(JSONObject)
	 * @see Client#joinChatRoomFailed(JSONObject)
	 */
	public void tryJoinChatRoom( String roomName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "join-room" );
			command.put( "room-name", roomName );

			System.out.println( "tryJoinChatRoom - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void leaveChatRoomSuccessful( JSONObject reply )
	{

	}

	public void leaveChatRoomFailed( JSONObject reply )
	{

	}

	public void tryLeaveChatRoom( String roomName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "leave-room" );
			command.put( "room-name", roomName );

			System.out.println( "tryLeaveChatRoom - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void processServerReply( JSONObject reply )
	{
		try
		{
			switch( reply.getString( "reply" ) )
			{
				case "create-account":
					if( reply.getBoolean( "result" ) )
						createAccountSuccessful( reply );
					else
						createAccountFailed( reply );
					break;

				case "login":
					if( reply.getBoolean( "result" ) )
						loginSuccessful( reply );
					else
						loginFailed( reply );
					break;

				case "enter-chat-room":
					if( reply.getBoolean( "result" ) )
						joinChatRoomSuccessful( reply );
					else
						joinChatRoomFailed( reply );
					break;

				case "supply-players-in-room-list":
					if( reply.getBoolean( "result" ) )
						getPlayersInRoomListSuccessful( reply );
					else
						getPlayersInRoomListFailed( reply );
					break;

				case "supply-all-players-list":
					if( reply.getBoolean( "result" ) )
						getAllPlayersListSuccessful( reply );
					else
						getAllPlayersListFailed( reply );
					break;

				case "create-new-game":
					if( reply.getBoolean( "result" ) )
						createNewGameSuccessful( reply );
					else
						createNewGameFailed( reply );
					break;

				case "check-chat-room-name-free":
					if( reply.getBoolean( "result" ) )
						chatRoomNameFreeSuccessful( reply );
					else
						chatRoomNameFreeFailed( reply );
					break;

				case "create-chat-room":
					if( reply.getBoolean( "result" ) )
						chatRoomCreationSuccessful( reply );
					else
						chatRoomCreationFailed( reply );
					break;

				case "joined-game":
					if( reply.getBoolean( "result" ) )
						joinedGameSuccessful( reply );
					else
						joinedGameFailed( reply );
					break;

				case "starting-game":
					if( reply.getBoolean( "result" ) )
						startGameSuccessful( reply );
					else
						startGameFailed( reply );
					break;

				default:
					break;
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public boolean inLobby()
	{
		return lobby != null;
	}

	public void exitLobby()
	{
		lobby = null;
	}

	public void createLobby( String gameID )
	{
		lobby = new GameLobby( false, new ArrayList<>( mapDataList.keySet() ), gameID );
	}

	public void joinedGameSuccessful( JSONObject reply )
	{
		try
		{
			String gameID = reply.getString( "game-id" );

			if( lobby == null )
				createLobby( gameID );

			lobby.playerEnterLobby( userTrueName, reply.getString( "team-name" ) );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void joinedGameFailed( JSONObject reply )
	{

	}

	/**
	 * Callback method for {@link Client#requestChatRoomCreation(String, JSONObject)} for when we succeed in creating a new {@code ChatRoom}.
	 *
	 * @param reply {@code Server} reply package.
	 *
	 * @see Client#requestChatRoomCreation(String, JSONObject)
	 * @see Client#chatRoomCreationFailed(JSONObject)
	 */
	public void chatRoomCreationSuccessful( JSONObject reply )
	{
		try
		{
			String roomName = reply.getString( "room-name" );

			JOptionPane.showMessageDialog( frontend, "created room '" + roomName + "'", "created room successful", JOptionPane.ERROR_MESSAGE );

			tryJoinChatRoom( roomName );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Callback method for {@link Client#requestChatRoomCreation(String, JSONObject)} for when we fail in creating a new {@code ChatRoom}.
	 *
	 * @param reply {@code Server} reply package.
	 *
	 * @see Client#requestChatRoomCreation(String, JSONObject)
	 * @see Client#chatRoomCreationSuccessful(JSONObject)
	 */
	public void chatRoomCreationFailed( JSONObject reply )
	{
		try
		{
			String roomName = reply.getString( "room-name" );

			JOptionPane.showMessageDialog( frontend, "failed to create room '" + roomName + "'", "failed to create room", JOptionPane.ERROR_MESSAGE );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void chatRoomNameFreeSuccessful( JSONObject reply )
	{
		frontend.createChatRoomReturn( true );
	}

	public void chatRoomNameFreeFailed( JSONObject reply )
	{
		frontend.createChatRoomReturn( false );
	}

	/**
	 * Causes a waterfall with the GUI and friends list updating based on the new all-players list.
	 * Called when the {@code Server} sends the {@code Client} a new version of the all-players list.
	 *
	 * @param data {@code Server} command package
	 *
	 * @see Server#handleUpdateAllPlayersList()
	 */
	public void updateAllPlayersList( JSONObject data )
	{
		try
		{
			System.out.println( "updateAllPlayersList: " + data.toString() );
			//allPlayers = new JSONObject( data.getJSONObject( "all-players" ) );
			allPlayers = data.getJSONObject( "all-players" );
			System.out.println( "" );
			System.out.println( allPlayers.toString() );
			System.out.println( "" );

			if( userTrueName != null && !userTrueName.equals( "" ) ) //Check if the data for the player this Client is logged in as has changed.
				if( !allPlayers.getJSONObject( userTrueName ).toString().equals( playerData.toString() ) )
				{
					playerData = allPlayers.getJSONObject( userTrueName );
				}

			if( frontend != null )
			{
				frontend.updatePlayersList( generatePlayersOnlineListFromJSONObject( allPlayers ), "all-online" );
				updateFriendsListFromPlayerData();
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Calls all list methods that pass data to the {@link ClientFrontend}.
	 * This is mainly for when the {@code Client} logs in and gets the all-players list
	 */
	public void updateFrontendLists()
	{
	}

	/** Sets up the {@link Client#frontend} after a successful login */
	public void setupAfterLogin()
	{
		frontend.updatePlayersList( generatePlayersOnlineListFromJSONObject( allPlayers ), "all-online" );
		updateFriendsListFromPlayerData();
		tryJoinChatRoom( "global-chat" );
		frontend.setTitle( "Advance Warfare - Client: " + userTrueName + " | " + thisPlayersDisplayName );
	}

	/**
	 * Callback method for {@link Client#requestAllPlayersList()} for when we succeed in getting the all-players list
	 *
	 * @param reply {@code Server} reply package.
	 */
	public void getAllPlayersListSuccessful( JSONObject reply )
	{
		try
		{
			allPlayers = new JSONObject( reply.getString( "all-players" ) );
			frontend.updatePlayersList( generatePlayersOnlineListFromJSONObject( allPlayers ), "all-online" );
			updateFriendsListFromPlayerData();
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Checks if the return from {@link Client#generatePlayersOnlineListFromJSONObject(JSONObject)} has the give {@code true-name}.
	 * If it does it means that a {@link Player} is connected with the given {@code true-name} and so they are online.
	 *
	 * @param playerName the "true-name" of the {@code Player} to check for.
	 *
	 * @return {@code true} if a {@code Player} with the provided "true-name" is online, {@code false} otherwise.
	 */
	public boolean isPlayerOnline( String playerName )
	{
		return generatePlayersOnlineListFromJSONObject( allPlayers ).has( playerName );
	}

	/**
	 * Generates a {@code JSONObject} made up of the {@code Player} data for all {@code Player}s that are considered "online" using a supplied
	 * {@code JSONObject} that's expected to be a player-list.
	 * The returned JSON is in the format of {@code { "true-name" : "player-data" } } .
	 * <p/>
	 * For a {@link Player} to be considered "online" their status must be anything except {@link PlayerStatus#OFFLINE}.
	 *
	 * @param object a {@code JSONObject} expected to contain player-data in the format  {@code { "true-name" : "player-data" } }.
	 *
	 * @return {@link JSONObject} contains the true-names and matching player-data of all players that are "online" from the {@code JSONObject} given.
	 */
	public JSONObject generatePlayersOnlineListFromJSONObject( JSONObject object )
	{
		try
		{
			JSONObject allOnlinePlayers = new JSONObject();
			Iterator<?> keys = object.keys();

			while( keys.hasNext() )
			{
				String key = (String) keys.next();

				if( PlayerStatus.fromString( object.getJSONObject( key ).getString( "status" ) ) != PlayerStatus.OFFLINE )
					allOnlinePlayers.put( key, object.getJSONObject( key ) );
				else
					System.out.println( "\t" + key + ": not online " );
			}

			return allOnlinePlayers;

		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Gets data for a {@code Player} based on their "true-name", checking first if the {@link Client#allPlayers} {@code JSONObject} actually
	 * has an element that matches the "true-name" given.
	 *
	 * @param trueName the true-name of the {@code Player} who's data we wish to get.
	 *
	 * @return {@link JSONObject} of the {@code Player}s data that we wanted, else {@code null} if no element matchs the given true-name
	 */
	public JSONObject getPlayerDataByTrueName( String trueName )
	{
		try
		{
			if( allPlayers.has( trueName ) )
				return allPlayers.getJSONObject( trueName );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Gets the display-name of a {@code Player} by their true-name. Used as a shortcut method so that {@link Client#getPlayerDataByTrueName(String)}
	 * doesn't have to be called and parsed every time a client-side object wants a {@code Player}s display-name.
	 *
	 * @param trueName the true-name of the player who's display-name we want.
	 *
	 * @return the display-name of the {@code Player} based on their true-name. {@code NULL} if no {@code Player} with the given true-name exists.
	 */
	public String getPlayerDisplayNameByTrueName( String trueName )
	{
		try
		{
			if( allPlayers.has( trueName ) ) //This could technically be replaced with a call to "getPlayerDataByTrueName(String)"
				return allPlayers.getJSONObject( trueName ).getString( "display-name" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Gets the true-name of a {@code Player} by their display-name. This is mainly for situations where the user is giving player-name inputs, since
	 * they would likely enter a {@code Player}s display-name instead of their true-name.
	 * <p/>
	 * If the given display-name is actually a true-name then its returned straight away instead.
	 *
	 * @param displayName the display-name of the {@code Player} who we want to know the true-name of.
	 *
	 * @return true-name of the {@code Player} with the given display-name, {@code NULL} if no player with the given true-name exists.
	 */
	public String getPlayerTrueNameByDisplayName( String displayName )
	{
		try
		{
			if( allPlayers.has( displayName ) ) //This could technically be replaced with a call to "getPlayerDataByTrueName(String)"
				return displayName; //Check if the given display-name is actually a true-name, and if it is just return it.

			Iterator iterator = allPlayers.keys();

			while( iterator.hasNext() )
			{
				String trueName = (String) iterator.next();
				JSONObject playerObject = allPlayers.getJSONObject( trueName );

				if( playerObject.getString( "display-name" ).equals( displayName ) )
					return trueName;
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Callback method for {@link Client#requestAllPlayersList()}} for when we fail in getting the all-players list
	 *
	 * @param reply {@code Server} reply package.
	 *
	 * @see Client#requestAllPlayersList()
	 * @see Client#getAllPlayersListSuccessful
	 */
	public void getAllPlayersListFailed( JSONObject reply )
	{
		System.out.println( "getAllPlayersList: Failed - " + reply.toString() );
	}

	/**
	 * Callback method for {@link Client#requestPlayersInChatRoomList(String)} for when we succeed in getting the players-in-room list
	 *
	 * @param reply {@code Server} reply package.
	 *
	 * @see Client#requestPlayersInChatRoomList(String)
	 * @see Client#getPlayersInRoomListFailed(JSONObject)
	 */
	public void getPlayersInRoomListSuccessful( JSONObject reply )
	{
		try
		{
			if( reply.getString( "room-name" ).equals( frontend.getCurrentVisibleChatRoom() ) )
			{
				System.out.println( reply.get( "players-in-room" ) );
				JSONObject playersInRoom = new JSONObject( reply.getString( "players-in-room" ) );
				frontend.updatePlayersList( playersInRoom, "in-room" );
			}
			else
				System.out.println( "Got room-player-list for " + reply.get( "room-name" ) + " but its unneeded so we're chucking it" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Callback method for {@link Client#requestPlayersInChatRoomList(String)} for when we fail to get the list.
	 *
	 * @param reply {@code Server} reply package.
	 *
	 * @see Client#requestPlayersInChatRoomList(String)
	 * @see Client#getPlayersInRoomListSuccessful(JSONObject)
	 */
	public void getPlayersInRoomListFailed( JSONObject reply )
	{
		try
		{
			if( reply.getString( "room-name" ).equals( frontend.getCurrentVisibleChatRoom() ) )
				System.out.println( "Failed to get room-player-list-for " + reply.get( "room-name" ) + " and its the currently visible room " );
			else
				System.out.println( "Failed to get room-player-list for " + reply.get( "room-name" ) + " but its unneeded so it doesn't matter" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/** Requests the list of all players in the chat room the client is currently looking at. */
	public void requestPlayersInCurrentChatRoomList()
	{
		requestPlayersInChatRoomList( frontend.getCurrentVisibleChatRoom() );
	}

	/**
	 * Requests a list of all the players for the specified chat-room.
	 * Callback Success {@link Client#getPlayersInRoomListSuccessful(JSONObject)}
	 * Callback Failed {@link Client#getPlayersInRoomListFailed(JSONObject)}
	 *
	 * @param roomName the name of the room that we want the player-list of.
	 */
	public void requestPlayersInChatRoomList( String roomName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "request-players-in-room-list" );
			command.put( "room-name", roomName );

			System.out.println( "requestPlayersInCurrentChatRoomList - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Requests that the {@code Server} checks that the given roomName is free, and currently not being used for a {@code ChatRoom}.
	 * Callback Success {@link Client#chatRoomNameFreeSuccessful(JSONObject)}
	 * Callback Failed {@link Client#chatRoomNameFreeFailed(JSONObject)}
	 *
	 * @param roomName the name that we want to check for use with a {@code ChatRoom}.
	 */
	public void requestCheckIfChatRoomNameIsFree( String roomName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "check-chat-room-name-is-free" );
			command.put( "room-name", roomName );

			System.out.println( "requestCheckIfChatRoomNameIsFree - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void requestAllPlayersList()
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "request-all-players-list" );

			System.out.println( "requestAllPlayersList - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void requestChatRoomCreation( String roomName, JSONObject roomData )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "create-new-chat-room" );
			command.put( "room-name", roomName );
			command.put( "room-data", roomData );

			System.out.println( "requestChatRoomCreation - Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Callback method for {@link Client#tryJoinChatRoom(String)} for when we succeed in joining the room.
	 *
	 * @param reply {@code Server} reply package.
	 */
	public void joinChatRoomSuccessful( JSONObject reply )
	{
		try
		{
			System.out.println( "joinChatRoomSuccessful: " + reply.toString() );
			JSONObject roomData = new JSONObject( reply.getString( "room-data" ) );
			roomsData.put( reply.getString( "room-name" ), roomData );

			if( reply.getString( "room-name" ).equals( "global-chat" ) )
				frontend.enterRoom( reply.getString( "room-name" ), false );
			else
				frontend.enterRoom( reply.getString( "room-name" ), true );

//			frontend.
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Checks if the {@code Player} that this {@code Client} is logged in as is the owner of the given {@code ChatRoom}.
	 *
	 * @param roomName name of the {@code ChatRoom} that we want to check for ownership.
	 *
	 * @return {@code true} if this {@code Player} owns the {@code ChatRoom}, {@code false} otherwise.
	 */
	public boolean isRoomOwner( String roomName )
	{
		return getRoomOwner( roomName ).equals( userTrueName );
	}

	/**
	 * Gets the true-name of the {@code Player} who owns the given {@code ChatRoom}.
	 *
	 * @param roomName name of the {@code ChatRoom} who we want to know the owner of.
	 *
	 * @return true-name of the {@code Player} who owns the {@code ChatRoom} if it exists, {@code NULL} if it doesn't exist.
	 */
	public String getRoomOwner( String roomName )
	{
		try
		{
			if( roomsData.containsKey( roomName ) ) //Check that the room actually exists first.
				return roomsData.get( roomName ).getString( "owner" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Callback method of {@link Client#tryJoinChatRoom(String)} for when we fail to join the room
	 *
	 * @param reply {@code Server} reply package.
	 */
	public void joinChatRoomFailed( JSONObject reply )
	{
		try
		{
			System.out.println( reply );
			System.out.println( "Join room failed: " + reply.getString( "reason" ) );

//			if( reply.getString( "response" ).equals( "room does not exist" ) )
			JOptionPane.showMessageDialog( frontend, "no chat-room with that name exists", "error", JOptionPane.ERROR_MESSAGE );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	private void updatePlayersInRoom( JSONObject data )
	{
		try
		{
			if( frontend.getCurrentVisibleChatRoom().equals( data.getString( "room-name" ) ) ) //If the room we're in is equal to the room the list is for...
			{
				JSONObject playersInRoom = new JSONObject( data.getString( "players-in-room" ) );
//				System.out.println( "" );
//				System.out.println( "players in room:" );
//				System.out.println( playersInRoom.toString() );
//				System.out.println( "" );
				frontend.updatePlayersList( playersInRoom, "in-room" );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public JSONObject getMapDataByName( String mapName )
	{
		return mapDataList.get( mapName );
	}

	/**
	 * Gets data about a {@code ChatRoom} from the {@code roomsData HashMap} by the key of the {@code ChatRoom}s name.
	 *
	 * @param roomName name of the {@code ChatRoom} who's data we want.
	 *
	 * @return {@code JSONObject} of the {@code ChatRoom}s data, {@code NULL} if no room with that name exists.
	 */
	public JSONObject getChatRoomDataFromName( String roomName )
	{
		if( roomsData.containsKey( roomName ) )
			return roomsData.get( roomName );
		else
			return null;
	}

	/**
	 * Sends a command to the {@code Server} via the {@link Client#socketWriter}.
	 * The {@code Server} expects the actual string to be a {@code JSONObject}.
	 *
	 * @param command string to send over the {@code Socket}. Expected to be a correctly formatted {@code JSONObject}.
	 */
	private void sendCommandToServer( String command )
	{
		socketWriter.println( command );
	}

	/**
	 * Sends a command with parameters in JSON as a {@code String} to the {@link Server} where it can be created back into a {@link JSONObject} and parsed.
	 *
	 * @param command information for the command that the {@code Server} is to carry out.
	 */
	public void sendCommandToServer( JSONObject command )
	{
		socketWriter.println( command.toString() );
	}

	/**
	 * Reads a single line as a command from the {@link Server}'s socket reader.
	 *
	 * @return Command in a single line.
	 *
	 * @throws IOException
	 */
	public String readCommandFromServer() throws IOException
	{
		return socketReader.readLine();
	}

	public String getHashedPassword( char[] password )
	{
		try
		{
			MessageDigest digest = MessageDigest.getInstance( "SHA-256" );
			digest.update( new String( password ).getBytes() );
			return new String( digest.digest() );
		}
		catch( NoSuchAlgorithmException e )
		{
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Tries to login with the {@code Server} by sending the supplied username and password.
	 * Callback Success {@link Client#loginSuccessful(JSONObject)}
	 * Callback Failed {@link Client#loginFailed(JSONObject)}
	 *
	 * @param username username to use
	 * @param password password to use
	 */
	public void tryLogin( String username, String password )
	{
		JSONObject command = new JSONObject();
		try
		{
			command.put( "command", "login" );
			command.put( "username", username );
			command.put( "password", String.valueOf( password ) );

			System.out.println( "tryLogin: Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Tries to create a new account with the {@code Server} by sending the supplied username and password.
	 * Callback Success {@link Client#createAccountSuccessful(JSONObject)}
	 * Callback Failed {@link Client#createAccountFailed(JSONObject)}
	 *
	 * @param username username to use
	 * @param password password to use
	 */
	public void tryCreateAccount( String username, String password )
	{
		if( username.equals( "" ) )
			return; //We can have a blank password but not a blank username

		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "create-account" );
			command.put( "username", username );
			command.put( "password", String.valueOf( password ) );

			System.out.println( "tryCreateAccount: Sending command to server: " + command.toString() );
			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Callback method for when the {@code Client} tries to make a new account, and the {@link Server} sends a replay back for a successful account creation.
	 * Callback from {@link Client#tryCreateAccount(String, String)}, failed counterpart is {@link Client#createAccountFailed(JSONObject)}
	 *
	 * @param reply {@code Server}'s JSON reply. May contain additional information of use
	 */
	private void createAccountSuccessful( JSONObject reply )
	{
		System.out.println( "create-account: successful - " + reply.toString() );
		loginDialog.setFeedbackText( "Account created successfully", Color.GREEN );
		tryLogin( loginDialog.getUsername(), loginDialog.getPassword() );
	}

	/**
	 * Callback method for when the {@code Client} tries to make a new account, and the {@code Server} sends a reply back for a failed account creation.
	 * Callback from {@link Client#tryCreateAccount(String, String)}, success counterpart is {@link Client#createAccountSuccessful(JSONObject)}
	 *
	 * @param reply {@code Server}'s JSON reply. May contain additional information of use.
	 */
	private void createAccountFailed( JSONObject reply )
	{
		System.out.println( "create-account: failed - " + reply.toString() );
		loginDialog.setFeedbackText( "Account creation failed", Color.RED );
		loginDialog.enableButtons();
	}

	/**
	 * Callback method for when the {@code Client} tries to login, and the {@code Server} sends a reply back for a successful login.
	 * Callback from {@link Client#tryLogin(String, String)}, failed counterpart is {@link Client#loginFailed(JSONObject)}
	 *
	 * @param reply {@code Server}'s JSON reply. May contain additional information of use.
	 */
	private void loginSuccessful( JSONObject reply )
	{
		System.out.println( "login: successful" );
		loginDialog.setFeedbackText( "Login success", Color.BLUE );

		moveFromLoginToClientFrontend();

		try //@todo keep an eye on this - technically if it takes too long to read the file then this variable will be accessed before its assigned
		{
			playerData = new JSONObject( reply.getString( "response" ) );
			userTrueName = reply.getString( "true-name" );
			thisPlayersDisplayName = playerData.getString( "display-name" );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	/**
	 * Callback method for when the {@link Client} tries to login, and the {@code Server} sends a reply back for a failed login.
	 * Callback from {@link Client#tryLogin(String, String)}, success counterpart is {@link Client#loginSuccessful(JSONObject)}
	 *
	 * @param reply {@link Server}'s JSON reply. May contain additional information of use.
	 */
	private void loginFailed( JSONObject reply )
	{
		System.out.println( "login: failed - " + reply.toString() );
		loginDialog.setFeedbackText( "Login failed", Color.RED );
		loginDialog.enableButtons();
	}

	public void updateFriendsListFromPlayerData()
	{
		createFriendsListFromPlayerData();
		frontend.rebuildPlayerFriendList( playerFriends );
	}

	public void createFriendsListFromPlayerData()
	{
		try //@todo keep an eye on this - technically if it takes too long to read the file then this variable will be accessed before its assigned
		{
			playerFriends.clear();

			JSONArray friendsObject = playerData.getJSONArray( "friends" );
			for( int i = 0; i < friendsObject.length(); i++ )
			{
				playerFriends.add( friendsObject.getString( i ) );//, frontend.isPlayerOnline( friendsObject.getString( i ) ) );
			}
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public String getThisPlayersDisplayName()
	{
		return thisPlayersDisplayName;
	}

	public void handleTryChangeDisplayName( String newDisplayName )
	{
		try
		{
			JSONObject command = new JSONObject();
			command.put( "command", "change-display-name" );
			command.put( "new-display-name", newDisplayName );

			System.out.println( "handleTryChangeDisplayName: Sending command to server: " + command.toString() );

			thisPlayersDisplayName = newDisplayName;
			frontend.setTitle( "Advance Warfare - Client: " + userTrueName + " | " + thisPlayersDisplayName );

			sendCommandToServer( command );
		}
		catch( JSONException e )
		{
			e.printStackTrace();
		}
	}

	public void retryForServerConnection()
	{
		tryForServerConnection();
	}

	/** Try and get a connection to the {@code Server} using the custom {@code HostName} and the server-port set by {@link Server#SERVER_PORT}. */
	public void tryForServerConnection()
	{
		try
		{
			conSocket = new Socket( hostName, Server.SERVER_PORT );
			haveServerConnection();
		}
		catch( IOException e )
		{
			System.out.println( "failed to connect to server " );
			gettingConnectionDialog.failedToGetConnection();
		}
	}

	public void setHostName( String hostName )
	{
		this.hostName = hostName;
	}

	/**
	 * Gets the status of a {@code Player} as a {@code PlayerStatus} based on their true-name. If no player exists by
	 * the given true-name or an exception is thrown then {@code null} is returned.
	 *
	 * @param trueName the true-name of the {@code Player} we're wanting the status of.
	 *
	 * @return {@link PlayerStatus} of the requested {@code Player}s status, {@code NULL} if no player with the given true-name
	 * exists, or if an {@code exception} is thrown when trying to parse the {@code allPlayers} JSON object.
	 */
	public PlayerStatus getPlayerStatusByTrueName( String trueName )
	{
		try
		{
			return allPlayers.has( trueName ) ? PlayerStatus.fromString( allPlayers.getJSONObject( trueName ).getString( "status" ) ) : null;
		}
		catch( JSONException e )
		{
			e.printStackTrace();
			return null;
		}
	}
}