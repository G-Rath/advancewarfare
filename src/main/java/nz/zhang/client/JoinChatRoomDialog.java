package nz.zhang.client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * JoinChatRoomDialog
 * <p>
 * Created by G-Rath on 29/05/2015.
 */
public class JoinChatRoomDialog extends JDialog
{
	private JTextField textField;
	private JButton button_Submit;
	private JButton button_Cancel;

	private String returnValue = "";

	public JoinChatRoomDialog() throws HeadlessException
	{
		setupGUI();

		setModal( true );
		this.setModalityType( ModalityType.APPLICATION_MODAL );
		this.setAlwaysOnTop( true );
	}

	public JoinChatRoomDialog( Frame owner )
	{
		super( owner );
		setModal( true );
		this.setModalityType( ModalityType.APPLICATION_MODAL );
		setupGUI();
//		this.setAlwaysOnTop( true );
//		this.setDefaultCloseOperation( DISPOSE_ON_CLOSE );
	}

	public void setupGUI()
	{
		JPanel buttonPanel = new JPanel();

		buttonPanel.setLayout( new BoxLayout( buttonPanel, BoxLayout.LINE_AXIS ) );
		buttonPanel.setBorder( BorderFactory.createEmptyBorder( 5, 10, 10, 10 ) );

		button_Submit = new JButton( "submit" );
		button_Cancel = new JButton( "cancel" );

		buttonPanel.add( Box.createHorizontalGlue() );
//		buttonPanel.add( Box.createHorizontalStrut( 20 ) );
		buttonPanel.add( button_Submit );
//		buttonPanel.add( Box.createHorizontalStrut( 10 ) );
//		buttonPanel.add( Box.createHorizontalGlue() );
		buttonPanel.add( Box.createHorizontalStrut( 20 ) );
//		buttonPanel.add( Box.createHorizontalStrut( 10 ) );
		buttonPanel.add( button_Cancel );
//		buttonPanel.add( Box.createHorizontalStrut( 20 ) );
		buttonPanel.add( Box.createHorizontalGlue() );

		JPanel inputPanel = new JPanel();

		inputPanel.setLayout( new BoxLayout( inputPanel, BoxLayout.PAGE_AXIS ) );

		textField = new JTextField( 20 );

		JPanel labelPanel = new JPanel();
		labelPanel.setLayout( new BoxLayout( labelPanel, BoxLayout.LINE_AXIS ) );

		labelPanel.add( new JLabel( "chat-room name:" ) );
		labelPanel.add( Box.createHorizontalGlue() );

		inputPanel.add( labelPanel );
		inputPanel.add( Box.createVerticalStrut( 5 ) );
		inputPanel.add( textField );

		JPanel mainPanel = new JPanel();
		mainPanel.setLayout( new BoxLayout( mainPanel, BoxLayout.PAGE_AXIS ) );

		mainPanel.setBorder( BorderFactory.createEmptyBorder( 5, 5, 5, 5 ) );

		mainPanel.add( inputPanel );
		mainPanel.add( Box.createVerticalStrut( 5 ) );
		mainPanel.add( buttonPanel );

//		this.setDefaultCloseOperation( WindowConstants.EXIT_ON_CLOSE );
		this.add( mainPanel );
		this.setResizable( false );
		this.pack();
		this.setLocationRelativeTo( null );
//		this.setVisible( true );
//		this.setAlwaysOnTop( true );
//		this.setModalExclusionType( Dialog.ModalExclusionType.APPLICATION_EXCLUDE );



		button_Cancel.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				cancel();
//				dispose();
//				JoinChatRoomDialog.this.setVisible( false );
//				JoinChatRoomDialog.this.dispatchEvent( new WindowEvent( JoinChatRoomDialog.this, WindowEvent.WINDOW_CLOSING ) );
			}
		} );

//						JoinChatRoomDialog.this.disableEvents( new WindowEvent( JoinChatRoomDialog.this, WindowEvent.WINDOW_CLOSING ) );
		// on ESC key close frame
//		this.getRootPane().getInputMap( JComponent.WHEN_IN_FOCUSED_WINDOW ).put( KeyStroke.getKeyStroke( KeyEvent.VK_ESCAPE, 0 ), "Cancel" ); //$NON-NLS-1$
//		this.getRootPane().getActionMap().put( "Cancel", new AbstractAction()
//		{ //$NON-NLS-1$
//			public void actionPerformed( ActionEvent e )
//			{
//				dispose();
//			}1`
//		} );
	}

	public void tryAccept()
	{

	}

	public void accept()
	{

	}

	public void cancel()
	{
		returnValue = "";
		this.setVisible( false );
	}

}

